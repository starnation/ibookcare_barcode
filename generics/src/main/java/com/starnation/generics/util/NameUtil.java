package com.starnation.generics.util;

/*
 * @author lsh
 * @since 2017. 5. 17.
*/
public final class NameUtil {

    //======================================================================
    // Public Methods
    //======================================================================

    @SuppressWarnings("unused")
    public static String getSimpleName(String fullName) {
        try {
            return fullName.substring(fullName.lastIndexOf(".") + 1);
        } catch (Exception e) {
            return fullName;
        }
    }
}
