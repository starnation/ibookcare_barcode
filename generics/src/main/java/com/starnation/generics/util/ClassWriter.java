package com.starnation.generics.util;

import java.io.BufferedWriter;
import java.util.ArrayList;

import javax.annotation.processing.ProcessingEnvironment;
import javax.tools.JavaFileObject;

/*
 * @author lsh
 * @since 2017. 5. 17.
*/
public abstract class ClassWriter {

    //======================================================================
    // Variables
    //======================================================================

    private String mPackage;

    private String mModifier;

    private String mSimpleName;

    //======================================================================
    // Constructor
    //======================================================================

    public ClassWriter(ProcessingEnvironment env, String name) {
        init(env, "public ", name);
    }

    public ClassWriter(ProcessingEnvironment env, String modifier, String name) {
        init(env, modifier, name);
    }

    //======================================================================
    // Public Methods
    //======================================================================

    public abstract void onBody(BufferedWriter buffer);

    public abstract void onImport(ArrayList<String> list);

    public String onExtendsString() {
        return "";
    }

    public String onImplementsString() {
        return "";
    }

    //======================================================================
    // Private Methods
    //======================================================================

    private void init(ProcessingEnvironment env, String modifier, String name) {
        int lastDotIndex = name.lastIndexOf(".");
        mPackage = name.substring(0, lastDotIndex);
        mModifier = modifier;
        mSimpleName = name.substring(lastDotIndex + 1);

        try {
            JavaFileObject jfo = env.getFiler().createSourceFile(name);
            BufferedWriter bufferedWriter = new BufferedWriter(jfo.openWriter());

            writePackage(bufferedWriter);
            writeImport(bufferedWriter);
            writeClass(bufferedWriter);

            bufferedWriter.close();
        }catch (Exception e) {
            //Nothing
        }
    }

    private void writePackage(BufferedWriter buffer) {
        try {
            buffer.append("package ");
            buffer.write(mPackage);
            buffer.append(";");
            buffer.newLine();
            buffer.newLine();
        } catch (Exception e) {
            //Nothing
        }
    }

    private void writeImport(BufferedWriter buffer) {
        ArrayList<String> list = new ArrayList<>();
        onImport(list);

        for (String name : list) {
            try {
                buffer.append("import ");
                buffer.write(name);
                buffer.append(";");
                buffer.newLine();
            } catch (Exception e) {
                //Nothing
            }
        }
    }

    private void writeClass(BufferedWriter buffer) {
        try {
            buffer.newLine();

            //Modify 2016. 11. 28. vkwofm 클래스 정의
            buffer.append(mModifier);
            buffer.append(" class ");
            buffer.write(mSimpleName);
            if (onExtendsString().length() > 0) {
                buffer.write(" extends ");
                buffer.write(onExtendsString());
            }

            if (onImplementsString().length() > 0) {
                buffer.write(" implements ");
                buffer.write(onImplementsString());
            }

            //Modify 2016. 11. 28. vkwofm 클래스 내부 정의 시작
            buffer.append(" {");

            buffer.newLine();
            onBody(buffer);
            buffer.newLine();

            //Modify 2016. 11. 28. vkwofm 클래스 내부 정의 종료
            buffer.append("}");
        } catch (Exception e) {
            //Nothing
        }
    }
}
