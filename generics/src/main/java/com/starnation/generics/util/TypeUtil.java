package com.starnation.generics.util;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/*
 * @author lsh
 * @since 2017. 5. 17.
*/
public final class TypeUtil {

    //======================================================================
    // Public Methods
    //======================================================================

    /**
     * @param target TypeHandler define class.
     *
     * @return Type map( com.xxx.1<co.xxx.2> -> Type {@link Type})
     */
    public static Map<String, Type> getInnerClassExtendsTypes(Class<?> target) {
        Map<String, Type> typeMap = new HashMap<>();
        int clazzIndex = 0;
        while (true) {
            try {
                String className = target.getName() + "$" + clazzIndex++;
                Class<?> clazz = Class.forName(className);
                Type types[] = getGenericSuperclassParameterizedTypesInternal(clazz);
                typeMap.put(types[0].toString(), types[0]);
            } catch (ClassNotFoundException notFound) {
                break;
            } catch (Exception e) {
                break;
            }
        }

        return typeMap;
    }

    /**
     * @param target      TypeHandler define class.
     * @param memberArray 1.class, 2.class ...
     *
     * @return Type {@link Type}
     */
    public static Type getInnerClassExtendsType(Class<?> target, Class<?>... memberArray) {
        Map<String, Type> typeMap = getInnerClassExtendsTypes(target);
        return getMatchingType(typeMap, memberArray);
    }

    /**
     * @param typeMap     Type map( com.xxx.1<co.xxx.2> -> Type {@link Type})
     * @param memberArray 1.class, 2.class ...
     *
     * @return Type {@link Type}
     */
    public static Type getMatchingType(Map<String, Type> typeMap, Class<?>... memberArray) {
        try {
            if (typeMap.containsKey(TypeUtil.toExtendsName(memberArray)) == true) {
                return typeMap.get(TypeUtil.toExtendsName(memberArray));
            } else if (typeMap.containsKey(TypeUtil.toExtendsSimpleName(memberArray)) == true) {
                return typeMap.get(TypeUtil.toExtendsSimpleName(memberArray));
            }
        } catch (Exception e) {
            //Nothing
        }
        return Object.class;
    }

    /**
     * @param memberArray ex) 1.class, 2.class ...
     *
     * @return ex) com.xxx.1'<com.xxx.2'>
     */
    public static String toExtendsName(Class<?>[] memberArray) {
        ArrayList<String> list = new ArrayList<>();
        for (Class<?> clazz : memberArray) {
            list.add(clazz.getName());
        }
        return toExtendsName(list);
    }

    /**
     * @param memberArray ex) 1.class, 2.class ...
     *
     * @return ex) 1'<2'>
     */
    @SuppressWarnings("unused")
    public static String toExtendsSimpleName(Class<?>[] memberArray) {
        ArrayList<String> list = new ArrayList<>();
        for (Class<?> clazz : memberArray) {
            list.add(clazz.getSimpleName());
        }
        return toExtendsName(list);
    }

    /**
     * @param list ArrayList'<Class.getName()'>
     *
     * @return ex) com.xxx.1'<com.xxx.2'>
     */
    public static String toExtendsName(ArrayList<String> list) {
        String extendsValue = "";
        if (list != null && list.size() > 0) {
            for (int i = list.size() - 1; i >= 0; i--) {
                String name = list.get(i);
                if (extendsValue.length() == 0) {
                    extendsValue = name;
                } else {
                    if (name.equals(Object.class.getName()) == false) {
                        extendsValue = "<" + extendsValue + ">";
                        extendsValue = name + extendsValue;
                    }
                }
            }
        }
        return extendsValue;
    }

    //======================================================================
    // Methods
    //======================================================================

    private static Type[] getGenericSuperclassParameterizedTypesInternal(Class<?> target) {
        Type[] types = getGenericSuperclassType(target);
        if (types.length > 0 && types[0] instanceof ParameterizedType) {
            return types;
        }
        return null;
    }

    private static Type[] getGenericSuperclassType(Class<?> target) {
        if (target == null) {
            return new Type[0];
        }

        Type type = target.getGenericSuperclass();
        if (type != null) {
            if (type instanceof ParameterizedType) {
                return new Type[]{type};
            }
        }
        return new Type[0];
    }
}
