package com.starnation.generics.model;

import com.starnation.generics.util.NameUtil;
import com.starnation.generics.util.TypeUtil;

import java.util.ArrayList;
import java.util.List;

import javax.lang.model.element.AnnotationValue;
import javax.lang.model.element.TypeElement;
import javax.lang.model.type.TypeMirror;

/*
 * @author lsh
 * @since 2017. 5. 17.
*/
public class GenericsHandler {

    //======================================================================
    // Variables
    //======================================================================

    private String mSimpleName;

    private String mQualifiedName;

    ArrayList<String> mTargetClass = new ArrayList<>();

    String mExtendsClassName;

    //======================================================================
    // Constructor
    //======================================================================

    public GenericsHandler(TypeElement typeElement) {
        mSimpleName = typeElement.getSimpleName().toString();
        mQualifiedName = typeElement.getQualifiedName().toString();
        //Modify 2016. 12. 5. lsh Default Object.class 면 현재 객체를 넣어준다.
        mTargetClass.add(mQualifiedName);
    }

    //======================================================================
    // Override Methods
    //======================================================================

    @Override
    public String toString() {
        return "mSimpleName -> " + mSimpleName +
                "\nmQualifiedName -> " + mQualifiedName;
    }

    //======================================================================
    // Public Methods
    //======================================================================

    public String getSimpleName() {
        return mSimpleName;
    }

    public String toCrateClassName() {
        return mQualifiedName;
    }

    public void setTargetClass(List<? extends AnnotationValue> list) {
        if (list != null) {
            for (AnnotationValue aValue : list) {
                //Modify 2016. 12. 5. lsh Default Object.class 면 현재 객체를 continue
                if (aValue.getValue().toString().equals(Object.class.getName()) == true
                        || aValue.getValue().toString().equals(mQualifiedName) == true) {
                    continue;
                }

                mTargetClass.add(aValue.getValue().toString());
            }
        }
    }

    public ArrayList<String> getTargetClass() {
        return mTargetClass;
    }

    public String getExtendsClassName() {
        return mExtendsClassName;
    }

    public void setExtendsClass(TypeMirror typeMirror) {
        if (typeMirror != null) {
            mExtendsClassName = typeMirror.toString();
        }
    }

    public String getPackageName() {
        String packageName = mQualifiedName;
        try {
            packageName = packageName.substring(0, packageName.lastIndexOf("."));
        } catch (Exception e) {
            //Nothing
        }
        return packageName;
    }

    public ArrayList<String> makeImportList(String targetClass) {
        ArrayList<String> list = new ArrayList<>();
        if (mExtendsClassName != null && mExtendsClassName.length() > 0) {
            list.add(mExtendsClassName);
        }
        if (targetClass.equals(mQualifiedName) == false) {
            list.add(targetClass.replace("[]", ""));
        }
        list.add(mQualifiedName.replace("[]", ""));
        return list;
    }

    public String toExtendsClassName(String targetClass) {
        ArrayList<String> list = new ArrayList<>();
        if (mExtendsClassName != null && mExtendsClassName.length() > 0) {
            list.add(NameUtil.getSimpleName(mExtendsClassName));
        }

        if (targetClass.equals(mQualifiedName) == false) {
            list.add(NameUtil.getSimpleName(targetClass));
        }

        if (isExtendsClassEquals(mExtendsClassName, mQualifiedName) == false) {
            list.add(NameUtil.getSimpleName(mQualifiedName));
        }
        return TypeUtil.toExtendsName(list);
    }

    public boolean isExtendsClassEquals(String targetClass) {
        return isExtendsClassEquals(mExtendsClassName, targetClass);
    }

    //======================================================================
    // Private Methods
    //======================================================================

    private static boolean isExtendsClassEquals(String extendsClassName, String targetClass) {
        return extendsClassName != null && extendsClassName.length() > 0 && extendsClassName.equals(targetClass) == true;
    }
}
