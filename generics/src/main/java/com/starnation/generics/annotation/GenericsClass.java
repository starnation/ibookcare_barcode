package com.starnation.generics.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/*
 * @author lsh
 * @since 2017. 5. 17.
*/
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.SOURCE)
public @interface GenericsClass {

    Class<?> extendsClass();

    Class<?>[] type() default Object.class;
}
