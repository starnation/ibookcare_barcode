package com.starnation.generics.processor;


import com.starnation.generics.annotation.GenericsClass;
import com.starnation.generics.model.GenericsHandler;
import com.starnation.generics.util.ClassWriter;

import java.io.BufferedWriter;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.RoundEnvironment;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.AnnotationMirror;
import javax.lang.model.element.AnnotationValue;
import javax.lang.model.element.Element;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.TypeElement;
import javax.lang.model.type.TypeMirror;

/*
 * http://hauchee.blogspot.kr/2015/12/compile-time-annotation-processing-getting-class-value.html
 * @author lsh
 * @since 2017. 5. 17.
*/
public final class GenericsProcessor extends AbstractProcessor {

    //======================================================================
    // Override Methods
    //======================================================================

    @Override
    public Set<String> getSupportedAnnotationTypes() {
        Set<String> set = new HashSet<>();
        set.add(GenericsClass.class.getCanonicalName());
        return set;
    }

    @Override
    public SourceVersion getSupportedSourceVersion() {
        return SourceVersion.latestSupported();
    }

    @SuppressWarnings("unchecked")
    @Override
    public boolean process(Set<? extends TypeElement> set, RoundEnvironment roundEnvironment) {
        try {
            for (Element handlerElement : roundEnvironment.getElementsAnnotatedWith(GenericsClass.class)) {
                TypeElement handlerClass = (TypeElement) handlerElement;

                final GenericsHandler genericsHandler = new GenericsHandler(handlerClass);

                List<? extends AnnotationMirror> annotationMirrors = handlerElement.getAnnotationMirrors();
                for (AnnotationMirror annotationMirror : annotationMirrors) {
                    Map<? extends ExecutableElement, ? extends AnnotationValue> elementValues = annotationMirror.getElementValues();
                    for (Map.Entry<? extends ExecutableElement, ? extends AnnotationValue> entry : elementValues.entrySet()) {
                        String key = entry.getKey().getSimpleName().toString();
                        Object value = entry.getValue().getValue();
                        switch(key) {
                            case "extendsClass":
                                genericsHandler.setExtendsClass((TypeMirror) value);
                                break;
                            case "type":
                                genericsHandler.setTargetClass((List<? extends AnnotationValue>) value);
                                break;
                        }
                    }
                }

                int index = 0;
                for (final String type : genericsHandler.getTargetClass()) {
                    String className = genericsHandler.toCrateClassName() + "$" + Integer.toString(index);
                    if (genericsHandler.isExtendsClassEquals(type) == true) {
                        continue;
                    }

                    new ClassWriter(processingEnv, className) {

                        @Override
                        public void onImport(ArrayList<String> list) {
                            list.addAll(genericsHandler.makeImportList(type));
                        }

                        @Override
                        public String onExtendsString() {
                            return genericsHandler.toExtendsClassName(type);
                        }

                        @Override
                        public void onBody(BufferedWriter buffer) {

                        }
                    };
                    index++;
                }
            }
        } catch (Exception e) {
            // TODO: 2016. 12. 5. ErrorLog 표시
        }
        return true;
    }
}