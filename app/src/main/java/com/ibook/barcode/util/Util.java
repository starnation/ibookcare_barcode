package com.ibook.barcode.util;

import android.support.v4.app.Fragment;

import com.ibook.barcode.app.fragment.Impl;
import com.starnation.util.StringUtil;
import com.starnation.util.validator.CollectionValidator;

import java.util.List;

/*
 * @author lsh
 * @since 2017. 5. 17.
*/
public class Util {

    @SuppressWarnings("RestrictedApi")
    public static boolean isTargetFragmentTop(Fragment target) {
        if (target != null) {
            List<Fragment> fragmentList = target.getFragmentManager().getFragments();
            if (CollectionValidator.isValid(fragmentList) == true) {
                for (int index = fragmentList.size() - 1; index >= 0; index--) {
                    Fragment fragment = fragmentList.get(index);
                    if (fragment instanceof Impl.SupportFragment) {
                        if (fragment.equals(target) == true) {
                            return true;
                        }
                        break;
                    }
                }
            }
        }
        return false;
    }

    public static boolean isDefaultImage(String url) {
        boolean emptyUrl = StringUtil.isEmpty(url);
        return emptyUrl == true || (StringUtil.isEmpty(url) == false && url.endsWith("default_img.png") == true);
    }
}
