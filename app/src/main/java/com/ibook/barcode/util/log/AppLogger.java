package com.ibook.barcode.util.log;

import com.ibook.barcode.BuildConfig;
import com.starnation.android.util.Logger;
import com.starnation.util.StringUtil;

import java.util.Locale;

/*
 * @author lsh
 * @since 17. 5. 17.
*/
public final class AppLogger {

    //======================================================================
    // Constants
    //======================================================================

    private final static String LOG_FORMAT = "[%s]: %s";

    //======================================================================
    // Public Methods
    //======================================================================

    public static void printStackTrace(Exception e) {
        if (BuildConfig.DEBUG && e != null) {
            e.printStackTrace();
        }
    }

    public static void d(String message) {
        if (BuildConfig.DEBUG) {
            Logger.d(Tag.DEFAULT.toTag(), message);
        }
    }

    public static void d(Tag tag, String message) {
        if (BuildConfig.DEBUG) {
            Logger.d(tag.toTag(), message);
        }
    }

    public static void e(Tag tag, String message) {
        if (BuildConfig.DEBUG) {
            Logger.e(tag.toTag(), message);
        }
    }

    public static void i(Tag tag, String message) {
        if (BuildConfig.DEBUG) {
            Logger.i(tag.toTag(), message);
        }
    }

    public static void v(Tag tag, String message) {
        if (BuildConfig.DEBUG) {
            Logger.v(tag.toTag(), message);
        }
    }

    public static void w(Tag tag, String message) {
        if (BuildConfig.DEBUG) {
            Logger.w(tag.toTag(), message);
        }
    }

    public static String makeLogMessage(String tag, String message) {
        if (StringUtil.isEmpty(tag) == false) {
            return String.format(Locale.US, LOG_FORMAT, tag, message);
        }
        return message;
    }

    //======================================================================
    // Private Methods
    //======================================================================

    private static void javaLog(Tag tag, String message) {
        System.out.println(tag.toString() + " : " + message);
    }
}
