package com.ibook.barcode.util.log;

/*
 * @author lsh
 * @since 2017. 5. 17.
*/
public enum Tag {
    ACTIVITY_CYCLE,
    FRAGMENT_CYCLE,
    NETWORK_REQUEST,
    NETWORK_RESPONSE,
    NETWORK_RESPONSE_RESULT,
    NETWORK_ERROR,
    DEFAULT;

    //======================================================================
    // Override Methods
    //======================================================================

    @Override
    public String toString() {
        return super.toString();
    }

    //======================================================================
    // Public Methods
    //======================================================================

    public String toTag() {
        return toString();
    }
}
