package com.ibook.barcode.util.toast;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.StringRes;
import android.view.Gravity;
import android.widget.Toast;

import com.ibook.barcode.util.Objects;

import java.util.concurrent.TimeUnit;

import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/*
 * @author lsh
 * @since 2017. 5. 17.
*/
public class AppToast {

    //======================================================================
    // Constants
    //======================================================================

    private final static long MIN_SHOW_TIME = 1000;

    private static final AppToast INSTANCE = new AppToast();

    //======================================================================
    // Variable
    //======================================================================

    private Toast mToast;

    private Callback mCallback;

    private Subscription mSubscription;

    private long mCurrentTime;

    //======================================================================
    // Constructor
    //======================================================================

    @Deprecated
    private AppToast() {
        // Nothing
    }

    @SuppressLint("ShowToast")
    public static void init(Context context) {
        if (INSTANCE.mToast == null) {
            INSTANCE.mToast = Toast.makeText(context, "", Toast.LENGTH_SHORT);
            INSTANCE.mToast.setGravity(Gravity.CENTER, 0, 0);
        }
    }

    public static AppToast with() {
        return INSTANCE;
    }

    //======================================================================
    // Public Methods
    //======================================================================

    public void showDefaultShortMessage(@StringRes int text) {
        showMessageInternal(getString(text), Toast.LENGTH_SHORT, null);
    }

    public void showDefaultShortMessage(String message, AppToast.Callback callback) {
        showMessageInternal(message, Toast.LENGTH_SHORT, callback);
    }

    public void showDefaultShortMessage(String message) {
        showMessageInternal(message, Toast.LENGTH_SHORT, null);
    }

    public void showDefaultLongMessage(@StringRes int text) {
        showMessageInternal(getString(text), Toast.LENGTH_LONG, null);
    }

    public void showDefaultLongMessage(String message) {
        showMessageInternal(message, Toast.LENGTH_LONG, null);
    }

    public void showMessage(@StringRes int text, int duration) {
        showMessageInternal(getString(text), duration, null);
    }

    public void showMessage(@NonNull String text, int duration) {
        showMessageInternal(text, duration, null);
    }

    public void showMessage(@StringRes int text, int duration, AppToast.Callback callback) {
        showMessageInternal(getString(text), duration, callback);
    }

    public void showMessage(@NonNull String text, int duration, AppToast.Callback callback) {
        showMessageInternal(text, duration, callback);
    }

    public boolean isToastShow() {
        try {
            final long minTime = System.currentTimeMillis() - mCurrentTime;
            return minTime <= MIN_SHOW_TIME || mToast.getView().isShown();
        } catch (Exception e) {
            return false;
        }
    }

    public void cancel() {
        if (mToast != null && isToastShow() == true) {
            mToast.cancel();
        }
        stopThread();
    }

    public void release(){
        cancel();
        mToast = null;
        if (mSubscription != null){
            mSubscription.unsubscribe();
            mSubscription = null;
        }
        mCallback = null;
    }

    //======================================================================
    // Private Methods
    //======================================================================

    private String getString(@StringRes int text) {
        try {
            return Objects.requireNonNull(getContext()).getString(text);
        } catch (Exception e) {
            // Nothing
        }
        return "";
    }

    private Context getContext() {
        try {
            return mToast.getView().getContext();
        } catch (Exception e) {
            return null;
        }
    }

    private void showMessageInternal(String text, int duration, AppToast.Callback callback) {
        if (isToastShow() == true) {
            // mToast.cancel();
            stopThread();
        }

        if (callback != null) {
            mCallback = callback;
        }

        mToast.setText(text);
        mToast.setDuration(duration);
        mToast.show();

        startThread();
    }

    private void startThread() {
        mCurrentTime = System.currentTimeMillis();
        mSubscription = Observable.interval(100, TimeUnit.MILLISECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.newThread())
                .subscribe(new Subscriber<Long>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                    }

                    @Override
                    public void onNext(Long s) {
                        final boolean show = isToastShow();
                        if (show == false) {
                            if (mCallback != null) {
                                mCallback.hide();
                                mCallback = null;
                            }
                            unsubscribe();
                        }
                    }
                });
    }

    private void stopThread() {
        if (mSubscription != null) {
            mSubscription.unsubscribe();
        }
    }

    //======================================================================
    // Callback
    //======================================================================

    public interface Callback {
        void hide();
    }
}
