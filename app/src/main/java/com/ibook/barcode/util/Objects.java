package com.ibook.barcode.util;

import com.starnation.util.StringUtil;
import com.starnation.util.validator.CollectionValidator;

import java.util.ArrayList;
import java.util.List;

/*
 * @author lsh
 * @since 2017. 5. 17.
*/
public final class Objects {

    //======================================================================
    // Public Methods
    //======================================================================

    public static <T> T requireNonNull(T obj) throws Exception {
        return requireNonNullInternal(obj);
    }

    public static <T extends String> T requireNonNull(T obj) throws Exception {
        return requireNonNullInternal(obj);
    }

    public static <T extends ArrayList> T requireNonNull(T obj, int position) throws Exception {
        return requireNonNullInternal(obj, position);
    }

    public static <T extends ArrayList> T requireNonNull(T obj) throws Exception {
        return requireNonNullInternal(obj, 0);
    }

    public static <T extends List> T requireNonNull(T obj, int position) throws Exception {
        return requireNonNullInternal(obj, position);
    }

    public static <T extends List> T requireNonNull(T obj) throws Exception {
        return requireNonNullInternal(obj, 0);
    }

    //======================================================================
    // Private Methods
    //======================================================================

    private static <T> T requireNonNullInternal(T obj) throws Exception {
        if (obj == null)
            throw new NullPointerException("Object null");
        return obj;
    }

    private static <T extends String> T requireNonNullInternal(T obj) throws Exception {
        if (StringUtil.isEmpty(obj) == true) {
            throw new NullPointerException("String null or empty");
        }
        return obj;
    }

    private static <T extends ArrayList> T requireNonNullInternal(T obj, int position) throws Exception {
        if (CollectionValidator.isValid(obj, position) == false) {
            requireNonNullInternal(obj);
            throw new IndexOutOfBoundsException("Collection size " + obj.size() + " in to position " + position);
        }
        return obj;
    }

    private static <T extends List> T requireNonNullInternal(T obj, int position) throws Exception {
        if (CollectionValidator.isValid(obj, position) == false) {
            requireNonNullInternal(obj);
            throw new IndexOutOfBoundsException("Collection size " + obj.size() + " in to position " + position);
        }
        return obj;
    }
}
