package com.ibook.barcode.app.fragment.common;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.view.ContextThemeWrapper;

import com.ibook.barcode.app.fragment.BaseDialogFragment;
import com.ibook.barcode.app.presenter.Presenter;
import com.ibook.barcode.app.AppTheme;


/*
 * @author lsh
 * @since 2017. 5. 17.
*/
public abstract class BottomDialogFragment<P extends Presenter> extends BaseDialogFragment<P> {

    //======================================================================
    // Override Methods
    //======================================================================

    @SuppressWarnings("RestrictedApi")
    @NonNull
    @Override
    public final Dialog onCreateDialog(Bundle savedInstanceState) {
        ContextThemeWrapper themeWrapper = new ContextThemeWrapper(getActivity(), AppTheme.with().getTheme());
        return new Dialog(themeWrapper, AppTheme.with().getBottomDialogTheme());
    }
}
