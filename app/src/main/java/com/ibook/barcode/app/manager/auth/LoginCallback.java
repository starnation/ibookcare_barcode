package com.ibook.barcode.app.manager.auth;

import com.ibook.barcode.network.model.user.LoginResult;
import com.ibook.barcode.network.response.BaseResponse;
import com.starnation.okhttp.JsonRequest;

import java.io.IOException;

/*
 * @author lsh
 * @since 2016. 5. 11.
*/
public interface LoginCallback extends JsonRequest.OnStatusListener {

    void onLoginSuccess(BaseResponse<LoginResult> response);

    void onLoginError(IOException ioe, String message);
}
