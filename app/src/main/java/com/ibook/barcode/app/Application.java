package com.ibook.barcode.app;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;

import com.ibook.barcode.app.manager.auth.UserSession;
import com.ibook.barcode.util.toast.AppToast;
import com.starnation.glide.BitmapImageViewTarget;
import com.starnation.util.validator.CollectionValidator;

import java.util.concurrent.CopyOnWriteArrayList;

/*
 * @author lsh
 * @since 2017. 5. 16.
*/
public final class Application extends android.app.Application {

    //======================================================================
    // Variables
    //======================================================================

    private static Application sContext;

    //======================================================================
    // Override Methods
    //======================================================================

    @Override
    public void onCreate() {
        super.onCreate();
        sContext = this;
        BitmapImageViewTarget.setTagId("bookmac".hashCode());
        AppToast.init(this);
        registerActivityLifecycleCallbacks(new ActivityLifecycleCallbacks() {
            @Override
            public void onAppTerminate() {
                release();
            }
        });
    }

    @Override
    protected void attachBaseContext(Context base) {
        try {
            super.attachBaseContext(base);
        } catch (RuntimeException ignored) {
        }
    }

    //======================================================================
    // Public Methods
    //======================================================================

    public static Application getContext() {
        return sContext;
    }

    //======================================================================
    // Private Methods
    //======================================================================

    private void release() {
        sContext = null;
    }

    //======================================================================
    // ActivityLifecycleCallbacks
    //======================================================================

    abstract static class ActivityLifecycleCallbacks implements android.app.Application.ActivityLifecycleCallbacks {

        private static CopyOnWriteArrayList<Activity> sActivities = new CopyOnWriteArrayList<>();

        public abstract void onAppTerminate();

        @Override
        public void onActivityCreated(Activity activity, Bundle savedInstanceState) {
            sActivities.add(activity);
        }

        @Override
        public void onActivityStarted(Activity activity) {

        }

        @Override
        public void onActivityResumed(Activity activity) {

        }

        @Override
        public void onActivityPaused(Activity activity) {

        }

        @Override
        public void onActivityStopped(Activity activity) {

        }

        @Override
        public void onActivitySaveInstanceState(Activity activity, Bundle outState) {

        }

        @Override
        public void onActivityDestroyed(Activity activity) {
            sActivities.remove(activity);
            if (CollectionValidator.isValid(sActivities) == false) {
                release();
                onAppTerminate();
            }
        }

        //======================================================================
        // Private Methods
        //======================================================================

        private void release() {
            UserSession.with().release();
            AppToast.with().release();
        }
    }
}
