package com.ibook.barcode.app.model.eventbus;

import com.ibook.barcode.app.model.bundle.EventBundle;

/*
 * @author lsh
 * @since 17. 5. 16.
*/
public final class Event extends com.starnation.eventbus.Event<EventKey> {

    //======================================================================
    // Variables
    //======================================================================

    private EventBundle mEventBundle = EventBundle.getBundle();

    private EventKey mEventKey = EventKey.DEFAULT;

    //======================================================================
    // Constructor
    //======================================================================

    public Event(EventBundle bundle) {
        this(EventKey.DEFAULT, bundle);
    }

    public Event(EventKey eventKey, EventBundle bundle) {
        super(eventKey, bundle == null ? null : bundle.fromBundle());
        if (bundle != null) {
            mEventBundle.setDataBundle(bundle.fromBundle());
        }
        mEventKey = eventKey;
    }

    public Event(EventKey eventKey) {
        this(eventKey, null);
    }

    //======================================================================
    // Public Methods
    //======================================================================

    public EventKey getEventKey() {
        return mEventKey;
    }

    public EventBundle getEventBundle() {
        return mEventBundle;
    }
}
