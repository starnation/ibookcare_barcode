package com.ibook.barcode.app.manager.preference;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.StringRes;

import com.starnation.android.content.preference.SharedPreference;
import com.starnation.util.StringUtil;

/*
 * @author lsh
 * @since 2016. 4. 16.
*/
class Base {

    final SharedPreference mPreference;

    public void clear(){
        if (mPreference != null){
            mPreference.clear();
        }
    }

    public Base(SharedPreference preference) {
        mPreference = preference;
    }

    public SharedPreference preference() {
        return mPreference;
    }

    public boolean save() {
        return isValid() != false && mPreference.save();
    }

    boolean remove(String key) {
        return isValid() != false && mPreference.remove(key);
    }

    Base putBoolean(String key, boolean value) {
        if (isValid() == true) {
            mPreference.putBoolean(key, value);
        }
        return this;
    }

    Base putInteger(String key, int value) {
        if (isValid() == true) {
            mPreference.putInteger(key, value);
        }
        return this;
    }

    Base putLong(String key, long value) {
        if (isValid() == true) {
            mPreference.putLong(key, value);
        }
        return this;
    }

    Base putFloat(String key, float value) {
        if (isValid() == true) {
            mPreference.putFloat(key, value);
        }
        return this;
    }

    Base putString(String key, String value) {
        if (isValid() == true) {
            mPreference.putString(key, value);
        }
        return this;
    }

    String getString(String key) {
        return getString(key, null);
    }

    String getString(String key, String value) {
        if (isValid() == true) {
            return mPreference.getString(key, StringUtil.textTrim(value));
        }
        return null;
    }

    boolean getBoolean(String key, boolean value) {
        return isValid() == true && mPreference.getBoolean(key, value);
    }

    int getInteger(String key, int value) {
        if (isValid() == true) {
            return mPreference.getInteger(key, value);
        }
        return 0;
    }

    long getLong(String key, long value) {
        if (isValid() == true) {
            return mPreference.getLong(key, value);
        }
        return 0;
    }

    float getFloat(String key, float value) {
        if (isValid() == true) {
            return mPreference.getFloat(key, value);
        }
        return 0;
    }

    boolean contains(String key) {
        return isValid() == true && mPreference.contains(key);
    }

    boolean isValid() {
        return mPreference != null;
    }

    public String getKey(@NonNull Context context, @StringRes int key){
        return context.getString(key);
    }
}
