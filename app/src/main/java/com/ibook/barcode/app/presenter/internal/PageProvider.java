package com.ibook.barcode.app.presenter.internal;

import android.os.Bundle;
import android.util.SparseArray;

import com.ibook.barcode.util.log.AppLogger;
import com.ibook.barcode.util.log.Tag;
import com.starnation.android.os.Bundles;
import com.starnation.android.os.parcel.ParcelUtil;
import com.starnation.android.os.parcel.SparseArrayParcelable;
import com.starnation.util.validator.CollectionValidator;

import java.util.ArrayList;

/*
 * @author lsh
 * @since 2017. 5. 17.
*/
final class PageProvider {

    //======================================================================
    // Constants
    //======================================================================

    final static int DEFAULT_KEY = Integer.MIN_VALUE;

    final static String KEY_RECYCLERVIEW_PAGE_DATA = "KEY_RECYCLERVIEW_PAGE_DATA";

    final static String KEY_RECYCLERVIEW_PAGE_LIST = "KEY_RECYCLERVIEW_PAGE_LIST";

    final static String KEY_RECYCLERVIEW_PAGE_KEY = "KEY_RECYCLERVIEW_PAGE_KEY";

    //======================================================================
    // Variables
    //======================================================================

    @SuppressWarnings("unchecked")
    private SparseArrayParcelable<PageInfo> mPageInfo = new SparseArrayParcelable(new SparseArray<PageInfo>());

    @SuppressWarnings("unchecked")
    private SparseArrayParcelable<ArrayList> mPageList = new SparseArrayParcelable<>(new SparseArray<ArrayList>());

    private int mPageKey = DEFAULT_KEY;

    private String mLogTag;

    //======================================================================
    // Constructor
    //======================================================================

    public PageProvider() {
        // Nothing
    }

    //======================================================================
    // Public Methods
    //======================================================================

    public int getPageKey() {
        return mPageKey;
    }

    public void updatePageKey(int pageKey) {
        mPageKey = pageKey;
    }

    public void init(boolean firstPage) {
        getPageInfo().init(firstPage);
    }

    public int getPage() {
        return getPageInfo().getPage();
    }

    public void setPage(int page) {
        getPageInfo().setPage(page);
    }

    public boolean isUpdate() {
        return getPageInfo().isUpdate();
    }

    public void setUpdate(boolean update) {
        getPageInfo().setUpdate(update);
    }

    public void complete() {
        getPageInfo().complete();
    }

    private void nextPage() {
        getPageInfo().nextPage();
    }

    public boolean isFirstPage() {
        return getPageInfo().isFirstPage();
    }

    public Object getItem(int position) {
        ArrayList list = getList();
        if (CollectionValidator.isValid(list, position) == true) {
            return list.get(position);
        }
        return null;
    }

    public boolean remove(Object o) {
        ArrayList list = getList();
        if (CollectionValidator.isValid(list) == true) {
            return list.remove(o);
        }
        return false;
    }

    public void remove(int position) {
        ArrayList list = getList();
        if (CollectionValidator.isValid(list, position) == true) {
            list.remove(position);
        }
    }

    public void clear() {
        getList().clear();
    }

    public void clearAll() {
        mPageInfo.getParcel().clear();
        mPageList.getParcel().clear();
    }

    public int size() {
        return getList().size();
    }

    public boolean isListUse() {
        return CollectionValidator.isValid(getList());
    }

    public boolean isListUse(int key) {
        return CollectionValidator.isValid(getList(key));
    }

    @SuppressWarnings("unchecked")
    public void addPageAllList(ArrayList<?> list) {
        setUpdate(CollectionValidator.isValid(list));
        if (isFirstPage() == true) {
            if (isListUse() == true) {
                clear();
                setUpdate(true);
            }
        }
        if (CollectionValidator.isValid(list) == true) {
            ArrayList pageList = getList();
            if (pageList != null) {
                pageList.addAll(list);
            }
        }
    }

    public void release() {
        //Modify 15. 10. 8. lsh mList 는 clear 시키면 안된다 reference 가 연결 되어 있어서 bundle 에 저장한겄도 clear 된다.
        getPageInfo().init(true);
        mPageInfo.getParcel().clear();
        mPageInfo = null;

        mLogTag = null;

        mPageList.getParcel().clear();
        mPageList = null;
    }

    public PageInfo getPageInfo() {
        return getPageInfoInternal(mPageKey);
    }

    public ArrayList getList() {
        return getListInternal(mPageKey);
    }

    public ArrayList getList(int key) {
        return getListInternal(key);
    }

    //======================================================================
    // Methods
    //======================================================================

    void setLogTag(String logTag) {
        mLogTag = logTag;
    }

    void save(Bundle outState) {
        log(mLogTag, "saveInstanceState");

        ParcelUtil.putParcelable(outState, KEY_RECYCLERVIEW_PAGE_DATA, mPageInfo);
        ParcelUtil.putParcelable(outState, KEY_RECYCLERVIEW_PAGE_LIST, mPageList);

        if (mPageKey != DEFAULT_KEY) {
            Bundles.put(outState, KEY_RECYCLERVIEW_PAGE_KEY, mPageKey);
        }
    }

    void restore(Bundle savedInstanceState) {
        SparseArrayParcelable<PageInfo> pageInfo = ParcelUtil.getParcelable(savedInstanceState, KEY_RECYCLERVIEW_PAGE_DATA);
        if (pageInfo != null) {
            mPageInfo = pageInfo;
        }

        SparseArrayParcelable<ArrayList> pageList = ParcelUtil.getParcelable(savedInstanceState, KEY_RECYCLERVIEW_PAGE_LIST);
        if (pageList != null) {
            mPageList = pageList;
        }

        mPageKey = Bundles.getInt(savedInstanceState, KEY_RECYCLERVIEW_PAGE_KEY, DEFAULT_KEY);

        log(mLogTag, "restore");
    }

    //======================================================================
    // Private Methods
    //======================================================================

    private synchronized PageInfo getPageInfoInternal(int key) {
        PageInfo getList = mPageInfo.getParcel().get(key);
        if (getList == null) {
            getList = new PageInfo();
            mPageInfo.getParcel().put(key, getList);
        }
        return getList;
    }

    private synchronized ArrayList getListInternal(int key) {
        ArrayList list = mPageList.getParcel().get(key);
        if (list == null) {
            list = new ArrayList();
            mPageList.getParcel().put(key, list);
        }
        return list;
    }

    private void log(String logTag, String title) {
        AppLogger.d(Tag.FRAGMENT_CYCLE, AppLogger.makeLogMessage(logTag, title));

        int count = mPageInfo.getParcel().size();
        for (int i = 0; i < count; i++) {
            int key = mPageInfo.getParcel().keyAt(i);
            PageInfo pageInfo = mPageInfo.getParcel().get(key);
            AppLogger.i(Tag.FRAGMENT_CYCLE, AppLogger.makeLogMessage(logTag, "pageInfo : " + key + " info : " + pageInfo));
        }

        count = mPageList.getParcel().size();
        for (int i = 0; i < count; i++) {
            int key = mPageList.getParcel().keyAt(i);
            ArrayList list = mPageList.getParcel().get(key);
            AppLogger.i(Tag.FRAGMENT_CYCLE, AppLogger.makeLogMessage(logTag, "pageList : " + key + " size : " + CollectionValidator.getSize(list)));
        }
    }
}
