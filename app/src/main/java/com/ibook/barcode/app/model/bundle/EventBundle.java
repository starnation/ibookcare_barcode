package com.ibook.barcode.app.model.bundle;

import android.os.Bundle;


/*
 * @author lsh
 * @since 17. 5. 17.
*/
public class EventBundle extends SupportBundle {

    //======================================================================
    // Constructor
    //======================================================================

    @SuppressWarnings("deprecation")
    public EventBundle() {
        // Nothing
    }

    @SuppressWarnings("unchecked")
    public static EventBundle asBundle(Bundle bundle) {
        EventBundle supportBundle = new EventBundle();
        try {
            supportBundle.setDataBundle(bundle);
            return supportBundle;
        } catch (Exception e) {
            // Nothing
        }
        return supportBundle;
    }

    @SuppressWarnings("unchecked")
    public static <T> T getBundle() {
        return getBundle(EventBundle.class, null, null);
    }

    @SuppressWarnings("unchecked")
    public static <T> T getBundle(Class<? extends SupportBundle> classObject, Bundle bundle, Object target) {
        try {
            SupportBundle supportBundle = classObject.newInstance();
            if (bundle != null) {
                supportBundle.setDataBundle(bundle, target);
            }
            return (T) supportBundle;
        } catch (Exception e) {
            // Nothing
        }
        return null;
    }

    //======================================================================
    // Public Methods
    //======================================================================

    public EventBundle scanResult(String displayValue) {
        return putInternal(getMethodName(new Object() {
        }), displayValue);
    }

    public String getScanResult() {
        return getInternal(getMethodName(new Object() {
        }));
    }

    public EventBundle scanResultFormat(String format) {
        return putInternal(getMethodName(new Object() {
        }), format);
    }

    public String getScanResultFormat() {
        return getInternal(getMethodName(new Object() {
        }));
    }
}