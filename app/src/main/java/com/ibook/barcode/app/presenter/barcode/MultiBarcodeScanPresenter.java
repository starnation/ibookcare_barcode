package com.ibook.barcode.app.presenter.barcode;

import android.support.v4.app.Fragment;

import com.ibook.barcode.app.model.barcode.BarcodeCollection;
import com.ibook.barcode.app.model.barcode.BarcodeWrapper;
import com.ibook.barcode.app.presenter.Presenter;
import com.ibook.barcode.network.model.book.BookInfo;
import com.ibook.barcode.network.model.book.BookItem;
import com.ibook.barcode.network.parameter.HeaderParameter;
import com.ibook.barcode.network.request.Requests;
import com.ibook.barcode.network.response.BaseResponse;
import com.ibook.barcode.util.log.AppLogger;
import com.starnation.okhttp.JsonRequest;
import com.starnation.util.validator.CollectionValidator;

import java.util.ArrayList;

import rx.Observable;

/*
 * @author lsh
 * @since 2017. 5. 17.
*/
public abstract class MultiBarcodeScanPresenter extends Presenter {

    //======================================================================
    // Constance
    //======================================================================

    public static final long INTERVAL = 2000;

    //======================================================================
    // Enum
    //======================================================================

    public enum ItemState {
        STATE_NEW,
        STATE_INSERT,
        STATE_EXIST,
        STATE_CHILD
    }

    //======================================================================
    // Variables
    //======================================================================

    private long mBarcodeInterval = INTERVAL;

    private long mLastBarcodeTime;

    private ArrayList<BarcodeWrapper> mItemList = new ArrayList<>();

    //======================================================================
    // Constructor
    //======================================================================

    public MultiBarcodeScanPresenter(Fragment fragment) {
        super(fragment);
    }

    //======================================================================
    // Public Methods
    //======================================================================

    public ArrayList<BookItem> getBookItemList() {
        ArrayList<BookItem> list = new ArrayList<>();
        for (BarcodeWrapper wrapper : mItemList) {
            list.add(wrapper.getBookItem());
        }
        return list;
    }

    public ArrayList<BarcodeWrapper> getItemList() {
        return mItemList;
    }

    public Observable onIsbnResponse(BookItem bookItem) {
        return onIsbnResponse(mItemList, bookItem);
    }

    @SuppressWarnings("PointlessBooleanExpression")
    public BarcodeWrapper getLastItem() {
        int lastIndex = CollectionValidator.getSize(mItemList) - 1;
        if (CollectionValidator.isValid(mItemList, lastIndex) == true) {
            return mItemList.get(lastIndex);
        }
        return null;
    }

    @SuppressWarnings("PointlessBooleanExpression")
    public void updateLastItemCollection(int bookCount, ArrayList<String> bookList) {
        BarcodeWrapper wrapper = getLastItem();
        if (wrapper != null) {
            wrapper.updateCollectionType(bookCount, bookList);
        }
    }

    public int getListItemSize() {
        return CollectionValidator.getSize(mItemList);
    }

    @SuppressWarnings("PointlessBooleanExpression")
    public int toDisplayCountItemSize() {
        int size = 0;
        for (BarcodeWrapper wrapper : mItemList) {
            if (wrapper.isCollection() == true) {
                size += wrapper.getBarcodeCollection().getBookCount();
            } else {
                size++;
            }
        }
        return size;
    }

    @SuppressWarnings("PointlessBooleanExpression")
    public BarcodeWrapper getItem(int position) {
        if (CollectionValidator.isValid(mItemList, position) == true) {
            return mItemList.get(position);
        }
        return null;
    }

    public void removeItem(BarcodeWrapper wrapper) {
        try {
            if (wrapper != null) {
                mItemList.remove(wrapper);
            } else {
                mItemList.clear();
            }
        } catch (Exception e) {
            AppLogger.printStackTrace(e);
        }
    }

    @SuppressWarnings("PointlessBooleanExpression")
    public BarcodeWrapper findBookIsbn(String isbn) {
        if (CollectionValidator.isValid(mItemList) == true) {
            for (BarcodeWrapper wrapper : mItemList) {
                try {
                    BookInfo bookInfo = wrapper.getBookItem().getBookInfo();
                    if (bookInfo != null && bookInfo.getIsbn13().equals(isbn) == true) {
                        return wrapper;
                    }
                    BarcodeCollection barcodeCollection = wrapper.getBarcodeCollection();
                    if (barcodeCollection != null) {
                        if (barcodeCollection.isbnContains(isbn) == true) {
                            return wrapper;
                        }
                    }
                } catch (Exception e) {
                    //Nothing
                }
            }
        }
        return null;
    }

    @SuppressWarnings("PointlessBooleanExpression")
    public BarcodeWrapper findBookCode(BookItem bookItem) {
        for (BarcodeWrapper wrapper : mItemList) {
            if (wrapper.equals(bookItem) == true) {
                return wrapper;
            }
        }
        return null;
    }

    @SuppressWarnings("PointlessBooleanExpression")
    public BarcodeWrapper findSetCode(BookItem bookItem) {
        for (BarcodeWrapper wrapper : mItemList) {
            if (wrapper.collectionEquals(bookItem) == true) {
                return wrapper;
            }
        }
        return null;
    }

    public void requestIsbn(final String isbn, String format, JsonRequest.OnStatusListener listener, JsonRequest.OnResponseListener<BaseResponse<BookItem>> responseListener) {
        Requests.getIsbn(isbn)
                .duplicateRequest(false)
                .header(new HeaderParameter()
                        .userSeq(getUserSeq())
                        .childSeq(getChildSeq())
                        .selfYn(format.equals("Input Format")))
                .statusListener(listener)
                .execute(new OnResponseListenerWrapper<BaseResponse<BookItem>>(responseListener) {
                    @Override
                    public void onResult(BaseResponse<BookItem> arrayListBaseResponse) {
                        BookItem bookItem = arrayListBaseResponse.getResult();
                        bookItem.setChecked(true);
                        bookItem.getBookInfo().setIsbn13(isbn);
                    }
                });
    }

    public long getBarcodeInterval() {
        return mBarcodeInterval;
    }

    public void setBarcodeInterval(long barcodeInterval) {
        mBarcodeInterval = barcodeInterval;
    }

    public long getLastBarcodeTime() {
        return mLastBarcodeTime;
    }

    public void setLastBarcodeTime(long lastBarcodeTime) {
        mLastBarcodeTime = lastBarcodeTime;
    }

    //======================================================================
    // Protected Methods
    //======================================================================

    /**
     * Fragment 에서 call함
     *
     * @param list
     * @param bookItem
     *
     * @return
     */
    protected abstract Observable onIsbnResponse(ArrayList<BarcodeWrapper> list, BookItem bookItem);
}
