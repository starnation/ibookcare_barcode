package com.ibook.barcode.app.viewholder;

import android.graphics.drawable.Drawable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.view.ViewGroup;

import com.ibook.barcode.R;
import com.ibook.barcode.app.model.barcode.BarcodeCollection;
import com.ibook.barcode.app.model.barcode.BarcodeWrapper;
import com.ibook.barcode.databinding.ViewHolderBarcodeBookBinding;
import com.ibook.barcode.widget.recyclerview.viewholder.BaseRecyclerViewHolder;
import com.starnation.android.view.ViewUtil;
import com.starnation.widget.BaseImageView;

import butterknife.OnClick;

/*
 * @author lsh
 * @since 2017. 5. 17.
*/
public final class BarcodeBookViewHolder extends BaseRecyclerViewHolder<BarcodeWrapper> {

    //======================================================================
    // Constructor
    //======================================================================

    public BarcodeBookViewHolder(Fragment fragment, ViewGroup viewGroup) {
        super(fragment, viewGroup, R.layout.view_holder_barcode_book);
    }

    //======================================================================
    // Override Methods
    //======================================================================

    @Override
    public ViewHolderBarcodeBookBinding viewDataBinding() {
        return (ViewHolderBarcodeBookBinding) super.viewDataBinding();
    }

    @Override
    public void onRefresh(BarcodeWrapper wrapper) {
        super.onRefresh(wrapper);
        if (wrapper != null) {
            ViewUtil.show(viewDataBinding().textviewTitle);
            viewDataBinding().imageviewBook.setLargeDefaultImage(ContextCompat.getColor(getContext(), R.color.blue_200));
            viewDataBinding().imageviewBook.setImageUrl(wrapper.getCoverAladin(), new BaseImageView.Callback() {
                @Override
                public boolean onResource(Drawable drawable) {
                    if (drawable != null) {
                        ViewUtil.hide(viewDataBinding().textviewTitle);
                    }
                    return false;
                }
            });

            viewDataBinding().textviewTitle.setText(wrapper.getBookItem().getBookInfo().getTitle());

            reFreshCollection();
            reFreshBookCount();
        }
    }

    //======================================================================
    // Methods
    //======================================================================

    @OnClick({R.id.imageview_book,
            R.id.button_close,
            R.id.textview_read_count})
    void onClick(View view) {
        sendEvent(view.getId());
    }

    //======================================================================
    // Private Methods
    //======================================================================

    private void reFreshCollection() {
        if (getItem().isCollection() == true) {
            ViewUtil.show(viewDataBinding().textviewCollectionBookCount);

            BarcodeCollection collection = getItem().getBarcodeCollection();
            viewDataBinding().textviewCollectionBookCount.setText(Integer.toString(collection.getBookCount()));
        } else {
            ViewUtil.hide(viewDataBinding().textviewCollectionBookCount);
        }
    }

    private void reFreshBookCount() {
        if (getItem().getAppendCount() > 0) {
            ViewUtil.show(viewDataBinding().textviewReadCount);

            viewDataBinding().textviewReadCount.setText(Integer.toString(getItem().getAppendCount()));
        } else {
            ViewUtil.hide(viewDataBinding().textviewReadCount);
        }
    }
}
