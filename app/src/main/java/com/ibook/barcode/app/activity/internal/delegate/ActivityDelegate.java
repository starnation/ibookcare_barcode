package com.ibook.barcode.app.activity.internal.delegate;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.NonNull;

import com.ibook.barcode.app.manager.auth.UserSession;
import com.ibook.barcode.app.model.bundle.ActivityBundle;
import com.ibook.barcode.content.BundleKey;
import com.ibook.barcode.util.log.AppLogger;
import com.ibook.barcode.util.log.Tag;
import com.starnation.android.app.activity.AppCompatActivity;
import com.starnation.android.app.activity.internal.helper.CacheInstanceHelper;
import com.starnation.android.util.PermissionChecker;

/*
 * @author lsh
 * @since 17. 4. 14.
*/
public class ActivityDelegate implements ActivityCycle {

    //======================================================================
    // Constants
    //======================================================================

    private final static String LOG_FORMAT = "[%s]: %s";

    //======================================================================
    // Variables
    //======================================================================

    Activity mActivity;

    ActivityBundle mActivityBundle;

    //======================================================================
    // Constructor
    //======================================================================

    public ActivityDelegate(@NonNull Activity activity) {
        mActivity = activity;
    }

    //======================================================================
    // Override Methods
    //======================================================================

    @Override
    public void onCreate(Bundle savedInstanceState) {
        mActivityBundle = ActivityBundle.createSupportBundle(getActivity().getIntent().getBundleExtra(BundleKey.ACTIVITY_BUNDLE_DATA));
        UserSession.with().onRestoreInstance(savedInstanceState);
        cycle("onCreate -> savedInstanceState : " + savedInstanceState);
    }

    @Override
    public void onCreate(Bundle savedInstanceState, PersistableBundle persistentState) {
        cycle("onCreate -> savedInstanceState : " + savedInstanceState + " persistentState : " + persistentState);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        UserSession.with().onSaveInstance(outState);
        cycle("onSaveInstanceState -> outState : " + outState);
    }

    @Override
    public void onSaveInstanceState(Bundle outState, PersistableBundle outPersistentState) {
        cycle("onSaveInstanceState -> outState : " + outState + " persistentState : " + outPersistentState);
    }

    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        cycle("onRestoreInstanceState -> savedInstanceState : " + savedInstanceState);
    }

    @Override
    public void onStart() {
        cycle("onStart");
    }

    @Override
    public void onResume() {
        cycle("onResume");
    }

    @Override
    public void onPostResume() {
        cycle("onPostResume");
    }

    @Override
    public void onStop() {
        cycle("onStop");
    }

    @Override
    public void onDestroy() {
        cycle("onDestroy");
        mActivity = null;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        cycle("onActivityResult -> requestCode : " + requestCode + " requestCode : " + requestCode + " data : " + data);
    }

    //======================================================================
    // Public Methods
    //======================================================================

    public PermissionChecker getPermissionChecker() {
        return ((AppCompatActivity) mActivity).getPermissionChecker();
    }

    public CacheInstanceHelper getCacheInstanceHelper() {
        return ((AppCompatActivity) mActivity).getCacheInstanceHelper();
    }

    /**
     * 1회성 값 전달시 사용
     *
     * @return {{@link ActivityBundle}}
     */
    public ActivityBundle getActivityBundle() {
        return mActivityBundle;
    }

    //======================================================================
    // Methods
    //======================================================================

    void cycle(String message) {
        if (mActivity == null) {
            return;
        }
        String objectName = mActivity.getClass().getSimpleName();
        AppLogger.i(Tag.ACTIVITY_CYCLE, String.format(LOG_FORMAT, objectName, message));
    }

    Activity getActivity() {
        return mActivity;
    }
}
