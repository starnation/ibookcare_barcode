package com.ibook.barcode.app.fragment;

import android.app.Activity;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.ibook.barcode.app.activity.BaseAppCompatActivity;
import com.ibook.barcode.app.activity.internal.delegate.ActivityDelegate;
import com.ibook.barcode.app.model.bundle.FragmentBundle;
import com.ibook.barcode.app.model.eventbus.Event;
import com.ibook.barcode.app.presenter.Presenter;
import com.starnation.android.app.fragment.DialogFragment;

/*
 * @author lsh
 * @since 2016. 5. 8.
*/
public class BaseDialogFragment<P extends Presenter> extends DialogFragment<P, Event> implements Impl.SupportDialogFragment {

    //======================================================================
    // Variables
    //======================================================================

    @SuppressWarnings("unchecked")
    private final FragmentDelegate mDelegate = new FragmentDelegate(this);

    //======================================================================
    // Override Methods
    //======================================================================

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mDelegate.onCreate(savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mDelegate.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mDelegate.onDestroy();
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        mDelegate.setUserVisibleHint(isVisibleToUser);
    }

    @Override
    public void onNetworkStart() {
        mDelegate.showProgressDialog();
    }

    @Override
    public void onNetworkEnd() {
        mDelegate.hideProgressDialog();
    }

    @Override
    public void onEvent(Event event) {

    }

    @Nullable
    @Override
    public Toolbar getToolbar() {
        return mDelegate.getToolbar();
    }

    @SuppressWarnings("unchecked")
    @Override
    public FragmentDelegate.Config getConfig() {
        return mDelegate.getConfig();
    }

    @Override
    public FragmentDelegate.Config onDefaultCreateConfig() {
        return new FragmentDelegate.Config();
    }

    @Override
    public void onApplyConfig(@NonNull FragmentDelegate.Config config) {
    }

    @Override
    public boolean onBackPressed() {
        return true;
    }

    @Override
    public ActivityDelegate getActivityDelegate() {
        Activity activity = getActivity();
        if (activity instanceof BaseAppCompatActivity) {
            return ((BaseAppCompatActivity) activity).getActivityDelegate();
        }
        return null;
    }

    @Override
    public void onProgressBarCancel() {
        // Override
    }

    @Override
    public void setTitle(String title) {
        mDelegate.setTitle(title);
    }

    @Override
    public void onToolbar(@Nullable Toolbar toolbar) {
        if (toolbar != null && toolbar.getBackground() != null) {
            Drawable drawable = toolbar.getBackground();
            drawable.setAlpha(255);
        }
    }

    @Override
    public FragmentBundle getSupportArguments() {
        return mDelegate.getSupportArguments();
    }
}
