package com.ibook.barcode.app.model.bundle;

import android.os.Bundle;

/*
 * @author lsh
 * @since 17. 5. 16.
*/
public class ActivityBundle extends SupportBundle {

    //======================================================================
    // Constructor
    //======================================================================

    @SuppressWarnings("deprecation")
    public ActivityBundle() {
        // Nothing
    }

    @SuppressWarnings("unchecked")
    public static <T> T getBundle() {
        return getBundle(ActivityBundle.class, null, null);
    }

    @SuppressWarnings("unchecked")
    public static ActivityBundle createSupportBundle(Bundle bundle) {
        ActivityBundle activityBundle = getBundle();
        if (bundle != null) {
            activityBundle.setDataBundle((Bundle) bundle.clone());
        }
        return activityBundle;
    }
}
