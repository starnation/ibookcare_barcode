package com.ibook.barcode.app.fragment;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.ibook.barcode.R;
import com.ibook.barcode.app.model.bundle.FragmentBundle;
import com.ibook.barcode.util.log.AppLogger;
import com.ibook.barcode.util.log.Tag;
import com.ibook.barcode.widget.dialog.LoadingDialog;
import com.starnation.android.app.fragment.FragmentList;
import com.starnation.android.app.fragment.FragmentSupport;
import com.starnation.util.StringUtil;

/*
 * @author lsh
 * @since 2016. 4. 18.
*/
public class FragmentDelegate {

    //======================================================================
    // Variables
    //======================================================================

    private Toolbar mToolbar;

    private Fragment mFragment;

    private Config mConfig;

    private FragmentBundle mSupportArguments;

    private Impl.SupportFragment mSupportFragment;

    private Dialog mDialog;

    //======================================================================
    // Constructor
    //======================================================================

    public FragmentDelegate(@NonNull Fragment fragment) {
        mFragment = fragment;
        mSupportFragment = (Impl.SupportFragment) mFragment;
        mConfig = mSupportFragment.onDefaultCreateConfig();
    }

    @SuppressWarnings("unchecked")
    public void onCreate(Bundle savedInstanceState) {
        mSupportFragment.onApplyConfig(mConfig);
        cycle("onCreate -> savedInstanceState : " + savedInstanceState);
    }

    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        cycle("onViewCreated -> savedInstanceState : " + savedInstanceState);

        mToolbar = (Toolbar) view.findViewById(R.id.toolbar);
        if (mToolbar != null) {
            if (mFragment.getActivity() instanceof AppCompatActivity) {
                AppCompatActivity appCompatActivity = ((AppCompatActivity) mFragment.getActivity());

                appCompatActivity.setSupportActionBar(mToolbar);

                mSupportFragment.onToolbar(mToolbar);

                String title = mConfig.getTitle(mFragment.getContext());
                setTitle(title);

                applyTitleEnable(appCompatActivity, mConfig.isShowTitle());

                mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (mFragment instanceof FragmentSupport) {
                            ((FragmentSupport) mFragment).popBackStackImmediate();
                        }
                    }
                });
            }
        }
    }

    public void setUserVisibleHint(boolean userVisibleHint) {
        cycle("setUserVisibleHint : " + userVisibleHint);
    }

    @SuppressWarnings("EmptyCatchBlock")
    public void onDestroy() {
        cycle("onDestroy");
        mFragment = null;
        mSupportArguments = null;
        mConfig = null;
        mSupportFragment = null;
        releaseProgressDialog();
    }

    //======================================================================
    // Public Methods
    //======================================================================

    /**
     * {@link FragmentList#findFragment(int)}
     */
    public static Fragment findFragment(int key) {
        return com.starnation.android.app.fragment.Fragment.findFragment(key);
    }

    public void setTitle(String title) {
        if (mToolbar != null) {
            if (mFragment.getActivity() instanceof AppCompatActivity) {
                AppCompatActivity appCompatActivity = ((AppCompatActivity) mFragment.getActivity());
                ActionBar actionBar = appCompatActivity.getSupportActionBar();
                if (StringUtil.isEmpty(title) == false) {
                    if (actionBar != null) {
                        actionBar.setDisplayHomeAsUpEnabled(true);
                        actionBar.setTitle(title);
                    }
                    mToolbar.setTitle(title);
                }
            }
        }
    }

    public Toolbar getToolbar() {
        return mToolbar;
    }

    public Config getConfig() {
        return mConfig;
    }

    public FragmentBundle getSupportArguments() {
        if (mSupportArguments == null) {
            mSupportArguments = FragmentBundle.getBundle(FragmentBundle.class, mFragment.getArguments(), this);
        }
        if (mSupportArguments == null) {
            mSupportArguments = new FragmentBundle();
        }
        return mSupportArguments;
    }

    //======================================================================
    // Methods
    //======================================================================

    void releaseProgressDialog() {
        try {
            if (mDialog != null && mDialog.isShowing() == true) {
                mDialog.hide();
            }
            mDialog = null;
        } catch (Exception e) {
            // Nothing
        }
    }

    void showProgressDialog() {
        try {
            if (mDialog == null) {
                mDialog = LoadingDialog.create(mFragment.getContext());
                mDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialog) {
                        if (mSupportFragment != null) {
                            mSupportFragment.onProgressBarCancel();
                        }
                    }
                });
            }
            mDialog.show();
        } catch (Exception ignored) {

        }
    }

    void hideProgressDialog() {
        try {
            if (mDialog != null) {
                mDialog.dismiss();
                mDialog = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //======================================================================
    // Private Methods
    //======================================================================

    public static void applyTitleEnable(Activity activity, boolean enable) {
        if (activity instanceof AppCompatActivity) {
            AppCompatActivity appCompatActivity = (AppCompatActivity) activity;
            ActionBar actionBar = appCompatActivity.getSupportActionBar();
            if (actionBar != null) {
                actionBar.setDisplayShowTitleEnabled(enable);
            }
        }
    }

    private void cycle(String message) {
        if (mFragment == null) {
            return;
        }
        String objectName = mFragment.getClass().getSimpleName();
        AppLogger.i(Tag.FRAGMENT_CYCLE, AppLogger.makeLogMessage(objectName, message));
    }

    //======================================================================
    // Config
    //======================================================================

    public static class Config {

        boolean mShowTitle = true;

        @StringRes
        int mTitle;

        String mTitleStr;

        @StringRes
        int mLoadingMessage;

        public void setTitle(@StringRes int title) {
            mTitle = title;
        }

        public void setTitle(String title) {
            mTitleStr = title;
        }

        public String getTitle(Context context) {
            if (mTitle != 0) {
                return context.getString(mTitle);
            }
            if (StringUtil.isEmpty(mTitleStr) == false) {
                return mTitleStr;
            }
            return "";
        }

        public void setShowTitle(boolean showTitle) {
            mShowTitle = showTitle;
        }

        public boolean isShowTitle() {
            return mShowTitle;
        }

        //======================================================================
        // Methods
        //======================================================================

        String getLoadingMessage(Context context) {
            if (mLoadingMessage != 0) {
                return context.getString(mLoadingMessage);
            }
            return "";
        }
    }
}
