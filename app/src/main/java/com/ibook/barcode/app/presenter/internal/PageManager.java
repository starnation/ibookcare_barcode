package com.ibook.barcode.app.presenter.internal;

import android.os.Bundle;

import java.util.ArrayList;

/*
 * @author lsh
 * @since 15. 10. 8.
*/
public final class PageManager {

    //======================================================================
    // Variables
    //======================================================================

    private PageProvider mProvider = new PageProvider();

    //======================================================================
    // Public Methods
    //======================================================================

    public void setLogTag(String logTag) {
        mProvider.setLogTag(logTag);
    }

    public void saveInstanceState(Bundle outState) {
        mProvider.save(outState);
    }

    @SuppressWarnings("unchecked")
    public void restoreInstanceState(Bundle savedInstanceState) {
        mProvider.restore(savedInstanceState);
    }

    public int getPageKey() {
        return mProvider.getPageKey();
    }

    public void updatePageKey(int pageKey) {
        mProvider.updatePageKey(pageKey);
    }

    public void init(boolean firstPage) {
        mProvider.init(firstPage);
    }

    public boolean isFirstPage() {
        return mProvider.isFirstPage();
    }

    public int getPage() {
        return mProvider.getPage();
    }

    public void setPage(int page) {
        mProvider.setPage(page);
    }

    public ArrayList<?> getList() {
        return mProvider.getList();
    }

    public Object getItem(int position) {
        return mProvider.getItem(position);
    }

    public boolean isListUse() {
        return mProvider.isListUse();
    }

    public boolean isListUse(int key) {
        return mProvider.isListUse(key);
    }

    public boolean remove(Object item) {
        return mProvider.remove(item);
    }

    public void remove(int position) {
        mProvider.remove(position);
    }

    public void clear() {
        mProvider.clear();
        mProvider.getPageInfo().init(true);
    }

    public void clearAll() {
        mProvider.clearAll();
        mProvider.getPageInfo().init(true);
    }

    public int getSize() {
        return mProvider.size();
    }

    public void addAllPageList(ArrayList<?> list) {
        mProvider.addPageAllList(list);
    }

    public boolean isUpdate() {
        return mProvider.isUpdate();
    }

    public void complete() {
        mProvider.complete();
    }

    public void release() {
        mProvider.release();
        mProvider = null;
    }
}
