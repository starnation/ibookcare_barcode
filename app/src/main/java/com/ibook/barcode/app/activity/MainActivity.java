package com.ibook.barcode.app.activity;

import android.content.Intent;
import android.os.Bundle;

import com.ibook.barcode.BuildConfig;
import com.ibook.barcode.R;
import com.ibook.barcode.app.activity.internal.RequestPermission;
import com.ibook.barcode.app.manager.auth.Auth;
import com.ibook.barcode.app.manager.auth.LoginCallback;
import com.ibook.barcode.content.Intents;
import com.ibook.barcode.network.model.user.LoginResult;
import com.ibook.barcode.network.response.BaseResponse;
import com.ibook.barcode.util.log.AppLogger;
import com.ibook.barcode.util.log.Tag;
import com.ibook.barcode.util.toast.AppToast;
import com.starnation.android.util.PermissionChecker;

import java.io.IOException;

public class MainActivity extends BaseAppCompatActivity implements LoginCallback {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Modify 17. 5. 17. lsh 앱 중복 instance 생성 방지 코드
        if (!isTaskRoot()) {
            final Intent intent = getIntent();
            final String intentAction = intent.getAction();
            if (intent.hasCategory(Intent.CATEGORY_LAUNCHER) && intentAction != null && intentAction.equals(Intent.ACTION_MAIN)) {
                AppLogger.w(Tag.ACTIVITY_CYCLE, "Main Activity is not the root.  Finishing Main Activity instead of launching.");
                finish();
                return;
            }
        }

        setContentView(R.layout.activity_main);
        Auth.with(this).login(BuildConfig.ID, BuildConfig.PW, this);
    }

    @Override
    public void onLoginSuccess(BaseResponse<LoginResult> response) {
        RequestPermission.camera(getActivityDelegate().getPermissionChecker(), new PermissionChecker.OnSupportRequestPermissionsResultCallback() {
            @Override
            public void onResult(boolean[] grants, boolean[] showRequestPermissions) {
                Intents.goBarcode(MainActivity.this);
                finish();
            }
        });
    }

    @Override
    public void onLoginError(IOException ioe, String message) {
        AppToast.with().showDefaultLongMessage(message);
        finish();
    }

    @Override
    public void onNetworkStart() {

    }

    @Override
    public void onNetworkEnd() {

    }
}
