package com.ibook.barcode.app.manager.preference;

/*
 * @author lsh
 * @since 2016. 4. 16.
*/
public final class PreferenceKey {

    public final static String AUTO_LOGIN = "AUTO_LOGIN";

    public final static String ACCOUNT_TYPE = "ACCOUNT_TYPE";

    public final static String IBOOK_USER_EMAIL = "IBOOK_USER_EMAIL";

    public final static String IBOOK_USER_PASSWORD = "IBOOK_USER_PASSWORD";

    public final static String BOOKREGISTRY_LIBRARY_FIRST_VISIT = "BOOKREGISTRY_LIBRARY_FIRST_VISIT";

    public final static String BOOK_READING_SETTING_GUIDE = "BOOK_READING_SETTING_GUIDE";

    public final static String SELECT_CHILD = "SELECT_CHILD";

    public final static String PUSH_TOKEN = "PUSH_TOKEN";

    public final static String BADGE_COUNT = "BADGE_COUNT";

    public final static String PUSH_SERVER_REGISTER = "PUSH_SERVER_REGISTER";

    public final static String ACCESS_TOKEN = "ACCESS_TOKEN";

    public final static String FIRST_LOGIN_SUCCESS = "FIRST_LOGIN_SUCCESS";

    public static String getThemeKey(int childSeq) {
        return "THEME_" + childSeq;
    }

    public static String getOpenLibraryPublicKey(int childSeq) {
        return "OPEN_LIBRARY_" + childSeq;
    }

    public static final String APP_FIRST_RUN = "APP_FIRST_RUN";

    public static final String APP_SERVER_INFO = "APP_SERVER_INFO";

    public static final String APP_BOOK_REGISTER_READ_STATUS = "APP_BOOK_REGISTER_READ_STATUS";

    public static final String APP_TOOL_TIP_SEARCH_HINT = "APP_TOOL_TIP_SEARCH_HINT";

    public static final String APP_MAX_COACHING_COUNT = "APP_MAX_COACHING_COUNT";

    public static final String APP_MAX_BOOKSHELF_COUNT = "APP_MAX_BOOKSHELF_COUNT";
}
