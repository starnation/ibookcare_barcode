package com.ibook.barcode.app.manager.auth;

import android.content.Context;
import android.support.annotation.NonNull;

import com.ibook.barcode.app.manager.preference.AppPreference;
import com.ibook.barcode.network.exception.ServerResponseErrorException;
import com.ibook.barcode.network.model.user.LoginResult;
import com.ibook.barcode.network.model.user.User;
import com.ibook.barcode.network.parameter.HeaderParameter;
import com.ibook.barcode.network.request.Requests;
import com.ibook.barcode.network.response.BaseResponse;
import com.ibook.barcode.network.response.ErrorStatus;
import com.starnation.okhttp.JsonRequest;

import java.io.IOException;

import okhttp3.Request;

/*
 * @author lsh
 * @since 2016. 4. 15.
*/
public final class Auth {

    //======================================================================
    // Variables
    //======================================================================

    private final static Auth INSTANCE = new Auth();

    private AppPreference mAppPreference;

    //======================================================================
    // Constructor
    //======================================================================

    public static Auth with(@NonNull Context context) {
        if (INSTANCE.mAppPreference == null) {
            INSTANCE.mAppPreference = AppPreference.from(context);
        }
        return INSTANCE;
    }

    //======================================================================
    // Public Methods
    //======================================================================

    public static void logout() {
        INSTANCE.clearPreference();
    }

    public void login(String email, String password, LoginCallback callback) {
        Requests.postUserLogin()
                .header(createParameter()
                        .email(email)
                        .password(password))
                .statusListener(callback)
                .execute(new LoginResponseWrapper(AccountType.IBOOK, callback));
    }

    //======================================================================
    // Private Methods
    //======================================================================

    private void responseLogin(LoginResult loginResult) {
        UserSession.with().responseLogin(loginResult);

        if (loginResult == null) {
            mAppPreference.account().clear();
        } else {
            User user = loginResult.getUser();
            switch(user.getStatus()) {
                case UNCONFIRM:
                case LEAVE:
                    mAppPreference.account().clear();
                    break;
                case CONFIRM:
                    mAppPreference.account().login(loginResult.getAccessToken());
                    break;
            }
        }
    }

    private void clearPreference() {
        if (mAppPreference == null) {
            return;
        }
        mAppPreference.clearAll();
        UserSession.with().release();
    }

    private HeaderParameter createParameter() {
        return createParameter(null);
    }

    private HeaderParameter createParameter(HeaderParameter headerParameter) {
        if (headerParameter == null) {
            headerParameter = new HeaderParameter();
        }
        return headerParameter;
    }

    //======================================================================
    // LoginResponseWrapper
    //======================================================================

    final class LoginResponseWrapper implements JsonRequest.OnResponseListener<BaseResponse<LoginResult>> {

        LoginCallback mCallback;

        AccountType mAccountType;

        public LoginResponseWrapper(AccountType accountType, LoginCallback callback) {
            mAccountType = accountType;
            mCallback = callback;
        }

        @Override
        public final void onSuccess(BaseResponse<LoginResult> userDefaultResponse) {
            responseLogin(userDefaultResponse.getResult());
            if (mCallback != null) {
                mCallback.onLoginSuccess(userDefaultResponse);
            }
            complete();
        }

        @Override
        public final void onError(Request request, IOException ioe) {
            if (ioe instanceof ServerResponseErrorException) {
                Object response = ((ServerResponseErrorException) ioe).getResponse();
                if (response != null && response instanceof BaseResponse) {
                    Object result = ((BaseResponse) response).getResult();
                    if (result instanceof LoginResult) {
                        UserSession.with().responseLogin((LoginResult) result);
                    }
                }
            }

            ErrorStatus status = ErrorStatus.toErrorStatus(ioe);
            if (status != null) {
                switch(status) {
                    case overlap:
                        UserSession.with().setLoginComplete(false);
                        mAppPreference.account().clear();
                        break;
                }
            }
            if (mCallback != null) {
                mCallback.onLoginError(ioe, ioe.getMessage());
            }
            complete();
        }

        public void complete() {
            mCallback = null;
        }
    }
}
