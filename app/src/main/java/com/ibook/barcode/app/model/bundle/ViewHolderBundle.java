package com.ibook.barcode.app.model.bundle;

import android.os.Bundle;
import android.support.annotation.NonNull;

import org.parceler.Parcel;

/*
 * @author lsh
 * @since 2017. 5. 17.
*/
@Parcel
public class ViewHolderBundle extends SupportBundle {

    //======================================================================
    // Constructor
    //======================================================================

    @SuppressWarnings("deprecation")
    public ViewHolderBundle() {
        // Nothing
    }

    @SuppressWarnings("unchecked")
    public static <T> T getBundle() {
        return getBundle(ViewHolderBundle.class, null, null);
    }

    @SuppressWarnings("unchecked")
    public static ViewHolderBundle createSupportBundle(@NonNull Bundle bundle) {
        ViewHolderBundle holderBundle = getBundle();
        holderBundle.setDataBundle((Bundle) bundle.clone());
        return holderBundle;
    }
}
