package com.ibook.barcode.app.activity;

import android.annotation.TargetApi;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.v4.app.Fragment;

import com.ibook.barcode.R;
import com.ibook.barcode.app.activity.internal.delegate.ActivityDelegate;
import com.ibook.barcode.app.fragment.BaseFragment;
import com.ibook.barcode.app.model.bundle.ActivityBundle;

/*
 * @author lsh
 * @since 17. 5. 16.
*/
public class BaseAppCompatActivity extends com.starnation.android.app.activity.AppCompatActivity {

    //======================================================================
    // Variables
    //======================================================================

    private final ActivityDelegate mDelegate = new ActivityDelegate(this);

    //======================================================================
    // Override Methods
    //======================================================================

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mDelegate.onCreate(savedInstanceState);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onCreate(Bundle savedInstanceState, PersistableBundle persistentState) {
        super.onCreate(savedInstanceState, persistentState);
        mDelegate.onCreate(savedInstanceState, persistentState);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mDelegate.onSaveInstanceState(outState);
    }

    @Override
    public void onSaveInstanceState(Bundle outState, PersistableBundle outPersistentState) {
        super.onSaveInstanceState(outState, outPersistentState);
        mDelegate.onSaveInstanceState(outState, outPersistentState);
    }

    @Override
    protected void onStart() {
        try {
            super.onStart();
        } catch (Exception e) {
            e.printStackTrace();
        }
        mDelegate.onStart();
    }

    @Override
    protected void onResume() {
        try {
            super.onResume();
        } catch (Exception e) {
            e.printStackTrace();
        }
        mDelegate.onResume();
    }

    @Override
    protected void onPostResume() {
        try {
            super.onPostResume();
        } catch (Exception e) {
            e.printStackTrace();
        }
        mDelegate.onPostResume();
    }

    @Override
    protected void onStop() {
        super.onStop();
        mDelegate.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mDelegate.onDestroy();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        mDelegate.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState, PersistableBundle persistentState) {
        super.onRestoreInstanceState(savedInstanceState, persistentState);
        mDelegate.onRestoreInstanceState(savedInstanceState);
    }

    @Override
    public void onBackPressed() {
        /*
           2016. 11. 23. lsh
           java.lang.IllegalStateException: FragmentManager is already executing transactions 예외처리
           fabric 에서 crash 발생
        */
        try {
            Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.container);
            if (fragment != null && fragment instanceof BaseFragment) {
                boolean backPressed = ((BaseFragment) fragment).onBackPressed();
                if (backPressed == true) {
                    super.onBackPressed();
                }
            }
        } catch (Exception e) {
            // Nothing
        }
    }

    public ActivityBundle getActivityBundle() {
        return mDelegate.getActivityBundle();
    }

    public ActivityDelegate getActivityDelegate() {
        return mDelegate;
    }
}
