package com.ibook.barcode.app.presenter;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;

import com.ibook.barcode.app.fragment.Impl;
import com.ibook.barcode.app.manager.auth.UserSession;
import com.ibook.barcode.app.model.bundle.FragmentBundle;
import com.ibook.barcode.network.response.BaseResponse;
import com.starnation.okhttp.JsonRequest;
import com.starnation.widget.recyclerview.holder.selector.MultiSelector;

import java.io.IOException;

import okhttp3.Request;

/*
 * @author lsh
 * @since 17. 5. 16.
*/
public class Presenter extends com.starnation.android.app.fragment.presenter.Presenter {

    //======================================================================
    // Constructor
    //======================================================================

    public Presenter(Fragment fragment) {
        super(fragment);
    }

    //======================================================================
    // Override Methods
    //======================================================================

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    //======================================================================
    // Public Methods
    //======================================================================

    @NonNull
    public final FragmentBundle getSupportArguments() {
        Fragment fragment = getFragment();
        if (fragment instanceof Impl.SupportFragment) {
            return ((Impl.SupportFragment) fragment).getSupportArguments();
        }
        return FragmentBundle.EMPTY;
    }

    public int getUserSeq(){
        return UserSession.with().getSeq();
    }

    public int getChildSeq(){
        return UserSession.with().getSelectChildSeq();
    }

    //======================================================================
    // OnResponseListenerWrapper
    //======================================================================

    protected abstract class OnResponseListenerWrapper<Response> implements JsonRequest.OnResponseListener<Response> {

        JsonRequest.OnResponseListener<Response> mListener;

        public OnResponseListenerWrapper(JsonRequest.OnResponseListener<Response> listener) {
            mListener = listener;
        }

        /**
         * {@link JsonRequest.OnResponseListener#onSuccess(Object)}으로 응답 받은 경우 호출.
         *
         * @param response {@link BaseResponse}
         */
        public abstract void onResult(Response response);

        @Override
        public final void onSuccess(Response response) {
            onResult(response);
            if (mListener != null) {
                mListener.onSuccess(response);
            }
            complete();
        }

        @Override
        public final void onError(Request request, IOException ioe) {
            onError(ioe);
            if (mListener != null) {
                mListener.onError(request, ioe);
            }
            complete();
        }

        protected void onError(IOException ioe) {
            // Override
        }

        public void complete() {
            mListener = null;
        }
    }
}
