package com.ibook.barcode.app.model.eventbus;


/*
 * @author lsh
 * @since 2017. 5. 17.
*/
public enum EventKey {
    /**
     * 바코드 스캔 결과
     */
    RESULT_BARCODE,
    DEFAULT;
}
