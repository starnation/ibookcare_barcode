package com.ibook.barcode.app.fragment.barcode;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ibook.barcode.R;
import com.ibook.barcode.app.fragment.Fragments;
import com.ibook.barcode.app.model.barcode.BarcodeWrapper;
import com.ibook.barcode.app.model.bundle.FragmentBundle;
import com.ibook.barcode.app.presenter.barcode.BookRegisterPresenter;
import com.ibook.barcode.app.presenter.barcode.MultiBarcodeScanPresenter;
import com.ibook.barcode.app.viewholder.BarcodeBookViewHolder;
import com.ibook.barcode.network.exception.ServerResponseErrorException;
import com.ibook.barcode.network.model.book.BookCollection;
import com.ibook.barcode.network.model.book.BookItem;
import com.ibook.barcode.network.model.book.IsbnInfo;
import com.ibook.barcode.network.response.BaseResponse;
import com.ibook.barcode.network.response.ErrorStatus;
import com.ibook.barcode.util.log.AppLogger;
import com.ibook.barcode.util.toast.AppToast;
import com.ibook.barcode.widget.dialog.AlertDialogs;
import com.ibook.barcode.widget.recyclerview.BaseRecyclerViewAdapter;
import com.starnation.android.app.fragment.DialogFragment;
import com.starnation.android.view.ViewUtil;
import com.starnation.okhttp.JsonRequest;
import com.starnation.util.validator.CollectionValidator;
import com.starnation.widget.recyclerview.AdapterUtil;
import com.starnation.widget.recyclerview.RecyclerViewAdapter;
import com.starnation.widget.recyclerview.RecyclerViewHolder;
import com.starnation.widget.recyclerview.decoration.SpacingItemDecoration;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.OnClick;
import okhttp3.Request;
import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.exceptions.Exceptions;
import rx.functions.Action1;
import rx.functions.Func1;

/*
 * @author lsh
 * @since 2017. 5. 17.
*/
public class BookRegisterFragment extends MultiBarcodeScanFragment<BookRegisterPresenter> {

    //======================================================================
    // Variables
    //======================================================================

    @BindView(R.id.recyclerview)
    RecyclerView mRecyclerview;

    @BindView(R.id.button_registration)
    AppCompatButton mButtonRegistration;

    @BindView(R.id.layout_recyclerview)
    LinearLayout mLayoutRecyclerview;

    @BindView(R.id.textview)
    TextView mTextview;

    @BindView(R.id.textview_collection)
    AppCompatTextView mTextviewCollection;

    @BindView(R.id.button_collection)
    AppCompatButton mButtonCollection;

    @BindView(R.id.button_select)
    AppCompatButton mButtonSelect;

    private Subscription mUISubscription;

    //======================================================================
    // Override Methods
    //======================================================================

    @Override
    protected View onCreateContentView(LayoutInflater inflater, ViewGroup container) {
        return inflater.inflate(R.layout.fragment_barcode_book_register, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        int spacing = getResources().getDimensionPixelSize(R.dimen.app_margin_10dip);
        mRecyclerview.setLayoutManager(new StaggeredGridLayoutManager(1, StaggeredGridLayoutManager.HORIZONTAL));
        mRecyclerview.setAdapter(createBaseRecyclerViewAdapter());
        mRecyclerview.addItemDecoration(new SpacingItemDecoration(mRecyclerview, spacing, spacing));
    }

    @Override
    public void onRecyclerViewHolderEvent(int id, @NonNull RecyclerViewHolder holder) {
        switch(id) {
            case R.id.button_close:
                removeItem((BarcodeWrapper) holder.getItem());
                break;
        }
    }

    @SuppressWarnings("PointlessBooleanExpression")
    @Override
    protected void isbnValid(String isbn) throws IllegalArgumentException {
        BarcodeWrapper wrapper = getPresenter().findBookIsbn(isbn);
        if (wrapper != null) {
            if (System.currentTimeMillis() - wrapper.getLastTimeStamp() < MultiBarcodeScanPresenter.INTERVAL) {
                throw new IllegalArgumentException(""); //인터벌 무시
            }
        }
    }

    @SuppressWarnings({"PointlessBooleanExpression", "EqualsBetweenInconvertibleTypes", "unchecked"})
    @Override
    protected void onIsbnResponse(final BookItem bookItem) {
        getPresenter().onIsbnResponse(bookItem)
                .subscribe(new Action1<MultiBarcodeScanPresenter.ItemState>() {
                    @Override
                    public void call(MultiBarcodeScanPresenter.ItemState itemState) {
                        switch(itemState) {
                            case STATE_CHILD:
                                AppToast.with().showDefaultShortMessage(R.string.app_message_already_book_child_bookshelf);
                                break;
                            case STATE_EXIST:
                                AppToast.with().showDefaultShortMessage(R.string.app_message_already_book_scan_list);
                                break;
                            case STATE_INSERT:
                                BookCollection bookCollection = bookItem.getBookCollection();
                                if (bookCollection != null) {
                                    String message = String.format(getString(R.string.app_format_insert_collection_item), bookCollection.getName());
                                    AppToast.with().showDefaultShortMessage(message);
                                }
                                break;
                        }
                        refreshBottom(bookItem);
                    }
                });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (mUISubscription != null) {
            mUISubscription.unsubscribe();
            mUISubscription = null;
        }
    }

    //======================================================================
    // Methods
    //======================================================================

    @OnClick({R.id.button_registration,
            R.id.button_collection,
            R.id.button_select})
    void onClick(View view) {
        switch(view.getId()) {
            case R.id.button_registration:
                requestRegister(false, false);
                break;
            case R.id.button_collection:
                BarcodeWrapper wrapper = getPresenter().getLastItem();
                IsbnInfo isbnInfo = wrapper.getBookItem().getIsbnInfo();
                if (isbnInfo != null) {
                    getPresenter().updateLastItemCollection(isbnInfo.getBookCount(), null);
                }
                refreshBottom(null);
                break;
            case R.id.button_select:
                showCollectionBookSelect();
                refreshBottom(null);
                break;
        }
    }

    //======================================================================
    // Private Methods
    //======================================================================

    private BaseRecyclerViewAdapter createBaseRecyclerViewAdapter() {
        return new BaseRecyclerViewAdapter(new BaseRecyclerViewAdapter.Provider() {
            @Override
            public int getItemHeaderCount() {
                return 0;
            }

            @Override
            public int getSupportItemCount() {
                return getPresenter().getListItemSize();
            }

            @Override
            public int getSupportItemViewType(int position) {
                return 0;
            }

            @Override
            public Object getSupportItem(int viewType, int position) {
                return getPresenter().getItem(position);
            }
        }) {
            @Override
            public RecyclerViewHolder onCreateItemViewHolder(ViewGroup parent, int viewType) {
                return new BarcodeBookViewHolder(BookRegisterFragment.this, parent);
            }
        };
    }

    private void removeItem(BarcodeWrapper wrapper) {
        try {
            mRecyclerview.setNestedScrollingEnabled(false);
            getPresenter().removeItem(wrapper);
            mRecyclerview.setNestedScrollingEnabled(true);

            refreshBottom(null);
        } catch (Exception e) {
            AppLogger.printStackTrace(e);
        }
    }

    /**
     * 책 등록
     *
     * @param overlap true 중복 허용
     * @param moveYn  true 새로운 책만 등록
     */
    private void requestRegister(boolean overlap, boolean moveYn) {
        getPresenter().requestRegister(
                overlap,
                moveYn,
                this,
                new JsonRequest.OnResponseListener<BaseResponse>() {
                    @Override
                    public void onSuccess(BaseResponse baseResponse) {
                        showBookRegisterResult();
                        removeItem(null);
                    }

                    @Override
                    public void onError(Request request, IOException ioe) {
                        Observable.just(ioe).map(new Func1<IOException, IOException>() {
                            @Override
                            public IOException call(IOException e) {
                                ErrorStatus errorStatus = ServerResponseErrorException.getErrorStatus(e);
                                if (errorStatus != null && errorStatus == ErrorStatus.overlap) {
                                    return new ServerResponseErrorException(e.getMessage());
                                }
                                throw Exceptions.propagate(new IOException(e.getMessage()));
                            }
                        }).subscribe(new Subscriber<IOException>() {
                            @Override
                            public void onCompleted() {

                            }

                            @Override
                            public void onError(Throwable e) {
                                AppToast.with().showDefaultLongMessage(e.getMessage());
                            }

                            @Override
                            public void onNext(IOException e) {
                                showOverlapBooks(e.getMessage());
                            }
                        });
                    }
                });
    }

    private void showBookRegisterResult() {
        mAlertDialog = AlertDialogs.createConfirm(getContext(),
                getString(R.string.app_format_book_register_complete),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
        mAlertDialog.show();
    }

    private void showOverlapBooks(String message) {
        AlertDialogs.create(getContext(),
                message,
                R.string.app_dialog_overlap_exclude_book,
                R.string.app_complete_ok,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        requestRegister(true, false);
                    }
                }, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        requestRegister(true, true);
                    }
                }).show();
    }

    private void showCollectionBookSelect() {
        final BarcodeWrapper wrapper = getPresenter().getLastItem();
        Fragments.showDialogFragment(getFragmentManager(),
                CollectionBookSelectFragment.class,
                new FragmentBundle()
                        .setCode(wrapper.getSetCode())
                        .maxCount(wrapper.getBookItem().getIsbnInfo().getBookCount())
                        .bookCodeList(wrapper.getChildBookCodeList())
                        .title(getCollectionGuideText(wrapper.getBookItem())),
                new DialogFragment.OnDismissCallback() {
                    @Override
                    public void onDismiss(@NonNull Bundle event) {
                        IsbnInfo isbnInfo = wrapper.getBookItem().getIsbnInfo();
                        FragmentBundle fragmentBundle = new FragmentBundle();
                        fragmentBundle.setDataBundle(event);
                        if (isbnInfo != null) {
                            getPresenter().updateLastItemCollection(CollectionValidator.getSize(fragmentBundle.getBookCodeList()),
                                    fragmentBundle.getBookCodeList());
                        }

                        //Modify 2017. 4. 14. vkwofm UI 갱신
                        refreshBottom(null);
                        AdapterUtil.notifySupportDataSetChanged((RecyclerViewAdapter) mRecyclerview.getAdapter(), true);
                    }
                });
    }

    @SuppressWarnings("PointlessBooleanExpression")
    private void refreshBottom(BookItem bookItem) {
        if (getPresenter().getListItemSize() == 0) {
            ViewUtil.show(mTextview);
            ViewUtil.hide(mLayoutRecyclerview);
        } else {
            ViewUtil.show(mLayoutRecyclerview);
            ViewUtil.hide(mTextview);

            ViewUtil.visibility(mTextviewCollection, getPresenter().showCollectionGuide(bookItem));
            ViewUtil.visibility(mButtonCollection, getPresenter().showCollectionGuide(bookItem));
            ViewUtil.visibility(mButtonSelect, getPresenter().showCollectionGuide(bookItem));
            if (getPresenter().showCollectionGuide(bookItem) == true) {
                mTextviewCollection.setText(getCollectionGuideText(bookItem));
            }

            String text = Integer.toString(getPresenter().toDisplayCountItemSize()) + "권 등록 확인";
            mButtonRegistration.setText(text);

            AdapterUtil.notifySupportDataSetChanged((RecyclerViewAdapter) mRecyclerview.getAdapter(), true);

            if (mUISubscription != null) {
                mUISubscription.unsubscribe();
                mUISubscription = null;
            }
            mUISubscription = Observable.timer(200, TimeUnit.MILLISECONDS)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Action1<Long>() {
                        @Override
                        public void call(Long aLong) {
                            final int size = getPresenter().getListItemSize();
                            if (size > 0) {
                                mRecyclerview.smoothScrollToPosition(size - 1);
                            }
                        }
                    });
        }
    }

    private String getCollectionGuideText(BookItem bookItem) {
        BookCollection bookCollection = bookItem.getBookCollection();
        IsbnInfo isbnInfo = bookItem.getIsbnInfo();
        return bookCollection.getName() + " " +
                String.format(getString(R.string.app_format_book_count), isbnInfo.getBookCount());
    }
}
