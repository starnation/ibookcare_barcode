package com.ibook.barcode.app.manager.preference;

import com.starnation.android.content.preference.SharedPreference;

import static com.ibook.barcode.app.manager.preference.PreferenceKey.ACCESS_TOKEN;

/*
 * @author lsh
 * @since 2016. 4. 16.
*/
public final class Account extends Base {

    //======================================================================
    // Constructor
    //======================================================================

    public Account(SharedPreference preference) {
        super(preference);
    }

    //======================================================================
    // Override Methods
    //======================================================================

    @Override
    public void clear() {
        super.clear();
    }

    //======================================================================
    // Public Methods
    //======================================================================

    public void login(String token) {
        putString(ACCESS_TOKEN, token);
        save();
    }

    public String getAccessToken() {
        return getString(ACCESS_TOKEN);
    }

    public void updateAccessToken(String accessToken) {
        putString(ACCESS_TOKEN, accessToken).save();
    }
}
