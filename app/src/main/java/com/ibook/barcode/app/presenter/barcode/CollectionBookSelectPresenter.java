package com.ibook.barcode.app.presenter.barcode;

import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;

import com.ibook.barcode.app.model.bundle.ViewHolderBundle;
import com.ibook.barcode.app.presenter.PagerRecyclerViewPresenter;
import com.ibook.barcode.network.model.book.BookItem;
import com.ibook.barcode.network.parameter.HeaderParameter;
import com.ibook.barcode.network.request.Requests;
import com.ibook.barcode.network.response.BaseResponse;
import com.starnation.okhttp.RequestBuilder;
import com.starnation.util.StringUtil;
import com.starnation.util.validator.CollectionValidator;
import com.starnation.widget.recyclerview.holder.selector.MultiSelector;

import java.util.ArrayList;

import lombok.Getter;

/*
 * @author lsh
 * @since 2017. 5. 17.
*/
public class CollectionBookSelectPresenter extends PagerRecyclerViewPresenter {

    //======================================================================
    // Variables
    //======================================================================

    @Getter
    private final MultiSelector mSelector = new MultiSelector();

    //======================================================================
    // Constructor
    //======================================================================

    public CollectionBookSelectPresenter(Fragment fragment) {
        super(fragment);
        setMaxCount(getSupportArguments().getMaxCount());
    }

    //======================================================================
    // Override Methods
    //======================================================================

    @Override
    protected PagerProvider onCreateProvider() {
        return new PagerProvider() {
            @Override
            public void onBindArguments(int viewType, ViewHolderBundle arguments) {

            }

            @Override
            public int getItemHeaderCount() {
                return 0;
            }

            @Override
            public int getSupportItemViewType(int position) {
                return 0;
            }

            @Override
            public int getSupportItemCount() {
                return super.getSupportItemCount();
            }
        };
    }

    @Override
    protected PagerRequestProcessor onCreatePagerRequestProcessor() {
        return new PagerRequestProcessor<ArrayList<BookItem>>() {
            @Override
            public void valid() throws IllegalArgumentException {
                if (StringUtil.isEmpty(getSupportArguments().getSetCode()) == true) {
                    throw new IllegalArgumentException("SetCode is empty!!");
                }
            }

            @Override
            public RequestBuilder onCreateBuilder() {
                return Requests.getSearchCollection(getSupportArguments().getSetCode())
                        .header(new HeaderParameter()
                                .userSeq(getUserSeq())
                                .page(getPage())
                                .sortType("regDate"));
            }

            @Override
            public ArrayList<?> parserList(BaseResponse<ArrayList<BookItem>> response) {
                return response.getResult();
            }

            @SuppressWarnings("unchecked")
            @Override
            public void onResult(BaseResponse<ArrayList<BookItem>> response) {
                ArrayList<String> bookCodeList = getSupportArguments().getBookCodeList();
                if (CollectionValidator.isValid(bookCodeList) == true) {
                    for (int index = 0; index < response.getResult().size(); index++) {
                        BookItem bookItem = response.getResult().get(index);
                        for (String bookCode : bookCodeList) {
                            if (bookItem.getBookCode().equals(bookCode) == true) {
                                bookItem.setChecked(true);
                                mSelector.setSelected(index, true);
                                break;
                            }
                        }
                    }
                }
            }
        };
    }

    //======================================================================
    // Public Methods
    //======================================================================

    @SuppressWarnings("unchecked")
    public ArrayList<String> makeCheckedBookCodeList() {
        ArrayList<String> list = new ArrayList<>();
        for (BookItem bookItem : (ArrayList<BookItem>)getList()) {
            if (bookItem.isChecked() == true) {
                list.add(bookItem.getBookCode());
            }
        }
        return list;
    }
}
