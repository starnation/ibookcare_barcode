package com.ibook.barcode.app.model.bundle;

import android.os.Bundle;
import android.support.annotation.NonNull;

import java.util.ArrayList;

/*
 * @author lsh
 * @since 17. 5. 16.
*/
public final class FragmentBundle extends SupportBundle {

    //======================================================================
    // Constants
    //======================================================================

    public final static FragmentBundle EMPTY = new FragmentBundle();

    //======================================================================
    // Constructor
    //======================================================================

    @SuppressWarnings("deprecation")
    public FragmentBundle() {
        // Nothing
    }

    @SuppressWarnings("unchecked")
    public static <T> T getBundle() {
        return getBundle(FragmentBundle.class, null, null);
    }

    @SuppressWarnings("unchecked")
    public static FragmentBundle createSupportBundle(@NonNull FragmentBundle bundle) {
        FragmentBundle fragmentBundle = getBundle();
        fragmentBundle.setDataBundle((Bundle) bundle.getDataBundle().clone());
        return fragmentBundle;
    }

    public String getSetCode() {
        return getInternal(getMethodName(new Object() {
        }));
    }

    public FragmentBundle setCode(String setCode) {
        return putInternal(getMethodName(new Object() {
        }), setCode);
    }

    public FragmentBundle maxCount(int maxCount) {
        return putInternal(getMethodName(new Object() {
        }), maxCount);
    }

    public int getMaxCount() {
        return getInternal(getMethodName(new Object() {
        }), 0);
    }

    public FragmentBundle bookCodeList(ArrayList<String> bookCodeList) {
        return putInternal(getMethodName(new Object() {
        }), bookCodeList);
    }

    public ArrayList<String> getBookCodeList() {
        return getParcelable(getMethodName(new Object() {
        }));
    }

    public FragmentBundle title(String title) {
        return putInternal(getMethodName(new Object() {
        }), title);
    }

    public String getTitle() {
        return getInternal(getMethodName(new Object() {
        }));
    }
}
