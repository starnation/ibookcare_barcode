package com.ibook.barcode.app.activity.internal;

import android.Manifest;
import android.support.annotation.NonNull;

import com.starnation.android.util.PermissionChecker;

import java.util.ArrayList;

/*
 * @author lsh
 * @since 2017. 5. 16.
*/
public final class RequestPermission {

    //======================================================================
    // Public Methods
    //======================================================================

    public static void camera(@NonNull PermissionChecker checker, PermissionChecker.OnSupportRequestPermissionsResultCallback callback) {
        ArrayList<String> list = new ArrayList<>();
        list.add(Manifest.permission.CAMERA);
        list.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        checker.makeRequestPermissions(list, callback);
    }
}
