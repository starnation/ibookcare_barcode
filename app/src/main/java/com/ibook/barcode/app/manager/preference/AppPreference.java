package com.ibook.barcode.app.manager.preference;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.SparseArray;

import com.starnation.android.content.preference.DefaultSharedPreference;
import com.starnation.android.content.preference.SharedPreference;

/*
 * @author lsh
 * @since 2017. 5. 17.
*/
public final class AppPreference {

    //======================================================================
    // Constants
    //======================================================================

    public final static String PREFERENCE_NAME = "com.ibook.preference";

    //======================================================================
    // Variables
    //======================================================================

    private final static AppPreference INSTANCE = new AppPreference();

    SharedPreference mDefault;

    SparseArray<Base> mSparseArray = new SparseArray<>();

    //======================================================================
    // Public Methods
    //======================================================================

    public static AppPreference from(@NonNull Context context) {
        if (INSTANCE.mDefault == null) {
            INSTANCE.init(context);
        }
        return INSTANCE;
    }

    public static AppPreference from() {
        return INSTANCE;
    }

    void init(@NonNull Context context) {
        mDefault = DefaultSharedPreference.create(context, PREFERENCE_NAME);
        put(context, Account.class);
    }

    public Account account() {
        return (Account) getPreference(Account.class);
    }

    public void clearAll() {
        int size = mSparseArray.size();
        for (int i = 0; i < size; i++) {
            Base base = mSparseArray.get(mSparseArray.keyAt(i));
            if (base != null) {
                base.clear();
            }
        }
    }

    //======================================================================
    // Private Methods
    //======================================================================

    private static int asKey(Class<? extends Base> preference) {
        return preference.getName().hashCode();
    }

    private void put(@NonNull Context context, Class<? extends Base> preference) {
        try {
            Base base = preference.getDeclaredConstructor(SharedPreference.class).newInstance(DefaultSharedPreference.create(context, preference.getName()));
            mSparseArray.put(asKey(preference), base);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private Base getPreference(Class<? extends Base> preference) {
        return mSparseArray.get(asKey(preference));
    }
}
