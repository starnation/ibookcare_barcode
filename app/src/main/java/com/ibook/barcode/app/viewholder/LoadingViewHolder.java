package com.ibook.barcode.app.viewholder;

import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.ViewGroup;

import com.ibook.barcode.R;
import com.starnation.widget.recyclerview.RecyclerViewHolder;

/*
 * @author lsh
 * @since 17. 5. 17.
*/
public final class LoadingViewHolder extends RecyclerViewHolder {

    //======================================================================
    // Constructor
    //======================================================================

    public LoadingViewHolder(ViewGroup viewGroup) {
        super(viewGroup, R.layout.view_holder_loading);
        if (getItemView().getLayoutParams() instanceof StaggeredGridLayoutManager.LayoutParams) {
            ((StaggeredGridLayoutManager.LayoutParams) getItemView().getLayoutParams()).setFullSpan(true);
        }
    }

    //======================================================================
    // Override Methods
    //======================================================================

    @Override
    public void onRefresh(Object o) {
        //Nothing
    }
}
