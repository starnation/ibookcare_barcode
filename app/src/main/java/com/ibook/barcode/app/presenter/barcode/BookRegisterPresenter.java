package com.ibook.barcode.app.presenter.barcode;

import android.os.Bundle;
import android.support.v4.app.Fragment;

import com.ibook.barcode.app.model.barcode.BarcodeWrapper;
import com.ibook.barcode.network.model.book.BookItem;
import com.ibook.barcode.network.parameter.BodyParameter;
import com.ibook.barcode.network.parameter.HeaderParameter;
import com.ibook.barcode.network.parameter.RequestBodyFactory;
import com.ibook.barcode.network.request.Requests;
import com.ibook.barcode.network.response.BaseResponse;
import com.starnation.okhttp.JsonRequest;
import com.starnation.util.validator.CollectionValidator;

import java.util.ArrayList;

import rx.Observable;
import rx.functions.Func0;
import rx.functions.Func1;

/*
 * @author lsh
 * @since 2017. 5. 17.
*/
public class BookRegisterPresenter extends MultiBarcodeScanPresenter {

    //======================================================================
    // Constructor
    //======================================================================

    public BookRegisterPresenter(Fragment fragment) {
        super(fragment);
    }

    //======================================================================
    // Override Methods
    //======================================================================

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @SuppressWarnings({"PointlessBooleanExpression", "unchecked"})
    @Override
    protected Observable<ItemState> onIsbnResponse(final ArrayList<BarcodeWrapper> list, final BookItem bookItem) {
        return Observable.defer(new Func0<Observable<ItemState>>() {
            @Override
            public Observable<ItemState> call() {
                if (bookItem.isRegister() == true) {
                    return Observable.just(ItemState.STATE_CHILD);
                } else if (findBookCode(bookItem) != null) {
                    //Modify 2017. 4. 14. vkwofm 리스트에 이미 있는 책에 대한 처리
                    BarcodeWrapper wrapper = findBookCode(bookItem);
                    wrapper.setLastTimeStamp(System.currentTimeMillis());

                    list.remove(wrapper);
                    list.add(wrapper);
                    return Observable.just(ItemState.STATE_EXIST);
                } else if (findSetCode(bookItem) != null) {
                    //Modify 2017. 4. 13. vkwofm 전집에 대한 처리
                    BarcodeWrapper wrapper = findSetCode(bookItem);
                    wrapper.setLastTimeStamp(System.currentTimeMillis());

                    ArrayList<String> bookCodeList = new ArrayList<>();
                    if (wrapper.getBarcodeCollection() == null) {
                        bookCodeList.add(wrapper.getBookCode());
                    } else {
                        bookCodeList.addAll(wrapper.getBarcodeCollection().getChildList());
                    }

                    bookCodeList.add(bookItem.getBookCode());
                    wrapper.updateCollectionType(CollectionValidator.getSize(bookCodeList), bookCodeList);
                    wrapper.getBarcodeCollection().insert(bookItem.getBookInfo().getIsbn13());

                    list.remove(wrapper);
                    list.add(wrapper);
                    return Observable.just(ItemState.STATE_INSERT);
                } else {
                    //Modify 2017. 4. 13. vkwofm 리스트에 없고 아이책장에 등록안된 책에 대한 처리
                    if (bookItem.isRegister() == false) {
                        BarcodeWrapper wrapper = new BarcodeWrapper(bookItem);
                        wrapper.setLastTimeStamp(System.currentTimeMillis());
                        list.add(wrapper);
                    }
                    return Observable.just(ItemState.STATE_NEW);
                }
            }
        });
    }

    //======================================================================
    // Public Methods
    //======================================================================

    @SuppressWarnings("PointlessBooleanExpression")
    public boolean showCollectionGuide(BookItem bookItem) {
        return bookItem != null && bookItem.isCollection() == true && bookItem.getBookCollection() != null;
    }

    public ArrayList<BodyParameter> getBodyParameterList() {
        return BodyParameter.asBodyParameterList(getItemList(),
                new Func1<BarcodeWrapper, BodyParameter>() {
                    @SuppressWarnings("PointlessBooleanExpression")
                    @Override
                    public BodyParameter call(BarcodeWrapper wrapper) {
                        BodyParameter bodyParameter = new BodyParameter();
                        if (wrapper.isCollection() == true) {
                            bodyParameter.set(wrapper.getSetCode(), wrapper.getChildBookCodeList());
                        } else {
                            bodyParameter.bookCode(wrapper.getBookCode());
                        }
                        return bodyParameter;
                    }
                });
    }

    @SuppressWarnings("unchecked")
    public void requestRegister(boolean bookRegister, boolean moveYn, JsonRequest.OnStatusListener statusListener, JsonRequest.OnResponseListener<BaseResponse> responseListener) {
        requestRegisterInternal(bookRegister, moveYn, statusListener, responseListener);
    }

    //======================================================================
    // Private Methods
    //======================================================================

    @SuppressWarnings("unchecked")
    private void requestRegisterInternal(boolean bookRegister, boolean moveYn, JsonRequest.OnStatusListener statusListener, JsonRequest.OnResponseListener<BaseResponse> responseListener) {
        try {
            Requests.postChildMyBook(getChildSeq())
                    .header(new HeaderParameter()
                            .userSeq(getUserSeq())
                            .registerYn(bookRegister)
                            .moveYn(moveYn))
                    .requestBody(RequestBodyFactory.createTargetList(getBodyParameterList()))
                    .statusListener(statusListener)
                    .execute(responseListener);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
