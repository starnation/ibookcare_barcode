package com.ibook.barcode.app.model.bundle;

import android.os.Bundle;
import android.support.annotation.NonNull;

import com.starnation.android.os.Bundles;
import com.starnation.android.os.parcel.ParcelUtil;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.util.ArrayList;

/*
 * @author lsh
 * @since 17. 5. 16.
*/
public class SupportBundle {

    //======================================================================
    // Constance
    //======================================================================

    private final static String GET = "get";

    private final static String IS = "is";

    //======================================================================
    // Variables
    //======================================================================

    Bundle mDataBundle = new Bundle();

    //======================================================================
    // Constructor
    //======================================================================

    @SuppressWarnings("unchecked")
    public static <T> T getBundle(Class<? extends SupportBundle> classObject, Bundle bundle, Object target) {
        try {
            SupportBundle supportBundle = classObject.newInstance();
            if (bundle != null) {
                supportBundle.setDataBundle(bundle, target);
            }
            return (T) supportBundle;
        } catch (Exception e) {
            // Nothing
        }
        return null;
    }

    @Deprecated
    public SupportBundle() {
        getDataBundle();
    }

    //======================================================================
    // Public Methods
    //======================================================================

    public Bundle fromBundle() {
        return getDataBundle();
    }

    public Bundle getDataBundle() {
        return mDataBundle;
    }

    public void setDataBundle(Bundle dataBundle) {
        if (dataBundle != null) {
            mDataBundle = dataBundle;
        }
    }

    public void setDataBundle(Bundle dataBundle, Object target) {
        mDataBundle = dataBundle;
        parsingSupportBundle(target);
    }

    //======================================================================
    // Protected Methods
    //======================================================================

    @SuppressWarnings("unchecked")
    protected void join(@NonNull String key, @NonNull Bundle arguments) {
        arguments.putBundle(key, fromBundle());
    }

    @SuppressWarnings("unchecked")
    protected void parsingSupportBundle(Object target) {
        if (target == null) return;

        Field[] fields = target.getClass().getDeclaredFields();
        for (Field field : fields) {
            GetBundle bundleData = field.getAnnotation(GetBundle.class);
            if (bundleData != null) {
                field.setAccessible(true);
                try {
                    if (field.getType() == String.class) {
                        field.set(target, getInternal(bundleData.value()));
                    } else if (field.getType() == int.class) {
                        field.set(target, getInternal(bundleData.value(), 0));
                    } else if (field.getType() == float.class) {
                        field.set(target, getInternal(bundleData.value(), 0f));
                    } else if (field.getType() == boolean.class) {
                        field.set(target, getInternal(bundleData.value()));
                    } else if (field.getType() == double.class) {
                        field.set(target, getInternal(bundleData.value(), 0));
                    }
                } catch (Exception ignored) {
                }
            }
        }
    }

    @SuppressWarnings("unchecked")
    protected String getMethodName(Object object) {
        String name = "";
        try {
            name = object.getClass().getEnclosingMethod().getName();
        } catch (Exception e) {
            StackTraceElement[] stacktrace = Thread.currentThread().getStackTrace();
            for (int i = 0; i < stacktrace.length; i++) {
                StackTraceElement stackTraceElement = stacktrace[i];
                if (stackTraceElement.getMethodName().equals("getMethodName") == true) {
                    name = stacktrace[i+1].getMethodName();
                    break;
                }
            }
        }

        if (name.startsWith(GET) == true) {
            try {
                String sub = name.substring(GET.length(), GET.length() + 1);
                return name.replaceAll("get[A-Z]", sub.toLowerCase());
            } catch (Exception ignored) {
            }
        } else if (name.startsWith(IS) == true) {
            try {
                String sub = name.substring(IS.length(), IS.length() + 1);
                return name.replaceAll("is[A-Z]", sub.toLowerCase());
            } catch (Exception ignored) {
            }
        }
        return name;
    }

    @SuppressWarnings("unchecked")
    protected <T> T putInternal(String key, String value) {
        Bundles.put(getDataBundle(), key, value);
        return (T) SupportBundle.this;
    }

    protected String getInternal(String key) {
        return Bundles.getString(getDataBundle(), key);
    }

    @SuppressWarnings("unchecked")
    protected <T> T putIntegerArrayList(String key, ArrayList<Integer> list) {
        Bundles.put(getDataBundle(), key, list);
        return (T) SupportBundle.this;
    }

    protected ArrayList<Integer> getIntegerArrayList(String key) {
        return Bundles.getIntegerArrayList(getDataBundle(), key);
    }

    @SuppressWarnings("unchecked")
    protected <T> T putInternal(String key, int value) {
        Bundles.put(getDataBundle(), key, value);
        return (T) SupportBundle.this;
    }

    protected int getInternal(String key, int defaultValue) {
        return Bundles.getInt(getDataBundle(), key, defaultValue);
    }

    @SuppressWarnings({"unused", "unchecked"})
    protected <T> T putInternal(String key, float value) {
        Bundles.put(getDataBundle(), key, value);
        return (T) SupportBundle.this;
    }

    protected float getInternal(String key, float defaultValue) {
        return Bundles.getFloat(getDataBundle(), key, defaultValue);
    }

    @SuppressWarnings({"unused", "unchecked"})
    protected <T> T putInternal(String key, boolean value) {
        Bundles.put(getDataBundle(), key, value);
        return (T) SupportBundle.this;
    }

    @SuppressWarnings("unused")
    protected boolean getInternal(String key, boolean defaultValue) {
        return Bundles.getBoolean(getDataBundle(), key, defaultValue);
    }

    @SuppressWarnings({"unused", "unchecked"})
    protected <T> T putInternal(String key, double value) {
        getDataBundle().putDouble(key, value);
        return (T) SupportBundle.this;
    }

    @SuppressWarnings("unused")
    protected double getInternal(String key, double defaultValue) {
        return getDataBundle().getDouble(key, defaultValue);
    }

    @SuppressWarnings({"unused", "unchecked"})
    protected <T> T putInternal(String key, long value) {
        getDataBundle().putLong(key, value);
        return (T) SupportBundle.this;
    }

    @SuppressWarnings("unused")
    protected long getInternal(String key, long defaultValue) {
        return getDataBundle().getLong(key, defaultValue);
    }

    @SuppressWarnings("unchecked")
    protected <T> T putInternal(String key, Serializable value) {
        Bundles.put(getDataBundle(), key, value);
        return (T) SupportBundle.this;
    }

    @SuppressWarnings("unchecked")
    protected <T> T getSerializable(String key) {
        return Bundles.get(getDataBundle(), key);
    }

    @SuppressWarnings("unchecked")
    protected <T> T putInternal(String key, Object value) {
        ParcelUtil.putParcelable(getDataBundle(), key, value);
        return (T) SupportBundle.this;
    }

    @SuppressWarnings("unchecked")
    protected <T> T getParcelable(String key) {
        return ParcelUtil.getParcelable(getDataBundle(), key);
    }

    @SuppressWarnings("unchecked")
    protected <T> T putInternal(String key, ArrayList<?> value) {
        ParcelUtil.putParcelable(getDataBundle(), key, value);
        return (T) SupportBundle.this;
    }

    @SuppressWarnings("unchecked")
    protected <T> ArrayList<T> getArrayList(String key) {
        return ParcelUtil.getArrayList(getDataBundle(), key);
    }
}
