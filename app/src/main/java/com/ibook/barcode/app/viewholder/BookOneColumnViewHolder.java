package com.ibook.barcode.app.viewholder;

import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.view.ViewGroup;

import com.ibook.barcode.R;
import com.ibook.barcode.databinding.ViewholderBook1ColumnBinding;
import com.ibook.barcode.network.model.book.BookItem;
import com.ibook.barcode.util.Objects;
import com.ibook.barcode.widget.recyclerview.viewholder.BaseRecyclerViewHolder;

import butterknife.OnClick;

/*
 * @author lsh
 * @since 2017. 5. 17.
*/
public final class BookOneColumnViewHolder extends BaseRecyclerViewHolder<BookItem> {

    //======================================================================
    // Constructor
    //======================================================================

    public BookOneColumnViewHolder(Fragment fragment, ViewGroup viewGroup) {
        super(fragment, viewGroup, R.layout.viewholder_book_1_column);
        viewDataBinding().checkbox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setChecked(viewDataBinding().checkbox.isChecked());
            }
        });
        viewDataBinding().imageviewBook.setRatio(0.7f);
        viewDataBinding().imageviewBook.setImageRatio(0.7f);

        viewDataBinding().layoutContent.setCheckedEnable(true);
    }

    //======================================================================
    // Override Methods
    //======================================================================

    @Override
    public ViewholderBook1ColumnBinding viewDataBinding() {
        return (ViewholderBook1ColumnBinding) super.viewDataBinding();
    }

    @Override
    public void onRefresh(BookItem bookItem) {
        if (bookItem != null) {
            viewDataBinding().imageviewBook.setDefaultImage(ContextCompat.getColor(getContext(), R.color.blue_200), bookItem.getTitle());
            viewDataBinding().imageviewBook.setImageUrl(bookItem.getCoverAladin());
            viewDataBinding().textviewTitle.setText(bookItem.getTitle());
            viewDataBinding().textviewPublisherDate.setText(bookItem.getPublisherOrDate());

            refreshChecked(bookItem.isChecked());
        }
    }

    @Override
    public void setChecked(boolean checked) {
        super.setChecked(checked);
        refreshChecked(checked);
        setItemChecked(checked);
    }

    //======================================================================
    // Methods
    //======================================================================

    @OnClick({R.id.layout_content})
    void onClick(View view) {
        switch(view.getId()) {
            default:
                setChecked(viewDataBinding().layoutContent.isChecked());
                break;
        }
    }

    //======================================================================
    // Private Methods
    //======================================================================

    private void refreshChecked(boolean checked) {
        viewDataBinding().checkbox.setChecked(checked);
        viewDataBinding().layoutContent.setChecked(checked);
    }

    private void setItemChecked(boolean checked) {
        try {
            BookItem bookItem = Objects.requireNonNull(getItem());
            bookItem.setChecked(checked);
        } catch (Exception e) {
            // Nothing
        }
    }
}
