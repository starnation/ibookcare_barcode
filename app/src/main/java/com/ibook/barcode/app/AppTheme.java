package com.ibook.barcode.app;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.annotation.ColorInt;
import android.support.annotation.NonNull;
import android.util.TypedValue;

import com.ibook.barcode.R;

/*
 * @author lsh
 * @since 17. 5. 16.
*/
public final class AppTheme {

    //======================================================================
    // Variables
    //======================================================================

    private final static AppTheme INSTANCE = new AppTheme();

    private final static Impl IMPL;

    //======================================================================
    // Constructor
    //======================================================================

    static {
        final int version = Build.VERSION.SDK_INT;
        if (version >= Build.VERSION_CODES.LOLLIPOP) {
            IMPL = new Impl21();
        } else {
            IMPL = new Impl();
        }
    }

    public static AppTheme with() {
        return INSTANCE;
    }

    @Deprecated
    public AppTheme() {
        // Nothing
    }

    //======================================================================
    // Public Methods
    //======================================================================

    public Drawable getStatusBarDrawable(@NonNull Context context) {
        return IMPL.getStatusBarDrawable(context);
    }

    public Drawable getAppBarDrawable(@NonNull Context context) {
        return IMPL.getAppBarDrawable(context);
    }

    @ColorInt
    public int getColorAccent(@NonNull Context context) {
        return IMPL.getColorAccent(context);
    }

    @ColorInt
    public int getColorPrimary(@NonNull Context context) {
        return IMPL.getColorPrimary(context);
    }

    @ColorInt
    public int getTextColorPrimary(@NonNull Context context) {
        return IMPL.getTextColorPrimary(context);
    }

    @ColorInt
    public int getTextColorSecondary(@NonNull Context context) {
        return IMPL.getTextColorSecondary(context);
    }

    public int getBottomDialogTheme() {
        return R.style.BottomDialogStyle;
    }

    public int getTheme() {
        return R.style.AppTheme;
    }

    //======================================================================
    // Impl
    //======================================================================

    static class Impl {

        @ColorInt
        public int getColorAccent(@NonNull Context context) {
            TypedValue value = new TypedValue();
            context.getTheme().resolveAttribute(R.attr.colorAccent, value, true);
            return value.data;
        }

        @ColorInt
        public int getColorPrimary(@NonNull Context context) {
            TypedValue value = new TypedValue();
            context.getTheme().resolveAttribute(R.attr.colorPrimary, value, true);
            return value.data;
        }

        @ColorInt
        public int getTextColorPrimary(@NonNull Context context) {
            TypedValue value = new TypedValue();
            context.getTheme().resolveAttribute(android.R.attr.textColorPrimary, value, true);
            return value.data;
        }

        @ColorInt
        public int getTextColorSecondary(@NonNull Context context) {
            TypedValue value = new TypedValue();
            context.getTheme().resolveAttribute(android.R.attr.textColorSecondary, value, true);
            return value.data;
        }

        public Drawable getStatusBarDrawable(@NonNull Context context) {
            return null;
        }

        public Drawable getAppBarDrawable(@NonNull Context context) {
            return null;
        }
    }

    //======================================================================
    // Impl21
    //======================================================================

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    static class Impl21 extends Impl {

        private static final int[] THEME_ATTRS_DARK = {
                R.attr.colorPrimaryDark
        };

        private static final int[] THEME_ATTRS_PRIMARY = {
                R.attr.colorPrimary
        };

        @Override
        public Drawable getStatusBarDrawable(@NonNull Context context) {
            final TypedArray a = context.obtainStyledAttributes(THEME_ATTRS_DARK);
            try {
                return a.getDrawable(0);
            } finally {
                a.recycle();
            }
        }

        public Drawable getAppBarDrawable(@NonNull Context context) {
            final TypedArray a = context.obtainStyledAttributes(THEME_ATTRS_PRIMARY);
            try {
                return a.getDrawable(0);
            } finally {
                a.recycle();
            }
        }
    }
}
