package com.ibook.barcode.app.model.barcode;

import com.starnation.util.validator.CollectionValidator;

import java.util.ArrayList;

import lombok.Getter;

/*
 * @author lsh
 * @since 2017. 5. 17.
*/
public final class BarcodeCollection {

    //======================================================================
    // Variables
    //======================================================================

    @Getter
    private int mBookCount;

    @Getter
    private ArrayList<String> mChildList;

    private ArrayList<String> mIsbnList = new ArrayList<>();

    //======================================================================
    // Constructor
    //======================================================================

    public BarcodeCollection(int bookCount, ArrayList<String> bookList) {
        mBookCount = bookCount;
        if (CollectionValidator.isValid(bookList) == true) {
            mChildList = new ArrayList<>(bookList);
        }
    }

    public void insert(String isbn) {
        if (isbnContains(isbn) == false) {
            mIsbnList.add(isbn);
        }
    }

    public boolean isbnContains(String isbn) {
        return mIsbnList.contains(isbn);
    }
}
