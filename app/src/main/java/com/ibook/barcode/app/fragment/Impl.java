package com.ibook.barcode.app.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;

import com.ibook.barcode.app.activity.internal.delegate.ActivityDelegate;
import com.ibook.barcode.app.model.bundle.FragmentBundle;
import com.starnation.okhttp.JsonRequest;

/*
 * @author lsh
 * @since 2016. 5. 2.
*/
public final class Impl {

    public interface SupportFragment<C extends FragmentDelegate.Config> extends JsonRequest.OnStatusListener {

        @Nullable
        Toolbar getToolbar();

        C getConfig();

        /**
         * {@link Fragment} 필요한 정보를 생성한다.
         * <p/>
         * Default 는 {@link FragmentDelegate.Config}
         *
         * @return {@link FragmentDelegate.Config}
         */
        FragmentDelegate.Config onDefaultCreateConfig();

        /**
         * {@link Fragment#onCreate(Bundle)} 호출후 호출 한다.
         *
         * @param config {@link FragmentDelegate.Config}
         */
        void onApplyConfig(@NonNull C config);

        ActivityDelegate getActivityDelegate();

        void onProgressBarCancel();

        void setTitle(String title);

        void onToolbar(@NonNull Toolbar toolbar);

        FragmentBundle getSupportArguments();

        boolean onBackPressed();
    }

    public interface SupportDialogFragment extends SupportFragment {

    }
}
