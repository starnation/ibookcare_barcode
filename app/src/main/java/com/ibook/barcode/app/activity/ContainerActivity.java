package com.ibook.barcode.app.activity;

import android.os.Bundle;
import android.support.v4.app.Fragment;

import com.ibook.barcode.R;
import com.ibook.barcode.content.BundleKey;
import com.starnation.util.StringUtil;

/*
 * @author lsh
 * @since 2016. 4. 18.
*/
public class ContainerActivity extends BaseContainerActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState == null) {
            String className = getIntent().getStringExtra(BundleKey.CONTAINER_FRAGMENT_CLASS_NAME);
            if (StringUtil.isEmpty(className) == true) {
                finish();
                return;
            }

            Bundle arguments = getIntent().getBundleExtra(BundleKey.CONTAINER_FRAGMENT_BUNDLE);
            if (arguments == null) {
                arguments = new Bundle();
            }

            Fragment fragment = Fragment.instantiate(this, className, arguments);
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.container, fragment)
                    .commit();
        }
    }
}
