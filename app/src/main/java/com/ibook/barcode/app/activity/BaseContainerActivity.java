package com.ibook.barcode.app.activity;

import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;

import com.ibook.barcode.R;
import com.ibook.barcode.app.AppTheme;
import com.ibook.barcode.content.LeakFreeSupportSharedElementCallback;
import com.starnation.widget.FullOverlayStatusBarFrameLayout;

/*
 * @author lsh
 * @since 17. 5. 17.
*/
public class BaseContainerActivity extends BaseAppCompatActivity {

    //======================================================================
    // Variables
    //======================================================================

    FullOverlayStatusBarFrameLayout mFullOverlayStatusBarFrameLayout;

    //======================================================================
    // Override Methods
    //======================================================================

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_container);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            ActivityCompat.setEnterSharedElementCallback(this, new LeakFreeSupportSharedElementCallback());
            ActivityCompat.setExitSharedElementCallback(this, new LeakFreeSupportSharedElementCallback());
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        refreshStatusBar();
    }

    //======================================================================
    // Public Methods
    //======================================================================

    public FullOverlayStatusBarFrameLayout getStatusBarFrameLayout() {
        return mFullOverlayStatusBarFrameLayout;
    }

    public void setStatusBarColor(int color) {
        try {
            mFullOverlayStatusBarFrameLayout.setStatusBarBackground(new ColorDrawable(color));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setStatusBarColor(Drawable statusBarColor) {
        try {
            mFullOverlayStatusBarFrameLayout.setStatusBarBackground(statusBarColor);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //======================================================================
    // Private Methods
    //======================================================================

    private void refreshStatusBar() {
        if (mFullOverlayStatusBarFrameLayout != null) {
            try {
                Drawable drawable = mFullOverlayStatusBarFrameLayout.getStatusBarBackground();
                if (drawable == null) {
                    drawable = AppTheme.with().getStatusBarDrawable(this);
                }
                if (drawable != null) {
                    mFullOverlayStatusBarFrameLayout.setStatusBarBackground(drawable);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
