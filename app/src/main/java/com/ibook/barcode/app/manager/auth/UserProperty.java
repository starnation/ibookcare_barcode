package com.ibook.barcode.app.manager.auth;

import com.ibook.barcode.network.model.user.Gender;
import com.ibook.barcode.network.model.user.LoginResult;
import com.ibook.barcode.network.model.user.User;
import com.ibook.barcode.network.model.user.child.Child;
import com.starnation.util.validator.CollectionValidator;

import org.parceler.Parcel;

import java.util.ArrayList;

import static com.ibook.barcode.app.manager.auth.UserSession.INVALID_SEQ;


/*
 * @author lsh
 * @since 2016. 7. 27.
*/
@Parcel
final class UserProperty {

    //======================================================================
    // Variables
    //======================================================================

    String mAccessToken;

    User mUser;

    ArrayList<Child> mChildArrayList;

    int mSelectChildSeq = INVALID_SEQ;

    boolean mLoginComplete;

    //======================================================================
    // Constructor
    //======================================================================

    public UserProperty() {
        // Nothing
    }

    //======================================================================
    // Public Methods
    //======================================================================

    public User getUser() {
        return mUser;
    }

    public String getAccessToken() {
        return mAccessToken;
    }

    public String getEmail() {
        return nonNullUser().getEmail();
    }

    public void updateAccessToken(String accessToken) {
        mAccessToken = accessToken;
    }

    public int getSeq() {
        return nonNullUser().getSeq();
    }

    public User.Status getStatus() {
        return nonNullUser().getStatus();
    }

    public String getName() {
        return nonNullUser().getName();
    }

    public String getBirthday() {
        return nonNullUser().getBirthday();
    }

    public String getSi() {
        return nonNullUser().getSi();
    }

    public String getRegDate() {
        return nonNullUser().getRegDate();
    }

    public boolean isRegisterChild() {
        return CollectionValidator.isValid(mChildArrayList) == true;
    }

    public int getChildCount() {
        return CollectionValidator.getSize(mChildArrayList);
    }

    public int findSelectChildPosition() {
        if (CollectionValidator.isValid(mChildArrayList) == true) {
            if (mSelectChildSeq != INVALID_SEQ) {
                for (int i = 0; i < mChildArrayList.size(); i++) {
                    if (mSelectChildSeq == mChildArrayList.get(i).getChildSeq()) {
                        return i;
                    }
                }
            }
            return 0;
        }
        return INVALID_SEQ;
    }

    public Child getChild(int index) {
        return mChildArrayList.get(index);
    }

    public Child findBySeq(int childSeq) {
        if (CollectionValidator.isValid(mChildArrayList) == true) {
            for (int i = 0; i < mChildArrayList.size(); i++) {
                Child child = mChildArrayList.get(i);
                if (child.getChildSeq() == childSeq) {
                    return child;
                }
            }
        }
        return null;
    }

    public void setSelectChildUser(int childSeq) {
        mSelectChildSeq = childSeq;
    }

    public Child getSelectChild() {
        if (CollectionValidator.isValid(mChildArrayList) == true) {
            if (mSelectChildSeq != INVALID_SEQ) {
                for (Child child : mChildArrayList) {
                    if (child.getChildSeq() == mSelectChildSeq) {
                        return child;
                    }
                }
            }
            return mChildArrayList.get(0);
        }
        return null;
    }

    public ArrayList<Child> getChildArrayList() {
        return mChildArrayList;
    }

    public int getSelectChildSeq() {
        Child child = getSelectChild();
        if (child != null) {
            return child.getChildSeq();
        }
        return INVALID_SEQ;
    }

    public int deleteChild(int deleteChildSeq) {
        int childPosition = findSelectChildPosition();
        if (CollectionValidator.isValid(mChildArrayList, childPosition) == true) {
            mChildArrayList.remove(childPosition);
        }

        return getSelectChildSeq();
    }

    public String getImage() {
        return nonNullUser().getImage();
    }

    public Gender getGender() {
        return nonNullUser().getGender();
    }

    public boolean isRoleGuest() {
        return nonNullUser().isRoleGuest();
    }

    public boolean isKakaoConnected() {
        return nonNullUser().isKakaoConnected();
    }

    public boolean isNaverConnected() {
        return nonNullUser().isNaverConnected();
    }

    public boolean isFacebookConnected() {
        return nonNullUser().isFacebookConnected();
    }

    public boolean isGooglePlusConnected() {
        return nonNullUser().isGooglePlusConnected();
    }

    public boolean isLoginComplete() {
        return mLoginComplete;
    }

    public boolean isLoginActive() {
        return mLoginComplete == true && nonNullUser().isEmpty() == false;
    }

    public void setLoginComplete(boolean loginComplete) {
        mLoginComplete = loginComplete;
    }

    public void release() {
        mAccessToken = null;
        mChildArrayList = null;
        mSelectChildSeq = INVALID_SEQ;
        updateUser(null);
        mLoginComplete = false;
    }

    public void responseUser(User user) {
        if (user != null) {
            updateUser(user);
            mLoginComplete = true;
        }
    }

    public void responseLogin(LoginResult loginResult) {
        if (loginResult != null) {
            mAccessToken = loginResult.getAccessToken();
            updateUser(loginResult.getUser());
            mChildArrayList = loginResult.getChildList();
            mLoginComplete = true;
        }
    }

    //======================================================================
    // Private Methods
    //======================================================================

    private User nonNullUser() {
        if (mUser == null) {
            mUser = User.createEmptyUser();
        }
        return mUser;
    }

    private void updateUser(User user) {
        mUser = user;
    }
}
