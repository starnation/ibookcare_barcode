package com.ibook.barcode.app.manager.auth;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;

import com.ibook.barcode.app.manager.preference.AppPreference;
import com.starnation.android.os.parcel.ParcelUtil;

import lombok.Data;
import lombok.experimental.Delegate;

/*
 * @author lsh
 * @since 2016. 5. 12.
*/
@Data
public final class UserSession {

    //======================================================================
    // Constants
    //======================================================================

    private final static String KEY_USER_SESSION = "KEY_USER_SESSION";

    public final static int INVALID_SEQ = -1;

    //======================================================================
    // Variables
    //======================================================================

    private final static UserSession INSTANCE = new UserSession();

    @Delegate
    private UserProperty mUserProperty = new UserProperty();

    //======================================================================
    // Public Methods
    //======================================================================

    public static UserSession with() {
        return INSTANCE;
    }

    public void init(@NonNull Context context) {
        mUserProperty.updateAccessToken(AppPreference.from(context).account().getAccessToken());
    }

    public void onSaveInstance(Bundle bundle) {
        ParcelUtil.putParcelable(bundle, KEY_USER_SESSION, mUserProperty);
    }

    public void onRestoreInstance(Bundle bundle) {
        UserProperty property = ParcelUtil.getParcelable(bundle, KEY_USER_SESSION);
        if (property != null) {
            mUserProperty = property;
        }
    }
}
