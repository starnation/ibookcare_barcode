package com.ibook.barcode.app.presenter;

import android.os.Bundle;
import android.support.v4.app.Fragment;

import com.ibook.barcode.app.presenter.internal.PageManager;
import com.ibook.barcode.network.NetworkUtil;
import com.ibook.barcode.network.response.BaseResponse;
import com.ibook.barcode.util.log.AppLogger;
import com.ibook.barcode.util.log.Tag;
import com.ibook.barcode.widget.recyclerview.BaseRecyclerViewAdapter;
import com.starnation.okhttp.JsonRequest;
import com.starnation.okhttp.OkHttp;
import com.starnation.okhttp.RequestBuilder;
import com.starnation.widget.recyclerview.scroll.ScrollController;

import java.io.IOException;
import java.util.ArrayList;

import lombok.Getter;

/*
 * @author lsh
 * @since 2017. 5. 17.
*/
public abstract class PagerRecyclerViewPresenter extends RecyclerViewPresenter {

    //======================================================================
    // Constants
    //======================================================================

    private final static int MIN_PAGE_LIST_SIZE = 30;

    //======================================================================
    // Variables
    //======================================================================

    @Getter
    private final PageManager mPageManager = new PageManager();

    private int mMaxCount = Integer.MAX_VALUE;

    private int mTempCancelKey = com.starnation.okhttp.RequestBuilder.NO_CANCEL_KEY;

    @Getter
    private int mMinPageListSize = MIN_PAGE_LIST_SIZE;

    private boolean mDuplicateRequest = false;

    //======================================================================
    // Constructor
    //======================================================================

    public PagerRecyclerViewPresenter(Fragment fragment) {
        super(fragment);
    }

    //======================================================================
    // Abstract Methods
    //======================================================================

    protected abstract PagerProvider onCreateProvider();

    protected abstract PagerRequestProcessor onCreatePagerRequestProcessor();

    //======================================================================
    // Override Methods
    //======================================================================

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPageManager.setLogTag(getClass().getSimpleName());
        mPageManager.restoreInstanceState(savedInstanceState);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mPageManager.saveInstanceState(outState);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        OkHttp.cancelAllRequest(mTempCancelKey);
    }

    //======================================================================
    // Public Methods
    //======================================================================

    public int getMaxCount() {
        return mMaxCount;
    }

    public void setDuplicateRequest(boolean duplicateRequest) {
        mDuplicateRequest = duplicateRequest;
    }

    /**
     * 더보기를 요청할수 있는 최대 리스트 사이즈 설정
     *
     * @param maxCount 최대 리스트 사이즈 0 보다 커야 설정 된다.
     */
    public void setMaxCount(int maxCount) {
        if (maxCount > 0) {
            if (mMaxCount != maxCount) {
                mMaxCount = maxCount;
            }
        }
    }

    /**
     * 요청 전 초기화
     *
     * @param firstPage 처음 페이지 유무
     */
    public final void init(boolean firstPage) {
        mPageManager.init(firstPage);
    }

    public int getPageKey() {
        return mPageManager.getPageKey();
    }

    public void updatePageKey(int pageKey) {
        mPageManager.updatePageKey(pageKey);
    }

    public final boolean isUpdate() {
        return mPageManager.isUpdate();
    }

    public final boolean isFirstPage() {
        return mPageManager.isFirstPage();
    }

    public final int getPage() {
        return mPageManager.getPage();
    }

    public final ArrayList<?> getList() {
        return mPageManager.getList();
    }

    public final Object getItem(int position) {
        return mPageManager.getItem(position);
    }

    public final int getSize() {
        return mPageManager.getSize();
    }

    public final boolean isListUse() {
        return mPageManager.isListUse();
    }

    public final boolean isListUse(int key) {
        return mPageManager.isListUse(key);
    }

    public final boolean remove(Object o) {
        return mPageManager.remove(o);
    }

    public final void remove(int position) {
        mPageManager.remove(position);
    }

    public final void clear() {
        mPageManager.clear();
    }

    public final void clearAll() {
        mPageManager.clearAll();
    }

    /**
     * <p>next page 요청 처리<p/>
     *
     * @param scroll {@link com.starnation.widget.recyclerview.scroll.ScrollController.State}
     *
     * @return next page 있는 경우 true 아니면 false
     *
     * @see #isMaxListSize()
     * @see #mMinPageListSize
     */
    public boolean nextPageRequest(ScrollController.State scroll) {
        if (scroll == ScrollController.State.FULL_DOWN_SCROLL) {
            AppLogger.w(Tag.DEFAULT, "nextPageRequest -> size : " + getSize() + " max : " + mMaxCount + " firstListSize : " + mMinPageListSize);

            //Modify 2016. 11. 25. lsh 첫번째 Page list size 보다 작거나 같으면 nextPageRequest 반환 값을 false 로 반환 한다.
            if (getSize() < mMinPageListSize) {
                return false;
            }

            //Modify 2016. 11. 25. lsh maxCount 보다 Page list size 가 작으면 nextPageRequest 반환 값을 true 로 반환 한다.
            if (isMaxListSize() == false) {
                return true;
            }
        }
        return false;
    }

    public final void addAllPageList(ArrayList<?> list) {
        mPageManager.addAllPageList(list);
    }

    @SuppressWarnings("unchecked")
    public final void requestPager(JsonRequest.OnStatusListener statusListener, JsonRequest.OnResponseListener<? extends BaseResponse> listener) throws Exception {
        PagerRequestProcessor processor = onCreatePagerRequestProcessor();
        processor.valid();

        RequestBuilder builder = processor.onCreateBuilder().duplicateRequest(mDuplicateRequest);

        mTempCancelKey = NetworkUtil.getCancelKey(builder.getSuffixUrl(), hashCode());

        builder.cancelKey(mTempCancelKey)
                .statusListener(statusListener)
                .execute(new OnPageListWrapper(listener, processor));
    }

    /**
     * @return {@link #getSize()} >= {@link #getMaxCount()} true 인지 체크
     */
    public boolean isMaxListSize() {
        return getSize() >= mMaxCount;
    }

    //======================================================================
    // Protected Methods
    //======================================================================

    /**
     * <p>{@link #requestPager} 요청후 응답 성공시 응답.</p>
     * {@link PagerRequestProcessor#parserList(BaseResponse)} 통해서 반환된 값을 list 인자로 전달 받는다.
     *
     * @param list 추가할 리스트
     *
     * @see #addAllPageList(ArrayList)
     * @see #requestPager(JsonRequest.OnStatusListener, JsonRequest.OnResponseListener)
     */
    protected void onPagerResponseResult(ArrayList<?> list) {
        mPageManager.addAllPageList(list);
    }

    protected void onPagerError(IOException ioe) {
        // Override
    }

    //======================================================================
    // PagerRequestProcessor
    //======================================================================

    protected interface PagerRequestProcessor<Result> {

        void valid() throws IllegalArgumentException;

        @SuppressWarnings("unchecked")
        RequestBuilder onCreateBuilder();

        ArrayList<?> parserList(BaseResponse<Result> response);

        /**
         * 데이터 응답시 {@link #addAllPageList(ArrayList)} 보다 먼저 호출
         *
         * @param response
         */
        void onResult(BaseResponse<Result> response);
    }

    //======================================================================
    // PagerProvider
    //======================================================================

    protected abstract class PagerProvider implements BaseRecyclerViewAdapter.ArgumentsProvider {

        @Override
        public int getSupportItemCount() {
            return mPageManager.getSize();
        }

        @Override
        public Object getSupportItem(int viewType, int position) {
            if (position < getItemHeaderCount()) {
                return null;
            }
            int itemPosition = position - getItemHeaderCount();
            return mPageManager.getItem(itemPosition);
        }
    }

    //======================================================================
    // OnPageListWrapper
    //======================================================================

    final class OnPageListWrapper<Result> extends OnResponseListenerWrapper<BaseResponse<Result>> {

        private PagerRequestProcessor mProcessor;

        public OnPageListWrapper(JsonRequest.OnResponseListener<BaseResponse<Result>> listener, PagerRequestProcessor processor) {
            super(listener);
            mProcessor = processor;
        }

        @SuppressWarnings("unchecked")
        @Override
        public void onResult(BaseResponse<Result> resultBaseResponse) {
            ArrayList<?> list = mProcessor.parserList(resultBaseResponse);
            mProcessor.onResult(resultBaseResponse);
            onPagerResponseResult(list);
        }

        @Override
        protected void onError(IOException ioe) {
            super.onError(ioe);
            onPagerError(ioe);
        }

        @Override
        public final void complete() {
            mPageManager.complete();
            mProcessor = null;
            super.complete();
        }
    }
}
