package com.ibook.barcode.app.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import com.ibook.barcode.app.model.bundle.FragmentBundle;
import com.ibook.barcode.util.log.AppLogger;
import com.ibook.barcode.util.log.Tag;
import com.starnation.android.BuildConfig;
import com.starnation.android.app.fragment.DialogFragment.OnDismissCallback;
import com.starnation.android.app.fragment.DialogFragmentSupport;

/*
 * @author lsh
 * @since 2017. 5. 17.
*/
public final class Fragments {

    public static Fragment createFragment(@NonNull Context context, Class<? extends Fragment> fragment, FragmentBundle arguments) {
        return createFragmentInternal(context, fragment, fromBundle(arguments));
    }

    public static void showDialogFragment(FragmentManager manager, Class<? extends DialogFragment> target, FragmentBundle arguments) {
        showDialogFragmentInternal(manager, target, fromBundle(arguments), null);
    }

    public static void showDialogFragment(FragmentManager manager, Class<? extends DialogFragment> target, FragmentBundle arguments, OnDismissCallback callback) {
        showDialogFragmentInternal(manager, target, fromBundle(arguments), callback);
    }

    //======================================================================
    // Private Methods
    //======================================================================

    private static Bundle fromBundle(FragmentBundle bundle) {
        if (bundle == null) {
            return new Bundle();
        }
        return bundle.fromBundle();
    }

    private static Fragment createFragmentInternal(@NonNull Context context, @NonNull Class<? extends Fragment> fragment, Bundle arguments) {
        return Fragment.instantiate(context, fragment.getName(), arguments);
    }

    private static void showDialogFragmentInternal(FragmentManager manager, Class<? extends DialogFragment> target, Bundle arguments, OnDismissCallback callback) {
        try {
            String tag = target.getName();
            FragmentTransaction transaction = manager.beginTransaction();
            Fragment prev = manager.findFragmentByTag(tag);
            if (prev != null) {
                transaction.remove(prev).commit();
            }

            DialogFragment dialogFragment = target.newInstance();
            if (dialogFragment instanceof DialogFragmentSupport) {
                ((DialogFragmentSupport) dialogFragment).setDismissCallback(callback);
            }
            dialogFragment.setArguments(arguments);
            dialogFragment.show(manager, tag);
        } catch (Exception e) {
            if (BuildConfig.DEBUG) {
                AppLogger.e(Tag.FRAGMENT_CYCLE, e.getMessage());
            }
        }
    }
}
