package com.ibook.barcode.app.presenter.internal;

import org.parceler.Parcel;

/*
 * @author lsh
 * @since 2016. 7. 21.
*/
@Parcel
final class PageInfo {

    //======================================================================
    // Constants
    //======================================================================

    public final static int FIRST_PAGE = 1;

    //======================================================================
    // Variables
    //======================================================================

    int mPage = FIRST_PAGE;

    int mDownloadCount = 0;

    boolean mUpdate;

    //======================================================================
    // Constructor
    //======================================================================

    public PageInfo() {
        // Nothing
    }

    //======================================================================
    // Public Methods
    //======================================================================

    public void init(boolean firstPage) {
        if (firstPage == true) {
            mPage = FIRST_PAGE;
        }
        mUpdate = false;
    }

    public int getPage() {
        return mPage;
    }

    public void setPage(int page) {
        mPage = page;
    }

    public boolean isUpdate() {
        return mUpdate;
    }

    public void setUpdate(boolean update) {
        mUpdate = update;
    }

    public void complete() {
        if (mUpdate == true) {
            nextPage();
            mUpdate = false;
        }
    }

    public void nextPage() {
        mPage++;
    }

    public boolean isFirstPage() {
        return mPage == FIRST_PAGE;
    }
}
