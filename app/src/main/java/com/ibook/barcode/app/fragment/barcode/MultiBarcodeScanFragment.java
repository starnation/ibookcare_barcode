package com.ibook.barcode.app.fragment.barcode;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.ibook.barcode.R;
import com.ibook.barcode.app.fragment.BaseFragment;
import com.ibook.barcode.app.fragment.FragmentDelegate;
import com.ibook.barcode.app.fragment.Fragments;
import com.ibook.barcode.app.model.bundle.EventBundle;
import com.ibook.barcode.app.model.bundle.ViewHolderBundle;
import com.ibook.barcode.app.model.eventbus.Event;
import com.ibook.barcode.app.presenter.barcode.MultiBarcodeScanPresenter;
import com.ibook.barcode.network.model.book.BookItem;
import com.ibook.barcode.network.response.BaseResponse;
import com.ibook.barcode.util.Objects;
import com.ibook.barcode.util.Util;
import com.ibook.barcode.util.log.AppLogger;
import com.ibook.barcode.util.toast.AppToast;
import com.ibook.barcode.widget.barcode.ScanView;
import com.ibook.barcode.widget.dialog.AlertDialogs;
import com.ibook.barcode.widget.recyclerview.viewholder.OnRecyclerViewHolderEventListener;
import com.starnation.android.app.fragment.DialogFragment;
import com.starnation.okhttp.JsonRequest;
import com.starnation.util.StringUtil;
import com.starnation.util.validator.InvalidException;
import com.starnation.util.validator.IsbnValidator;
import com.starnation.widget.recyclerview.RecyclerViewHolder;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Request;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;

import static com.google.android.gms.vision.barcode.Barcode.ISBN;

/*
 * @author lsh
 * @since 2017. 5. 17.
*/
public abstract class MultiBarcodeScanFragment<P extends MultiBarcodeScanPresenter> extends BaseFragment<P> implements OnRecyclerViewHolderEventListener {

    //======================================================================
    // Variables
    //======================================================================

    ScanView mScanView;

    AlertDialog mAlertDialog;

    //======================================================================
    // Abstract Methods
    //======================================================================

    /**
     * 바코드 카메라 위에 표시할 content view 생성
     *
     * @param inflater  {@inheritDoc}
     * @param container {@inheritDoc}
     *
     * @return {@link View}
     *
     * @see #onCreateView(LayoutInflater, ViewGroup, Bundle)
     */
    protected abstract View onCreateContentView(LayoutInflater inflater, ViewGroup container);

    //======================================================================
    // Override Methods
    //======================================================================

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onApplyConfig(@NonNull FragmentDelegate.Config config) {
        super.onApplyConfig(config);
        config.setShowTitle(false);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_multi_barcode_scan, container, false);
        CoordinatorLayout parent = (CoordinatorLayout) view.findViewById(R.id.layout_parent);
        if (parent != null) {
            View contentView = onCreateContentView(inflater, container);
            if (contentView == null) {
                throw new NullPointerException("ContentView null");
            }
            CoordinatorLayout.LayoutParams params = new CoordinatorLayout.LayoutParams(CoordinatorLayout.LayoutParams.MATCH_PARENT, CoordinatorLayout.LayoutParams.MATCH_PARENT);
            parent.addView(contentView, params);
        }
        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_self_input, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {
            case R.id.menu_self_input:
                Fragments.showDialogFragment(getFragmentManager(),
                        InputFragment.class,
                        null,
                        new DialogFragment.OnDismissCallback() {
                            @Override
                            public void onDismiss(@NonNull Bundle event) {
                                processSelfBarcodeScanResult(EventBundle.asBundle(event));
                            }
                        });
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mScanView = (ScanView) view.findViewById(R.id.scanview);
        if (mScanView != null) {
            mScanView.setSupportFormat(ISBN);
        }
    }

    @Override
    public void onRecyclerViewHolderEvent(int id, @NonNull RecyclerViewHolder holder) {
        // Nothing
    }

    @Override
    public void onRecyclerViewHolderEvent(ViewHolderBundle arguments, @NonNull RecyclerViewHolder holder) {
        // Nothing
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mScanView != null) {
            Observable.just(mScanView)
                    .delay(400, TimeUnit.MILLISECONDS, AndroidSchedulers.mainThread())
                    .subscribe(new Subscriber<ScanView>() {
                        @Override
                        public void onCompleted() {

                        }

                        @Override
                        public void onError(Throwable e) {

                        }

                        @Override
                        public void onNext(ScanView scanView) {
                            scanView.onResume();
                        }
                    });
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (mScanView != null) {
            mScanView.onPause();
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (mScanView != null) {
            mScanView.onRelease();
        }
    }

    @Override
    public void onEvent(Event event) {
        super.onEvent(event);
        switch(event.getEventKey()) {
            case RESULT_BARCODE:
                processBarcodeScanResult(event.getEventBundle());
                break;
        }
    }

    //======================================================================
    // Protected Methods
    //======================================================================

    /**
     * @param bookItem
     */
    protected abstract void onIsbnResponse(BookItem bookItem);

    protected void isbnValid(String isbn) throws IllegalArgumentException {
        //override
    }

    //======================================================================
    // Private Methods
    //======================================================================

    private void processBarcodeScanResult(EventBundle eventBundle) {
        try {
            if (mAlertDialog != null && mAlertDialog.isShowing()) {
                throw new InvalidException("Dialog Show");
            }

            long current = System.currentTimeMillis();
            long time = System.currentTimeMillis() - getPresenter().getLastBarcodeTime();
            if (time < getPresenter().getBarcodeInterval()) {
                throw new InvalidException("Not current time interval " + time);
            }

            if (Util.isTargetFragmentTop(MultiBarcodeScanFragment.this) == true) {
                requestISBN(eventBundle.getScanResult(), eventBundle.getScanResultFormat());
            }

            getPresenter().setLastBarcodeTime(current);
        } catch (InvalidException e) {
            e.printStackTrace();
        }
    }

    private void processSelfBarcodeScanResult(EventBundle eventBundle) {
        try {
            Objects.requireNonNull(eventBundle);

            requestISBN(eventBundle.getScanResult(), eventBundle.getScanResultFormat());

            getPresenter().setLastBarcodeTime(System.currentTimeMillis());
        } catch (Exception e) {
            AppLogger.printStackTrace(e);
        }
    }

    @SuppressWarnings({"unused", "PointlessBooleanExpression"})
    private void requestISBN(final String isbn, String format) {
        try {
            IsbnValidator.valid(isbn);
            isbnValid(isbn);

            getPresenter().requestIsbn(isbn,
                    format,
                    this,
                    new JsonRequest.OnResponseListener<BaseResponse<BookItem>>() {
                        @Override
                        public void onSuccess(BaseResponse<BookItem> response) {
                            onIsbnResponse(response.getResult());
                        }

                        @Override
                        public void onError(Request request, IOException ioe) {
                            mAlertDialog = AlertDialogs.create(getContext(),
                                    ioe.getMessage(),
                                    R.string.app_confirm,
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            mAlertDialog.dismiss();
                                            mAlertDialog = null;
                                        }
                                    });
                            mAlertDialog.show();
                        }
                    });
        } catch (IllegalAccessException ill) {
            AppLogger.printStackTrace(ill);
        } catch (IllegalArgumentException ill) {
            if (AppToast.with().isToastShow() == false) {
                if (StringUtil.isEmpty(ill.getMessage()) == false) {
                    AppToast.with().showDefaultShortMessage(ill.getMessage());
                }
            }
        } catch (Exception e) {
            if (AppToast.with().isToastShow() == false) {
                AppToast.with().showDefaultShortMessage(R.string.app_hint_input_retry_barcode);
            }
        }
    }
}
