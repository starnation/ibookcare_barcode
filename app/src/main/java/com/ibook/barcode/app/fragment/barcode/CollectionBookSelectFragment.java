package com.ibook.barcode.app.fragment.barcode;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ibook.barcode.R;
import com.ibook.barcode.app.fragment.BaseBottomSheetDialogFragment;
import com.ibook.barcode.app.model.bundle.FragmentBundle;
import com.ibook.barcode.app.presenter.barcode.CollectionBookSelectPresenter;
import com.ibook.barcode.app.viewholder.BookOneColumnViewHolder;
import com.ibook.barcode.network.response.BaseResponse;
import com.ibook.barcode.util.log.AppLogger;
import com.ibook.barcode.util.log.Tag;
import com.ibook.barcode.util.toast.AppToast;
import com.ibook.barcode.widget.recyclerview.BaseRecyclerViewAdapter;
import com.starnation.android.view.ViewUtil;
import com.starnation.okhttp.JsonRequest;
import com.starnation.widget.CircularProgressBar;
import com.starnation.widget.recyclerview.AdapterUtil;
import com.starnation.widget.recyclerview.RecyclerViewAdapter;
import com.starnation.widget.recyclerview.RecyclerViewHolder;
import com.starnation.widget.recyclerview.scroll.ScrollController;

import java.io.IOException;

import butterknife.BindView;
import butterknife.OnClick;
import okhttp3.Request;

/*
 * @author lsh
 * @since 2017. 5. 17.
*/
public class CollectionBookSelectFragment extends BaseBottomSheetDialogFragment<CollectionBookSelectPresenter> {

    //======================================================================
    // Variables
    //======================================================================

    @BindView(R.id.progressbar)
    CircularProgressBar mProgressbar;

    @BindView(R.id.textview_dialog_title)
    AppCompatTextView mTextviewDialogTitle;

    @BindView(R.id.recyclerview)
    RecyclerView mRecyclerview;

    //======================================================================
    // Override Methods
    //======================================================================

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_dialog_collection_book_select, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mTextviewDialogTitle.setText(getSupportArguments().getTitle());
        initRecyclerView();
    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
        requestPager(true);
    }

    //======================================================================
    // Methods
    //======================================================================

    @OnClick(R.id.button_complete)
    void onViewClicked() {
        dismiss(new FragmentBundle()
                .bookCodeList(getPresenter().makeCheckedBookCodeList())
                .fromBundle());
    }

    //======================================================================
    // Private Methods
    //======================================================================

    private void initRecyclerView() {
        mRecyclerview.setLayoutManager(new StaggeredGridLayoutManager(1, StaggeredGridLayoutManager.VERTICAL));
        mRecyclerview.addOnScrollListener(new ScrollController() {
            @Override
            public void onScrollState(State state) {
                if (getPresenter().nextPageRequest(state) == true) {
                    requestPager(false);
                }
            }
        });

        BaseRecyclerViewAdapter adapter = new BaseRecyclerViewAdapter() {
            @Override
            public RecyclerViewHolder onCreateItemViewHolder(ViewGroup parent, int viewType) {
                BookOneColumnViewHolder holder = new BookOneColumnViewHolder(CollectionBookSelectFragment.this, parent);
                holder.setMultiSelector(getPresenter().getSelector());
                return holder;
            }
        };
        adapter.setNonItemHeaderRemove(true);
        adapter.setProvider(getPresenter().getAdapterProvider());
        mRecyclerview.setAdapter(adapter);
    }

    private BaseRecyclerViewAdapter getAdapter() {
        return (BaseRecyclerViewAdapter) mRecyclerview.getAdapter();
    }

    private void requestPager(final boolean firstPage) {
        try {
            getPresenter().init(firstPage);
            getPresenter().requestPager(new JsonRequest.OnStatusListener() {
                @Override
                public void onNetworkStart() {
                    if (firstPage == true) {
                        ViewUtil.show(mProgressbar);
                    }
                }

                @Override
                public void onNetworkEnd() {
                    ViewUtil.hide(mProgressbar);
                }
            }, new JsonRequest.OnResponseListener<BaseResponse<?>>() {
                @Override
                public void onSuccess(BaseResponse<?> baseResponse) {
                    notifyRecyclerViewPager();
                }

                @Override
                public void onError(Request request, IOException ioe) {
                    AppToast.with().showDefaultShortMessage(ioe.getMessage());
                }
            });
        } catch (Exception e) {
            onRequestException();
        }
    }

    private void onRequestException() {
        completeRequest();
        getPresenter().clear();
        AdapterUtil.notifySupportDataSetChanged((RecyclerViewAdapter) mRecyclerview.getAdapter(), getPresenter().isUpdate());
    }

    private void notifyRecyclerViewPager() {
        try {
            boolean firstPage = getPresenter().isListUse() == true && getPresenter().isFirstPage() == true;
            RecyclerView.LayoutManager manager = mRecyclerview.getLayoutManager();

            RecyclerViewAdapter adapter = getAdapter();

            AdapterUtil.notifySupportDataSetChanged(adapter, getPresenter().isUpdate());

            if (firstPage == true) {
                manager.scrollToPosition(0);
            }
            completeRequest();
        } catch (Exception e) {
            completeRequest();
            AppLogger.e(Tag.FRAGMENT_CYCLE, e.getMessage());
        }
    }

    private void completeRequest() {
        ViewUtil.hide(mProgressbar);
    }
}
