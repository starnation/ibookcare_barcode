package com.ibook.barcode.app.presenter;

import android.os.Bundle;
import android.support.v4.app.Fragment;

import com.ibook.barcode.widget.recyclerview.BaseRecyclerViewAdapter;

/*
 * @author lsh
 * @since 2017. 5. 17.
*/
public abstract class RecyclerViewPresenter extends Presenter {

    //======================================================================
    // Variables
    //======================================================================

    private BaseRecyclerViewAdapter.Provider mAdapterProvider;

    //======================================================================
    // Constructor
    //======================================================================

    /**
     * <p>{@link Fragment#onCreate(Bundle)}  생성할때 호출.</p>
     * <p>{@link Fragment} 관련 처리는 {@link Fragment#onCreate(Bundle)} 에서 처리한다.</p>
     *
     * @see Fragment#onCreate(Bundle)
     */
    public RecyclerViewPresenter(Fragment fragment) {
        super(fragment);
    }

    //======================================================================
    // Abstract Methods
    //======================================================================

    /**
     * {@link Fragment#onCreate(Bundle)} 생성하고 {@link Fragment#onDestroy()} 일때 해제한다.
     * {@link #onCreateProvider()} 생성을 안하면 {@link NullPointerException} 발생한다.
     *
     * @return {@link BaseRecyclerViewAdapter.Provider}
     */
    protected abstract BaseRecyclerViewAdapter.Provider onCreateProvider();

    //======================================================================
    // Override Methods
    //======================================================================

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mAdapterProvider = onCreateProvider();
        if (mAdapterProvider == null) {
            throw new NullPointerException("BaseRecyclerViewAdapter.Provider null");
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mAdapterProvider = null;
    }

    //======================================================================
    // Public Methods
    //======================================================================

    public BaseRecyclerViewAdapter.Provider getAdapterProvider() {
        return mAdapterProvider;
    }
}
