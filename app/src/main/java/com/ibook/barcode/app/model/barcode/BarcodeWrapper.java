package com.ibook.barcode.app.model.barcode;

import com.ibook.barcode.network.model.Base;
import com.ibook.barcode.network.model.book.BookCollection;
import com.ibook.barcode.network.model.book.BookItem;
import com.starnation.util.StringUtil;
import com.starnation.util.validator.CollectionValidator;

import java.util.ArrayList;

import lombok.Getter;
import lombok.Setter;

/*
 * @author lsh
 * @since 2017. 5. 17.
*/
public class BarcodeWrapper extends Base {

    //======================================================================
    // Variables
    //======================================================================

    @Getter
    BookItem mBookItem;

    @Getter
    BarcodeCollection mBarcodeCollection;

    @Getter
    int mAppendCount;

    @Getter
    @Setter
    long mLastTimeStamp;

    //======================================================================
    // Constructor
    //======================================================================

    public BarcodeWrapper(BookItem bookItem) {
        mBookItem = bookItem;
    }

    //======================================================================
    // Override Methods
    //======================================================================

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof BookItem) {
            if (getBarcodeCollection() != null) {
                //Modify 2017. 4. 14. vkwofm 전집의 경우 childList가 없으면 전체책 포함 상태
                if (CollectionValidator.isValid(getBarcodeCollection().getChildList()) == true) {
                    for (String bookCode : getBarcodeCollection().getChildList()) {
                        if (bookCode.equals(((BookItem) obj).getBookCode()) == true) {
                            return true;
                        }
                    }
                } else {
                    if (collectionEquals(obj) == true) {
                        return true;
                    }
                }
            }
            return getBookCode().equals(((BookItem) obj).getBookCode());
        }
        return super.equals(obj);
    }

    //======================================================================
    // Public Methods
    //======================================================================

    public boolean collectionEquals(Object obj) {
        if (obj instanceof BookItem) {
            BookItem target = (BookItem) obj;
            if (getBookCollection() != null && StringUtil.isEmpty(getBookCollection().getSetCode()) == false) {
                return getSetCode().equals(target.getBookCollection().getSetCode());
            }
        }
        return false;
    }

    public BookCollection getBookCollection() {
        return mBookItem.getBookCollection();
    }

    public boolean isCollection() {
        return getBarcodeCollection() != null;
    }

    public void updateCollectionType(int bookCount, ArrayList<String> bookList) {
        mBarcodeCollection = new BarcodeCollection(bookCount, bookList);
    }

    @SuppressWarnings("PointlessBooleanExpression")
    public String getCoverAladin() {
        if (isCollection() == true) {
            return getBookCollection().getImage();
        }
        return mBookItem.getCoverAladin();
    }

    @SuppressWarnings("PointlessBooleanExpression")
    public String getBookCode() {
        return mBookItem.getBookCode();
    }

    public String toBookCode() {
        if (isCollection() == true) {
            return mBookItem.getBookInfo().getCollectionBookCode();
        }
        return getBookCode();
    }

    public String getSetCode() {
        return getBookCollection().getSetCode();
    }

    public ArrayList<String> getChildBookCodeList() {
        if (isCollection() == true) {
            return getBarcodeCollection().getChildList();
        }
        return null;
    }

    public void setAppendCount(int appendCount) {
        mAppendCount = appendCount;
    }
}
