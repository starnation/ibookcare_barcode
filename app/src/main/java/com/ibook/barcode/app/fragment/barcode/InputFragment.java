package com.ibook.barcode.app.fragment.barcode;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.ibook.barcode.R;
import com.ibook.barcode.app.fragment.common.BottomDialogFragment;
import com.ibook.barcode.app.model.bundle.EventBundle;
import com.ibook.barcode.util.toast.AppToast;
import com.starnation.android.view.Keyboard;
import com.starnation.util.validator.IsbnValidator;

import butterknife.BindView;
import butterknife.OnClick;

/*
 * @author lsh
 * @since 2017. 5. 17.
*/
public final class InputFragment extends BottomDialogFragment {

    //======================================================================
    // Variables
    //======================================================================

    @BindView(R.id.edittext_barcode)
    EditText mBarcodeEditText;

    //======================================================================
    // Override Methods
    //======================================================================

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_barcode_input, container, false);
    }

    @Override
    public void onViewCreated(final View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mBarcodeEditText.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getKeyCode() == KeyEvent.KEYCODE_ENTER) {
                    sendBarcode();
                    return true;
                }
                return false;
            }
        });
    }

    //======================================================================
    // Methods
    //======================================================================

    @OnClick({R.id.button_confirm})
    void onClick(View view) {
        switch(view.getId()) {
            case R.id.button_confirm:
                sendBarcode();
                break;
        }
    }

    //======================================================================
    // Private Methods
    //======================================================================

    private void sendBarcode() {
        try {
            IsbnValidator.valid(mBarcodeEditText.getText().toString());

            hideKeyboard();

            dismiss(new EventBundle()
                    .scanResult(mBarcodeEditText.getText().toString())
                    .scanResultFormat("Input Format").fromBundle());
        } catch (Exception e) {
            AppToast.with().showMessage(R.string.app_hint_input_retry_barcode, Toast.LENGTH_LONG);
        }
    }

    @SuppressWarnings("unused")
    private void showKeyboard() {
        mBarcodeEditText.requestFocus();
        mBarcodeEditText.setCursorVisible(true);
        Keyboard.showKeyboard(mBarcodeEditText);
    }

    private boolean hideKeyboard() {
        mBarcodeEditText.clearFocus();
        mBarcodeEditText.setCursorVisible(false);
        Keyboard.hideKeyboard(mBarcodeEditText);
        return true;
    }
}
