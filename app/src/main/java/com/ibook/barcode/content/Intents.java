package com.ibook.barcode.content;

import android.app.Activity;
import android.app.ActivityOptions;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;

import com.ibook.barcode.app.activity.ContainerActivity;
import com.ibook.barcode.app.fragment.barcode.BookRegisterFragment;

/*
 * @author lsh
 * @since 2017. 5. 17.
*/
public final class Intents {

    public static void goBarcode(@NonNull Context context) {
        goActivityContainerInternal(context, ContainerActivity.class, BookRegisterFragment.class, null, null);
    }

    //======================================================================
    // Private Methods
    //======================================================================

    private static void goActivityContainerInternal(@NonNull Context context, Class<? extends Activity> activity, Class<? extends Fragment> fragment, Bundle bundle, ActivityOptions options) {
        Intent intent = new Intent(context, activity);
        if (fragment != null) {
            intent.putExtra(BundleKey.CONTAINER_FRAGMENT_CLASS_NAME, fragment.getName());
        }
        intent.putExtra(BundleKey.CONTAINER_FRAGMENT_BUNDLE, bundle);

        if (options != null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                context.startActivity(intent, options.toBundle());
            } else {
                context.startActivity(intent);
            }
        } else {
            context.startActivity(intent);
        }
    }
}
