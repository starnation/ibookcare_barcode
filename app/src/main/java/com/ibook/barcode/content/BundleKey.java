package com.ibook.barcode.content;

/*
 * @author lsh
 * @since 17. 5. 17.
*/
public final class BundleKey {

    public final static String ACTIVITY_BUNDLE_DATA = "ACTIVITY_BUNDLE_DATA";

    public final static String CONTAINER_FRAGMENT_CLASS_NAME = "CONTAINER_FRAGMENT_CLASS_NAME";

    public final static String CONTAINER_FRAGMENT_BUNDLE = "CONTAINER_FRAGMENT_BUNDLE";
}
