package com.ibook.barcode.network;

import android.text.TextUtils;

import com.google.gson.GsonBuilder;
import com.ibook.barcode.BuildConfig;
import com.ibook.barcode.network.response.BaseResponse;
import com.ibook.barcode.util.log.AppLogger;
import com.ibook.barcode.util.log.Tag;
import com.starnation.okhttp.Method;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import okhttp3.RequestBody;
import okhttp3.Response;

/*
 * @author lsh
 * @since 2017. 5. 17.
*/
public final class NetworkLog {

    //======================================================================
    // Constants
    //======================================================================

    private final static String FORMAT_RESULT_TITLE = "########################### %s ###########################";

    private final static String FORMAT_INFO = "%s : %s";

    private final static String FORMAT = "[%s] : %s";

    //======================================================================
    // Public Methods
    //======================================================================

    public static void request(IBookUrl iBookUrl, String url, Method method, Map<String, String> map, RequestBody body) {
        try {
            valid();
            String format = "RequestBuilder %s";

            log(Tag.NETWORK_REQUEST, iBookUrl, String.format(Locale.US, FORMAT_RESULT_TITLE, String.format(Locale.US, format, url)));
            log(Tag.NETWORK_REQUEST, iBookUrl, "method " + method + " url : " + url);

            if (body != null) {
                log(Tag.NETWORK_REQUEST, iBookUrl, "body -> contentType : " + body.contentType() + " contentLength : " + body.contentLength() + " other : " + body.toString());
            }

            if (map != null) {
                for (Map.Entry<String, String> entry : map.entrySet()) {
                    String decoder = URLDecoder.decode(entry.getValue(), "utf-8");
                    log(Tag.NETWORK_REQUEST, iBookUrl, String.format(Locale.US, FORMAT_INFO, entry.getKey(), decoder));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @SuppressWarnings("ConstantConditions")
    public static void response(Response response, BaseResponse baseResponse, String result) {
        try {
            valid();

            String url = response.request().url().url().toString();
            log(Tag.NETWORK_RESPONSE, String.format(Locale.US, FORMAT_RESULT_TITLE, url));
            log(Tag.NETWORK_RESPONSE, "status : " + baseResponse.getStatus());
            log(Tag.NETWORK_RESPONSE, "message : " + baseResponse.getMessage());
            if (baseResponse.getResult() != null) {
                printResult(new GsonBuilder().setPrettyPrinting().create().toJson(baseResponse.getResult()));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //======================================================================
    // Private Methods
    //======================================================================

    private static void printResult(String json) {
        try {
            List<String> depth = new ArrayList<>();

            if (json.startsWith("[") == true && json.endsWith("]") == true) {
                printJsonArray("result", json, depth);
            } else if (json.startsWith("{") == true && json.endsWith("}") == true) {
                printJsonObject(json, depth);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void printJsonArray(String key, String json, List<String> jsonDepth) throws Exception {
        try {
            JSONArray jsonArray = new JSONArray(json);
            if (jsonArray.length() > 0) {
                for (int i = 0; i < jsonArray.length(); i++) {
                    if (jsonArray.isNull(i) == false) {
                        ArrayList<String> temp = new ArrayList<>();
                        temp.addAll(jsonDepth);
                        temp.add(key + "[" + i + "]");

                        Object object = jsonArray.get(i);

                        if (object instanceof JSONObject) {
                            printJsonObject(object.toString(), temp);
                        } else if (object instanceof JSONArray) {
                            printJsonArray(key, object.toString(), temp);
                        } else {
                            log(Tag.NETWORK_RESPONSE_RESULT, toJsonInfo(toJsonDepth(temp), object.toString()));
                        }
                    }
                }
            }
        } catch (OutOfMemoryError e) {
            // Nothing
        }
    }

    private static void printJsonObject(String json, List<String> jsonDepth) throws Exception {
        try {
            JSONObject jsonObject = new JSONObject(json);
            Iterator<String> iterator = jsonObject.keys();

            JSONObject tempJsonObject = new JSONObject();

            while (iterator.hasNext()) {
                String key = iterator.next();
                Object value = jsonObject.get(key);

                ArrayList<String> temp = new ArrayList<>();
                temp.addAll(jsonDepth);

                if (jsonObject.isNull(key) == false) {
                    if (value instanceof JSONObject) {
                        temp.add(key);

                        printJsonObject(value.toString(), temp);
                    } else if (value instanceof JSONArray) {
                        printJsonArray(key, value.toString(), temp);
                    } else {
                        tempJsonObject.put(key, value);
                    }
                }
            }

            if (tempJsonObject.length() > 0) {
                log(Tag.NETWORK_RESPONSE_RESULT, asString(toJsonDepth(jsonDepth), tempJsonObject));
            }
        } catch (OutOfMemoryError e) {
            // Nothing
        }
    }

    private static String asString(String key, JSONObject jsonObject) throws JSONException {
        try {
            return String.format(Locale.US, FORMAT_INFO, key, jsonObject.toString(3));
        } catch (OutOfMemoryError e) {
            return String.format(Locale.US, FORMAT_INFO, key, "");
        }
    }

    private static String toJsonInfo(String key, String value) throws JSONException {
        try {
            return String.format(Locale.US, FORMAT_INFO, key, value);
        } catch (OutOfMemoryError e) {
            return String.format(Locale.US, FORMAT_INFO, key, "");
        }
    }

    private static void valid() throws IllegalStateException {
        if (BuildConfig.DEBUG == false) {
            throw new IllegalStateException("not debug mode");
        }
    }

    private static void log(Tag tag, IBookUrl iBookUrl, String message) {
        switch(tag) {
            case NETWORK_ERROR:
                AppLogger.e(tag, String.format(Locale.US, FORMAT, iBookUrl.toString(), message));
                break;
            case NETWORK_REQUEST:
                AppLogger.i(tag, String.format(Locale.US, FORMAT, iBookUrl.toString(), message));
                break;
            case NETWORK_RESPONSE:
            case NETWORK_RESPONSE_RESULT:
                AppLogger.v(tag, String.format(Locale.US, FORMAT, iBookUrl.toString(), message));
                break;
        }
    }

    private static void log(Tag tag, String message) {
        switch(tag) {
            case NETWORK_ERROR:
                AppLogger.e(tag, message);
                break;
            case NETWORK_REQUEST:
                AppLogger.i(tag, message);
                break;
            case NETWORK_RESPONSE:
            case NETWORK_RESPONSE_RESULT:
                AppLogger.v(tag, message);
                break;
        }
    }

    private static String toJsonDepth(List<String> depth) {
        if (depth != null && depth.size() > 0) {
            return TextUtils.join("-> ", depth);
        }
        return "";
    }
}
