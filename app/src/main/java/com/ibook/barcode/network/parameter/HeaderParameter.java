package com.ibook.barcode.network.parameter;

import android.support.annotation.NonNull;

import com.ibook.barcode.BuildConfig;
import com.ibook.barcode.app.manager.auth.UserSession;
import com.starnation.util.StringUtil;

import org.parceler.Parcel;

import java.util.Map;

import static com.ibook.barcode.network.parameter.ParameterKey.ANDROID_APP_DEPLOY;
import static com.ibook.barcode.network.parameter.ParameterKey.ANDROID_APP_VERSION_CODE;
import static com.ibook.barcode.network.parameter.ParameterKey.ANDROID_APP_VERSION_NAME;
import static com.ibook.barcode.network.parameter.ParameterKey.API_VERSION;
import static com.ibook.barcode.network.parameter.ParameterKey.AUTHORIZATION;

/*
 * @author lsh
 * @since 2017. 5. 17.
*/
@Parcel
public final class HeaderParameter extends BaseParameter<HeaderParameter> {

    //======================================================================
    // Constructor
    //======================================================================

    public HeaderParameter() {
        this(ValueState.DEFAULT);
    }

    public HeaderParameter(@NonNull ValueState valueState) {
        super(valueState);
        putInternal(ANDROID_APP_VERSION_NAME, BuildConfig.VERSION_NAME.replaceAll("g|d|c|o", ""));
        putInternal(ANDROID_APP_VERSION_CODE, Integer.toString(BuildConfig.VERSION_CODE));
        putInternal(ANDROID_APP_DEPLOY, BuildConfig.DEPLOY);
        putInternal(API_VERSION, BuildConfig.IBOOKCARE_API_VERSION);

        String token = UserSession.with().getAccessToken();
        if (StringUtil.isEmpty(token) == false) {
            putInternal(AUTHORIZATION, token);
        }
    }

    //======================================================================
    // Public Methods
    //======================================================================

    public static boolean removeAuthorization(Map<String, String> map) {
        if (map != null && map.containsKey(AUTHORIZATION) == true) {
            map.remove(AUTHORIZATION);
            return true;
        }
        return false;
    }

    public HeaderParameter userId(String userId) {
        return putInternal(getMethodName(new Object() {
        }), userId);
    }

    public HeaderParameter password(String password) {
        return putInternal(getMethodName(new Object() {
        }), password);
    }

    public HeaderParameter childSeq(int childSeq) {
        return putInternal(getMethodName(new Object() {
        }), Integer.toString(childSeq));
    }

    public HeaderParameter userSeq(int userSeq) {
        return putInternal(getMethodName(new Object() {
        }), Integer.toString(userSeq));
    }

    @SuppressWarnings("PointlessBooleanExpression")
    public HeaderParameter selfYn(boolean self) {
        return putInternal(getMethodName(new Object() {
        }), self == true ? "Y" : "N");
    }

    public HeaderParameter registerYn(boolean registerYn) {
        return putInternal(getMethodName(new Object() {
        }), registerYn == true ? "Y" : "N");
    }

    public HeaderParameter moveYn(boolean moveYn) {
        return putInternal(getMethodName(new Object() {
        }), moveYn == true ? "Y" : "N");
    }

    public HeaderParameter email(String email) {
        return putInternal(getMethodName(new Object() {
        }), email);
    }

    public HeaderParameter page(int page) {
        return putInternal(getMethodName(new Object() {
        }), Integer.toString(page));
    }

    public HeaderParameter sortType(String sortType) {
        return putInternal(getMethodName(new Object() {
        }), sortType);
    }
}
