package com.ibook.barcode.network.request;

import com.google.gson.reflect.TypeToken;
import com.ibook.barcode.BuildConfig;
import com.ibook.barcode.R;
import com.ibook.barcode.app.Application;
import com.ibook.barcode.network.IBookUrl;
import com.ibook.barcode.network.NetworkLog;
import com.ibook.barcode.network.exception.ServerResponseErrorException;
import com.ibook.barcode.network.parameter.HeaderParameter;
import com.ibook.barcode.network.response.BaseResponse;
import com.ibook.barcode.network.response.ResponseParse;
import com.starnation.okhttp.Method;
import com.starnation.okhttp.OkHttp;
import com.starnation.okhttp.RequestBuilder;
import com.starnation.okhttp.ServerException;
import com.starnation.util.StringUtil;

import java.io.IOException;

import okhttp3.Request;

/*
 * @author lsh
 * @since 2017. 5. 17.
*/
public final class RequestFactory {

    //======================================================================
    // Public Search Methods
    //======================================================================

    public static <Response extends BaseResponse> RequestBuilder<Response> getSearch() {
        return getInternal(IBookUrl.SEARCH);
    }

    public static <Response extends BaseResponse> RequestBuilder<Response> postSearch() {
        return postInternal(IBookUrl.SEARCH);
    }

    //======================================================================
    // Public Book Methods
    //======================================================================

    public static <Response extends BaseResponse> RequestBuilder<Response> getBook() {
        return getInternal(IBookUrl.BOOK);
    }

    public static <Response extends BaseResponse> RequestBuilder<Response> postBook() {
        return postInternal(IBookUrl.BOOK);
    }

    //======================================================================
    // Public User Methods
    //======================================================================

    public static <Response extends BaseResponse> RequestBuilder<Response> getUser(boolean unauthorized) {
        return getInternal(IBookUrl.USER, unauthorized);
    }

    public static <Response extends BaseResponse> RequestBuilder<Response> getUser() {
        return getInternal(IBookUrl.USER);
    }

    public static <Response extends BaseResponse> RequestBuilder<Response> postUser(boolean unauthorized) {
        return postInternal(IBookUrl.USER, unauthorized);
    }

    public static <Response extends BaseResponse> RequestBuilder<Response> postUser() {
        return postInternal(IBookUrl.USER);
    }

    //======================================================================
    // Public Child Methods
    //======================================================================

    public static <Response extends BaseResponse> RequestBuilder<Response> getChild() {
        return getInternal(IBookUrl.CHILD);
    }

    public static <Response extends BaseResponse> RequestBuilder<Response> postChild() {
        return postInternal(IBookUrl.CHILD);
    }

    //======================================================================
    // Public Methods
    //======================================================================

    public static <Response extends BaseResponse> RequestBuilder<Response> get(IBookUrl iBookUrl) {
        return getInternal(iBookUrl);
    }

    public static <Response extends BaseResponse> RequestBuilder<Response> get(IBookUrl iBookUrl, boolean unauthorized) {
        return getInternal(iBookUrl, unauthorized);
    }

    public static <Response extends BaseResponse> RequestBuilder<Response> post(IBookUrl iBookUrl) {
        return postInternal(iBookUrl);
    }

    public static <Response extends BaseResponse> RequestBuilder<Response> post(IBookUrl iBookUrl, boolean unauthorized) {
        return postInternal(iBookUrl, unauthorized);
    }

    //======================================================================
    // Private Methods
    //======================================================================

    private static <Response extends BaseResponse> RequestBuilder<Response> getInternal(IBookUrl iBookUrl) {
        return with(Method.GET, iBookUrl);
    }

    private static <Response extends BaseResponse> RequestBuilder<Response> getInternal(IBookUrl iBookUrl, boolean unauthorized) {
        return with(Method.GET, iBookUrl, unauthorized);
    }

    private static <Response extends BaseResponse> RequestBuilder<Response> postInternal(IBookUrl iBookUrl) {
        return with(Method.POST, iBookUrl);
    }

    private static <Response extends BaseResponse> RequestBuilder<Response> postInternal(IBookUrl iBookUrl, boolean unauthorized) {
        return with(Method.POST, iBookUrl, unauthorized);
    }

    private static <Response extends BaseResponse> RequestBuilder<Response> with(Method method, IBookUrl iBookUrl) {
        return with(method, iBookUrl, false);
    }

    private static <Response extends BaseResponse> RequestBuilder<Response> with(Method method, IBookUrl iBookUrl, boolean unauthorized) {
        return OkHttp.<Response>with(method)
                .url(iBookUrl.getUrl())
                .parse(new ResponseParse<Response>(TypeToken.get(BaseResponse.class)))
                .header(new HeaderParameter())
                .behavior(new Behavior<Response>(unauthorized, iBookUrl));
    }

    //======================================================================
    // Behavior
    //======================================================================

    final static class Behavior<Response extends BaseResponse> implements com.starnation.okhttp.Behavior<Response> {

        private boolean mUnauthorized;

        private IBookUrl mIBookUrl;

        public Behavior(boolean unauthorized, IBookUrl IBookUrl) {
            mUnauthorized = unauthorized;
            mIBookUrl = IBookUrl;
        }

        @SuppressWarnings("unchecked")
        @Override
        public void onRequest(com.starnation.okhttp.RequestBuilder requestBuilder) {
            if (mUnauthorized == true) {
                HeaderParameter.removeAuthorization(requestBuilder.getHeader());
            }
            if (BuildConfig.DEBUG) {
                NetworkLog.request(mIBookUrl, requestBuilder.getSuffixUrl(), requestBuilder.getMethod(), requestBuilder.getHeader(), requestBuilder.getRequestBody());
            }
        }

        @Override
        public Response onSuccess(Response response) {
            return response;
        }

        @Override
        public IOException onError(Request request, IOException ioe) {
            if (BuildConfig.DEBUG) {
                return onErrorDebug(ioe);
            } else {
                return onErrorRelease(ioe);
            }
        }

        @Override
        public void onComplete(boolean response, long responseTime) {

        }

        IOException onErrorDebug(IOException ioe) {
            String message = ioe.getMessage();

            if (ioe instanceof ServerException) {
                if (StringUtil.isEmpty(message) == true) {
                    ((ServerResponseErrorException) ioe).setMessage(Application.getContext().getString(R.string.app_message_network_server_error));
                }
            } else {
                ioe = new ServerException(message);
            }
            return ioe;
        }

        IOException onErrorRelease(IOException ioe) {
            String message = ioe.getMessage();

            if (ioe instanceof ServerException) {
                if (StringUtil.isEmpty(message) == true) {
                    ((ServerResponseErrorException) ioe).setMessage(Application.getContext().getString(R.string.app_message_network_server_error));
                }
            } else {
                ioe = new ServerException(Application.getContext().getString(R.string.app_message_network_error));
            }
            return ioe;
        }
    }
}
