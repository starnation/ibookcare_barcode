package com.ibook.barcode.network.model.user;

import com.google.gson.annotations.SerializedName;
import com.ibook.barcode.network.model.Base;
import com.ibook.barcode.network.response.BaseResponse;
import com.starnation.generics.annotation.GenericsClass;
import com.starnation.util.StringUtil;

import org.parceler.Parcel;

import lombok.Data;
import lombok.EqualsAndHashCode;

/*
 * @author lsh
 * @since 2017. 5. 17.
*/
@Parcel
@Data
@EqualsAndHashCode(callSuper = false)
@GenericsClass(extendsClass = BaseResponse.class)
public class User extends Base {

    //======================================================================
    // Variables
    //======================================================================

    @SerializedName("userSeq")
    int mSeq;

    @SerializedName("password")
    String mPassword;

    @SerializedName("email")
    String mEmail;

    @SerializedName("name")
    String mName;

    @SerializedName("phoneName")
    String mPhoneName;

    @SerializedName("status")
    Status mStatus;

    @SerializedName("regDate")
    String mReqDate;

    @SerializedName("facebookId")
    String mFacebookId;

    @SerializedName("naverId")
    String mNaverId;

    @SerializedName("kakaoId")
    String mKakaoId;

    @SerializedName("googleId")
    String mGoogleId;

    @SerializedName("image")
    String mImage;

    @SerializedName("thumbnailImage")
    String mThumbnailImage;

    @SerializedName("si")
    String mSi;

    @SerializedName("gungu")
    String mGungu;

    @SerializedName("birthday")
    String mBirthday;

    @SerializedName("gender")
    Gender mGender;

    @SerializedName("childCount")
    int mChildCount;

    @SerializedName("phone")
    String mPhone;

    @SerializedName("phoneOpenYn")
    String mPhoneOpen;

    @SerializedName("role")
    String mRole;

    @SerializedName("dong")
    String mDong;

    String mCacheDecodePassword;

    boolean mEmpty;

    //======================================================================
    // Constructor
    //======================================================================

    public static User createEmptyUser() {
        User user = new User();
        user.mEmpty = true;
        return user;
    }

    @Deprecated
    public User() {
        // Nothing
    }

    //======================================================================
    // Override Methods
    //======================================================================

    @Override
    public String toString() {
        return "seq : " + mSeq +
                " name : " + mName +
                " gender : " + mGender +
                " birthDay : " + mBirthday;
    }


    //======================================================================
    // Public Methods
    //======================================================================

    public final boolean isRoleGuest() {
        if (StringUtil.isEmpty(mRole) == false) {
            if (mRole.toLowerCase().contains("guest") == true) {
                return true;
            }
        }
        return false;
    }

    public boolean isKakaoConnected() {
        return StringUtil.isEmpty(mKakaoId) == false;
    }

    public boolean isFacebookConnected() {
        return StringUtil.isEmpty(mFacebookId) == false;
    }

    public boolean isNaverConnected() {
        return StringUtil.isEmpty(mNaverId) == false;
    }

    public boolean isGooglePlusConnected() {
        return StringUtil.isEmpty(mGoogleId) == false;
    }

    public String getRegDate() {
        return getDate(mReqDate);
    }

    public String toRegDate() {
        return getRegDate() + " 가입";
    }

    public String getChoiceImage() {
        if (StringUtil.isEmpty(mThumbnailImage) == false) {
            return getUrl(mThumbnailImage);
        }
        return getUrl(mImage);
    }

    public String getImage() {
        return getUrl(mImage);
    }

    public String toNameContacts() {
        if (StringUtil.isEmpty(mPhoneName) == false) {
            return mPhoneName;
        }
        return mName;
    }

    //======================================================================
    // Status
    //======================================================================

    public enum Status {
        /**
         * 미인증
         */
        UNCONFIRM,
        /**
         * 인증
         */
        CONFIRM,
        /**
         * 탈퇴
         */
        LEAVE
    }
}
