package com.ibook.barcode.network.request;

import com.google.gson.reflect.TypeToken;
import com.ibook.barcode.network.response.BaseResponse;
import com.ibook.barcode.network.response.ResponseParse;
import com.starnation.generics.util.TypeUtil;

import java.util.ArrayList;

/*
 * @author lsh
 * @since 2017. 5. 17.
*/
final class ParseFactory {

    //======================================================================
    // Public Methods
    //======================================================================

    public static ResponseParse createStringParse() {
        return new ResponseParse<>(getTypeToken(BaseResponse.class, BaseResponse.class, String.class));
    }

    public static ResponseParse createModelParse(Class<?> modelClass) {
        return new ResponseParse<>(getTypeToken(modelClass, BaseResponse.class, modelClass));
    }

    public static ResponseParse createArrayListParse(Class<?> modelClass) {
        return new ResponseParse<>(getTypeToken(modelClass, BaseResponse.class, ArrayList.class, modelClass));
    }

    //======================================================================
    // Private Methods
    //======================================================================

    private static TypeToken getTypeToken(Class target, Class... aClass) {
        return TypeToken.get(TypeUtil.getInnerClassExtendsType(target, aClass));
    }
}
