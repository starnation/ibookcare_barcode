package com.ibook.barcode.network.model.book;

import com.google.gson.annotations.SerializedName;
import com.ibook.barcode.network.model.Base;

import org.parceler.Parcel;

import lombok.Data;
import lombok.EqualsAndHashCode;

/*
 * @author lsh
 * @since 2017. 5. 17.
*/
@Parcel
@Data
@EqualsAndHashCode(callSuper = false)
public class BookContent extends Base {

    //======================================================================
    // Variables
    //======================================================================

    @SerializedName("title")
    String mTitle;

    @SerializedName("content1")
    String mContent1;

    @SerializedName("content2")
    String mContent2;

    @SerializedName("content3")
    String mContent3;
}
