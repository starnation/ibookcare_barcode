package com.ibook.barcode.network.model.book;

import android.widget.Checkable;

import com.google.gson.annotations.SerializedName;
import com.ibook.barcode.network.model.Base;
import com.starnation.util.StringUtil;

import org.parceler.Parcel;

import lombok.AccessLevel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;

/*
 * @author lsh
 * @since 2017. 5. 17.
*/
@Parcel
@Data
@EqualsAndHashCode(callSuper = false)
public final class BookInfo extends Base implements Checkable {

    //======================================================================
    // Variables
    //======================================================================

    @SerializedName("sellBookSeq")
    int mSellSeq;

    @Getter(AccessLevel.NONE)
    @SerializedName("likeYn")
    String mLikeYn;

    @Getter(AccessLevel.NONE)
    @SerializedName("readYn")
    String mReadYn;

    @SerializedName("rentYn")
    String mRentYn;

    @Getter(AccessLevel.NONE)
    @SerializedName("interestYn")
    String mInterestYn;

    @Getter(AccessLevel.NONE)
    @SerializedName("collectionYn")
    String mCollectionYn;

    @SerializedName("bookCode")
    String mBookCode;

    @SerializedName("collectionBookCode")
    String mCollectionBookCode;

    @SerializedName("isbn10")
    String mIsbn10;

    @SerializedName("isbn13")
    String mIsbn13;

    @SerializedName("title")
    String mTitle;

    @SerializedName("author")
    String mAuthor;

    @SerializedName("description")
    String mDescription;

    @SerializedName("publisher")
    String mPublisher;

    @SerializedName("publisherCode")
    String mPublisherCode;

    @SerializedName("pubDate")
    String mPubDate;

    @SerializedName("foreignLink")
    String mForeignLink;

    @SerializedName("previewLink")
    String mPreviewLink;

    @SerializedName("cid")
    String mCid;

    @SerializedName("coverNlarge")
    String mCoverNlarge;

    @SerializedName("coverXlarge")
    String mCoverXlarge;

    @SerializedName("coverAladin")
    String mCoverAladin;

    @SerializedName("width")
    int mWidth;

    @SerializedName("height")
    int mHeight;

    @Getter(AccessLevel.NONE)
    @SerializedName("userRating")
    String mUserRating;

    @Getter(AccessLevel.NONE)
    @SerializedName("childLibraryRegister")
    String mChildLibraryRegister;

    @SerializedName("childRegisterCount")
    int mChildRegisterCount;

    @SerializedName("userRegisterCount")
    int mUserRegisterCount;

    @SerializedName("commentCount")
    int mCommentCount;

    @SerializedName("bookScore")
    float mBookScore;

    @SerializedName("todayType")
    String mTodayType;

    @SerializedName("evaluationState")
    BookEvaluationState mBookEvaluationState;

    float mAsUserRating;

    boolean mChecked;

    //======================================================================
    // Public Methods
    //======================================================================

    public void setDescription(String description) {
        if (StringUtil.isEmpty(description) == false) {
            mDescription = description;
        }
    }

    public String getPublisherOrDate() {
        return mPublisher + " | " + getDate(mPubDate);
    }

    public String getPubDate() {
        return getDate(mPubDate);
    }

    public String getCoverNlarge() {
        return getUrl(mCoverNlarge);
    }

    public String getCoverXlarge() {
        return getUrl(mCoverXlarge);
    }

    public String getCoverAladin() {
        return getUrl(mCoverAladin);
    }

    public float getRatio() {
        float width = mWidth;
        float height = mHeight;
        if (width > 0 && height > 0) {
            return width / height;
        }
        return 0;
    }

    @Override
    public void setChecked(boolean checked) {
        mChecked = checked;
    }

    @Override
    public boolean isChecked() {
        return mChecked;
    }

    @Override
    public void toggle() {
        setChecked(!mChecked);
    }

    public int getSellSeq() {
        return mSellSeq;
    }

    public boolean isReadingBook() {
        return getBoolean(mReadYn);
    }

    public void setReadYn(boolean readYn) {
        mReadYn = toStringOf(readYn, "Y", "N");
    }

    public boolean isLike() {
        return getBoolean(mLikeYn);
    }

    public void setLikeYn(boolean likeYn) {
        mLikeYn = toStringOf(likeYn, "Y", "N");
    }

    public boolean isInterest() {
        return getBoolean(mInterestYn);
    }

    public void setInterest(boolean interestYn) {
        mInterestYn = toStringOf(interestYn, "Y", "N");
    }

    public boolean isCollection() {
        return getBoolean(mCollectionYn);
    }

    public float getUserRating() {
        if (StringUtil.isEmpty(mUserRating) == false) {
            mAsUserRating = Float.parseFloat(mUserRating);
        }
        return mAsUserRating;
    }

    public boolean isChildLibraryRegister() {
        return getBoolean(mChildLibraryRegister);
    }

    public void setChildLibraryRegister(boolean childLibraryRegister) {
        mChildLibraryRegister = toStringOf(childLibraryRegister, "Y", "N");
    }

    /**
     * 대표 전집 유무 판단.
     *
     * @return 대표 전집인경우 true 아닌경우 false
     */
    public boolean isMainCollection() {
        return StringUtil.isEmpty(mCollectionBookCode) == true;
    }
}
