package com.ibook.barcode.network.model.user;

import com.google.gson.annotations.SerializedName;
import com.ibook.barcode.network.model.user.child.Child;
import com.ibook.barcode.network.response.BaseResponse;
import com.starnation.generics.annotation.GenericsClass;

import org.parceler.Parcel;

import java.util.ArrayList;

import lombok.Data;

/*
 * @author lsh
 * @since 2017. 5. 17.
*/
@Parcel
@Data
@GenericsClass(extendsClass = BaseResponse.class)
public final class LoginResult {

    //======================================================================
    // Variables
    //======================================================================

    @SerializedName("accessToken")
    String mAccessToken;

    @SerializedName("user")
    User mUser;

    @SerializedName("child")
    ArrayList<Child> mChildList;
}
