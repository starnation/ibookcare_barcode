package com.ibook.barcode.network.parameter;

/*
 * @author lsh
 * @since 2017. 5. 17.
*/
public enum ValueState {
    EMPTY_OR_NULL_MAP_REMOVE,
    EMPTY_OR_NULL_MAP_NULL,
    DEFAULT,
}
