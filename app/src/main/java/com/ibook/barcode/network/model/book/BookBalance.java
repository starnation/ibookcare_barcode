package com.ibook.barcode.network.model.book;

import android.support.annotation.ColorInt;
import android.widget.Checkable;

import com.google.gson.annotations.SerializedName;
import com.ibook.barcode.network.model.Base;

import org.parceler.Parcel;

import lombok.AccessLevel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;

/*
 * @author lsh
 * @since 2017. 5. 17.
*/
@Parcel
@Data
@EqualsAndHashCode(callSuper = false)
public final class BookBalance extends Base implements Checkable {

    //======================================================================
    // Variables
    //======================================================================

    @SerializedName("categoryName")
    String mCategoryName;

    @SerializedName("categoryCode")
    String mCategoryCode;

    @Getter(AccessLevel.NONE)
    @SerializedName("categoryColor")
    String mCategoryColor;

    @SerializedName("cur")
    float mCur;

    @SerializedName("curRatio")
    float mCurRatio;

    @Getter(AccessLevel.NONE)
    @SerializedName("categoryFlag")
    String mCategoryFlag;

    @SerializedName("content")
    String mContent;

    boolean mChecked;

    //======================================================================
    // Override Methods
    //======================================================================

    @Override
    public void toggle() {
        setChecked(!mChecked);
    }

    //======================================================================
    // Public Methods
    //======================================================================

    @ColorInt
    public int getCategoryColor() {
        return getColor(mCategoryColor);
    }

    public float getCorrectionCur() {
        return mCur;
    }

    public boolean isCategoryFlag() {
        return getBoolean(mCategoryFlag);
    }
}
