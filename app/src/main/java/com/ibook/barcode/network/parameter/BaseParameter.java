package com.ibook.barcode.network.parameter;

import android.support.annotation.NonNull;

import com.google.gson.JsonObject;
import com.starnation.util.StringUtil;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/*
 * @author lsh
 * @since 2017. 5. 17.
*/
abstract class BaseParameter<Parameter extends BaseParameter> implements Map<String, String>{

    //======================================================================
    // Constance
    //======================================================================

    private final static String GET = "get";

    private final static String IS = "is";

    private final static String AS = "as";

    //======================================================================
    // Variables
    //======================================================================

    ValueState mValueState = ValueState.DEFAULT;

    HashMap<String, String> mMap = new HashMap<>();

    //======================================================================
    // Constructor
    //======================================================================

    @SuppressWarnings("WeakerAccess")
    public BaseParameter(ValueState valueState) {
        mValueState = valueState;
    }

    //======================================================================
    // Override Methods
    //======================================================================

    @Override
    public int size() {
        return mMap.size();
    }

    @Override
    public boolean isEmpty() {
        return mMap.isEmpty();
    }

    @Override
    public boolean containsKey(Object key) {
        return mMap.containsKey(key);
    }

    @Override
    public boolean containsValue(Object value) {
        return mMap.containsValue(value);
    }

    @Override
    public String get(Object key) {
        return mMap.get(key);
    }

    @Override
    public String put(String key, String value) {
        return mMap.put(key, value);
    }

    @Override
    public String remove(Object key) {
        return mMap.remove(key);
    }

    @Override
    public void putAll(Map<? extends String, ? extends String> m) {
        mMap.putAll(m);
    }

    @Override
    public void clear() {
        mMap.clear();
    }

    @NonNull
    @Override
    public Set<String> keySet() {
        return mMap.keySet();
    }

    @NonNull
    @Override
    public Collection<String> values() {
        return mMap.values();
    }

    @NonNull
    @Override
    public Set<Entry<String, String>> entrySet() {
        return mMap.entrySet();
    }

    //======================================================================
    // Methods
    //======================================================================

    @SuppressWarnings({"unchecked", "PointlessBooleanExpression"})
    String getMethodName(Object object) {
        String name = "";
        try {
            name = object.getClass().getEnclosingMethod().getName();
        } catch (Exception e) {
            StackTraceElement[] stacktrace = Thread.currentThread().getStackTrace();
            for (int i = 0; i < stacktrace.length; i++) {
                StackTraceElement stackTraceElement = stacktrace[i];
                if (stackTraceElement.getMethodName().equals("getMethodName") == true) {
                    name = stacktrace[i+1].getMethodName();
                    break;
                }
            }
        }

        if (name.startsWith(GET) == true) {
            try {
                String sub = name.substring(GET.length(), GET.length() + 1);
                return name.replaceAll("get[A-Z]", sub.toLowerCase());
            } catch (Exception ignored) {
                //Nothing
            }
        } else if (name.startsWith(IS) == true) {
            try {
                String sub = name.substring(IS.length(), IS.length() + 1);
                return name.replaceAll("is[A-Z]", sub.toLowerCase());
            } catch (Exception ignored) {
                //Nothing
            }
        } else if (name.startsWith(AS) == true) {
            try {
                String sub = name.substring(AS.length(), AS.length() + 1);
                return name.replaceAll("as[A-Z]", sub.toLowerCase());
            } catch (Exception ignored) {
                //Nothing
            }
        }
        return name;
    }

    @SuppressWarnings({"PointlessBooleanExpression", "unused"})
    boolean isNullValue(String key) {
        if (containsKey(key) == true) {
            String value = get(key);
            return StringUtil.isEmpty(value);
        }
        return true;
    }

    @SuppressWarnings({"unchecked", "PointlessBooleanExpression"})
    final Parameter putInternal(String key, String value) {
        try {
            URLEncoder.encode(StringUtil.textTrim(value), "utf-8");

            switch(mValueState) {
                case EMPTY_OR_NULL_MAP_REMOVE:
                    if (StringUtil.isEmpty(value) == false) {
                        put(key, URLEncoder.encode(StringUtil.textTrim(value), "utf-8"));
                    }
                    break;
                case EMPTY_OR_NULL_MAP_NULL:
                    String replaceValue = StringUtil.isEmpty(value) == true ? null : URLEncoder.encode(StringUtil.textTrim(value), "utf-8");
                    put(key, replaceValue);
                    break;
                case DEFAULT:
                    put(key, URLEncoder.encode(StringUtil.textTrim(value), "utf-8"));
                    break;
            }
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return (Parameter) this;
    }

    public final JsonObject asJSONObject() {
        JsonObject jsonObject = new JsonObject();
        try {
            for (Entry<String, String> entry : entrySet()) {
                jsonObject.addProperty(entry.getKey(), entry.getValue());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return jsonObject;
    }
}
