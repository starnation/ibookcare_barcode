package com.ibook.barcode.network.parameter;

import com.google.gson.JsonArray;
import com.ibook.barcode.BuildConfig;
import com.ibook.barcode.util.log.AppLogger;
import com.ibook.barcode.util.log.Tag;
import com.starnation.okhttp.RequestBodyBuilderFactory;
import com.starnation.util.validator.CollectionValidator;

import java.util.ArrayList;

import okhttp3.RequestBody;

/*
 * @author lsh
 * @since 2017. 5. 17.
*/
public final class RequestBodyFactory {

    public static RequestBody createTargetList(ArrayList<BodyParameter> builders) {
        try {
            JsonArray jsonArray = new JsonArray();
            if (CollectionValidator.isValid(builders) == true) {
                for (BodyParameter builder : builders) {
                    jsonArray.add(builder.asJSONObject());
                }
            }
            if (BuildConfig.DEBUG) {
                AppLogger.w(Tag.NETWORK_REQUEST, "body info : " + jsonArray.toString());
            }
            return RequestBodyBuilderFactory.createFormBody(ParameterKey.TARGET_LIST, jsonArray.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
