package com.ibook.barcode.network.request;


/*
 * @author lsh
 * @since 2017. 5. 17.
*/
@SuppressWarnings("ALL")
public final class SuffixFactory {

    public static String getISBN(String isbn) {
        return "isbn/" + isbn;
    }

    public static String getMyBook(int seq) {
        return Integer.toString(seq) + "/myBook";
    }

    public static String getLogin() {
        return "login";
    }

    public static String getCollectionCode(String code) {
        return "collection/" + code;
    }
}
