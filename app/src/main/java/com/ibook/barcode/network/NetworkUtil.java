package com.ibook.barcode.network;

import com.starnation.util.StringUtil;

import java.io.File;
import java.util.Locale;

/*
 * @author lsh
 * @since 2017. 5. 16.
*/
public final class NetworkUtil {

    //======================================================================
    // Constants
    //======================================================================

    private final static String CANCEL_KEY_FORMAT = "%d_%d";

    //======================================================================
    // Public Methods
    //======================================================================

    public static int getCancelKey(String url, int key) {
        return String.format(Locale.US, CANCEL_KEY_FORMAT, url.hashCode(), key).hashCode();
    }

    public static String joinUrl(String leftUrl, String rightUrl) {
        if (StringUtil.isEmpty(rightUrl) == false) {
            String suffix = rightUrl;
            if (rightUrl.startsWith(File.separator) == false
                    && leftUrl.endsWith(File.separator) == false) {
                suffix = File.separator + rightUrl;
            }
            return leftUrl + suffix;
        }
        return leftUrl;
    }

    public static boolean isJsonObject(String json) {
        return StringUtil.isEmpty(json) == false && json.startsWith("{") == true && json.endsWith("}") == true;
    }

    public static boolean isJsonArray(String json) {
        return StringUtil.isEmpty(json) == false && json.startsWith("[") == true && json.endsWith("]") == true;
    }
}
