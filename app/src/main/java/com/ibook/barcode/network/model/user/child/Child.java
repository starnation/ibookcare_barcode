package com.ibook.barcode.network.model.user.child;

import com.google.gson.annotations.SerializedName;
import com.ibook.barcode.network.model.Base;
import com.ibook.barcode.network.model.user.Gender;
import com.ibook.barcode.network.response.BaseResponse;
import com.starnation.generics.annotation.GenericsClass;
import com.starnation.util.StringUtil;

import org.parceler.Parcel;

import java.util.ArrayList;

import lombok.AccessLevel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

/*
 * @author lsh
 * @since 2017. 5. 17.
*/
@Parcel
@Data
@EqualsAndHashCode(callSuper = false)
@GenericsClass(extendsClass = BaseResponse.class, type = {ArrayList.class})
public final class Child extends Base {

    //======================================================================
    // Variables
    //======================================================================

    @SerializedName("childSeq")
    int mChildSeq;

    @SerializedName("userSeq")
    int mUserSeq;

    @SerializedName("name")
    String mName;

    @SerializedName("gender")
    Gender mGender;

    @SerializedName("birthday")
    String mBirthday;

    @SerializedName("image")
    String mImage;

    @SerializedName("thumbnailImage")
    String mThumbnailImage;

    @Getter(AccessLevel.NONE)
    @SerializedName("tendencyYn")
    String mTendencyYn;

    @SerializedName("regDate")
    String mRegDate;

    @SerializedName("bookCount")
    int mBookCount;

    @SerializedName("readYCount")
    int mReadYCount;

    @SerializedName("readNCount")
    int mReadNCount;

    @Deprecated
    @Getter(AccessLevel.NONE)
    @SerializedName("analysisYn")
    String mAnalysisYn;

    @Deprecated
    @SerializedName("analysisMsg")
    String mAnalysisMsg;

    @Setter(AccessLevel.NONE)
    @Getter(AccessLevel.NONE)
    @SerializedName("bookshelfOpenYn")
    String mOpenYn;

    @Setter(AccessLevel.NONE)
    @Getter(AccessLevel.NONE)
    @SerializedName("reviewYn")
    String mReviewYn;

    //======================================================================
    // Public Methods
    //======================================================================

    public String getImage() {
        return getUrl(mImage);
    }

    public String getChoiceImage() {
        String url = getUrl(mThumbnailImage);
        if (StringUtil.isEmpty(url) == true) {
            return getUrl(mImage);
        }
        return url;
    }

    @Deprecated
    public String getThumbnailImage() {
        return getUrl(mThumbnailImage);
    }

    public boolean isTendencyYn() {
        return StringUtil.equals(mTendencyYn, "Y");
    }

    @Deprecated
    public boolean isAnalysisYn() {
        return getBoolean(mAnalysisYn);
    }

    public boolean isOpenYn() {
        return StringUtil.equals(mOpenYn, "Y");
    }

    public void setOpenYn(boolean isyn) {
        mOpenYn = isyn ? "Y" : "N";
    }

    public boolean isReviewYn() {
        return getBoolean(mReviewYn);
    }

    public void setReviewYn(boolean isyn) {
        mReviewYn = toStringOf(isyn, "Y", "N");
    }
}
