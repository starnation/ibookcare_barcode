package com.ibook.barcode.network.parameter;

/*
 * @author lsh
 * @since 2017. 5. 17.
*/
public final class ParameterKey {

    //======================================================================
    // Constants
    //======================================================================

    public final static String IMAGE = "image";

    public final static String IMAGE_IMAGE = "selfImage";

    public final static String NAME = "name";

    public final static String FILE = "file";

    public final static String VERSION = "version";

    public final static String ANDROID_APP_VERSION_NAME = "androidAppVersionName";

    public final static String ANDROID_APP_VERSION_CODE = "androidAppVersionCode";

    public final static String ANDROID_APP_DEPLOY = "androidAppDeploy";

    public final static String API_VERSION = "apiVersion";

    public final static String AUTHORIZATION = "Authorization";

    final static String TARGET_LIST = "targetList";

    public final static String PHONE_LIST = "phoneList";
}
