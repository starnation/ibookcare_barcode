package com.ibook.barcode.network.model.user;

/*
 * @author lsh
 * @since 2017. 5. 17.
*/
public enum Gender {
    /**
     * 남자
     */
    M,
    /**
     * 여자
     */
    F,
    DEFAULT
}
