package com.ibook.barcode.network.model.book;

import com.google.gson.annotations.SerializedName;
import com.ibook.barcode.network.model.Base;

import org.parceler.Parcel;

import lombok.Data;
import lombok.EqualsAndHashCode;

/*
 * @author lsh
 * @since 2017. 5. 17.
*/
@Parcel
@Data
@EqualsAndHashCode(callSuper=false)
public class BookCollection extends Base {

    //======================================================================
    // Variables
    //======================================================================

    @SerializedName("setCode")
    String mSetCode;

    @SerializedName("name")
    String mName;

    @SerializedName("genreName")
    String mGenreName;

    @SerializedName("typeName")
    String mTypeName;

    @SerializedName("publisher")
    String mPublisher;

    @SerializedName("pubDate")
    String mPubDate;

    @SerializedName("content")
    String mContent;

    @SerializedName("image")
    String mImage;

    //======================================================================
    // Public Methods
    //======================================================================

    public String getPubDate() {
        return getDate(mPubDate);
    }
}
