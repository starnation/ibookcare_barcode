package com.ibook.barcode.network.response;

import com.google.gson.Gson;
import com.ibook.barcode.BuildConfig;
import com.ibook.barcode.network.NetworkLog;
import com.ibook.barcode.network.exception.ServerResponseErrorException;
import com.ibook.barcode.util.log.AppLogger;
import com.ibook.barcode.util.log.Tag;
import com.starnation.okhttp.JsonRequest;

import java.io.IOException;
import java.io.StringReader;

import okhttp3.Response;

/*
 * @author lsh
 * @since 2017. 5. 17.
*/
public final class ResponseParse<R extends BaseResponse> implements JsonRequest.Parse<R> {

    com.google.gson.reflect.TypeToken mType;

    public ResponseParse(com.google.gson.reflect.TypeToken typeToken) {
        mType = typeToken;
    }

    @SuppressWarnings("unchecked")
    @Override
    public R parse(Response response, String json) throws IOException {
        R r;
        try {
            StringReader reader = new StringReader(json);
            if (mType != null) {
                r = new Gson().fromJson(reader, mType.getType());
            } else {
                r = (R) new Gson().fromJson(reader, BaseResponse.class);
            }

            reader.close();

            try {
                if (BuildConfig.DEBUG) {
                    NetworkLog.response(response, r, json);
                }
            } catch (Exception e) {
                if (BuildConfig.DEBUG) {
                    AppLogger.e(Tag.NETWORK_RESPONSE, e.getMessage() + " class : ");
                }
            }
            mType = null;
        } catch (Exception e) {
            if (BuildConfig.DEBUG) {
                e.printStackTrace();
            }
            throw new IOException(e.getMessage());
        }

        if (r.isSuccess() == false) {
            ServerResponseErrorException responseErrorException = new ServerResponseErrorException(r.mMessage);
            responseErrorException.setStatus(r.mStatus);
            responseErrorException.setHttpStatusCode(response.code());
            responseErrorException.setResponse(r);
            throw responseErrorException;
        }
        return r;
    }
}
