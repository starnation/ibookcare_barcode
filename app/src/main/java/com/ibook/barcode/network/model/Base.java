package com.ibook.barcode.network.model;

import android.graphics.Color;
import android.support.annotation.ColorInt;

import com.ibook.barcode.network.IBookUrl;
import com.starnation.util.StringUtil;

/*
 * @author lsh
 * @since 2017. 5. 17.
*/
public class Base {

    //======================================================================
    // Constants
    //======================================================================

    private final static String HTTP = "http";

    //======================================================================
    // Public Methods
    //======================================================================

    @SuppressWarnings({"WeakerAccess", "PointlessBooleanExpression"})
    public static String getIbookUrl(String url) {
        if (StringUtil.isEmpty(url) == true) {
            return "";
        }
        if (url.startsWith(HTTP) == true) {
            return url;
        }
        return IBookUrl.getDomain() + url;
    }

    public final String getUrl(String url) {
        return getIbookUrl(url);
    }

    @SuppressWarnings({"PointlessBooleanExpression", "WeakerAccess"})
    public final String getDate(String date) {
        if (StringUtil.isEmpty(date) == true) {
            return "";
        }

        String[] split = date.split(" ");
        if (split.length == 0) {
            return "";
        }
        return split[0];
    }

    public final boolean getBoolean(String src) {
        return StringUtil.equals(src, "Y");
    }

    public final boolean getBoolean(String src, String trueValue) {
        return StringUtil.equals(src, trueValue);
    }

    @SuppressWarnings("WeakerAccess")
    public final String toStringOf(boolean src, String trueValue, String falseValue) {
        try {
            return src ? trueValue : falseValue;
        } catch (Exception e) {
            return falseValue;
        }
    }

    @SuppressWarnings("PointlessBooleanExpression")
    @ColorInt
    public final int getColor(String src) {
        if (StringUtil.isEmpty(src) == true) {
            return Color.BLACK;
        }
        try {
            return Color.parseColor(src);
        } catch (Exception e) {
            return Color.BLACK;
        }
    }
}
