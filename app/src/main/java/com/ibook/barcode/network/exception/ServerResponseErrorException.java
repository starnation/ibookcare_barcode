package com.ibook.barcode.network.exception;

import android.support.annotation.Nullable;

import com.ibook.barcode.network.response.ErrorStatus;
import com.starnation.okhttp.ServerException;

import java.io.IOException;

/*
 * @author lsh
 * @since 2017. 5. 7.
*/
public class ServerResponseErrorException extends ServerException {

    //======================================================================
    // Variables
    //======================================================================

    String mStatus;

    Object mResponse;

    //======================================================================
    // Constructor
    //======================================================================

    public ServerResponseErrorException(String detailMessage) {
        super(detailMessage);
    }

    //======================================================================
    // Public Methods
    //======================================================================

    public static ErrorStatus getErrorStatus(IOException e) {
        if (e instanceof ServerResponseErrorException) {
            return ((ServerResponseErrorException) e).toErrorStatus();
        }
        return null;
    }

    public static String getStatus(IOException e) {
        if (e instanceof ServerResponseErrorException) {
            return ((ServerResponseErrorException) e).getStatus();
        }
        return "";
    }

    public String getStatus() {
        return mStatus;
    }

    public void setStatus(String status) {
        mStatus = status;
    }

    public Object getResponse() {
        return mResponse;
    }

    public void setResponse(Object response) {
        mResponse = response;
    }

    @Nullable
    public ErrorStatus toErrorStatus() {
        try {
            return ErrorStatus.valueOf(mStatus);
        } catch (Exception e) {
            // Nothing
        }
        return null;
    }
}
