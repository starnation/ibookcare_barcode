package com.ibook.barcode.network.model.book;

import com.google.gson.annotations.SerializedName;
import com.ibook.barcode.network.model.Base;

import org.parceler.Parcel;

import lombok.Data;
import lombok.EqualsAndHashCode;

/*
 * @author lsh
 * @since 2016. 5. 17.
*/
@Parcel
@Data
@EqualsAndHashCode(callSuper=false)
public class IsbnInfo extends Base {

    @SerializedName("content")
    String mContent;

    @SerializedName("content2")
    String mContent2;

    @SerializedName("bookCount")
    int mBookCount;
}
