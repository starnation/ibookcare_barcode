package com.ibook.barcode.network.model.user;

import android.widget.Checkable;

import com.google.gson.annotations.SerializedName;
import com.ibook.barcode.network.model.Base;
import com.starnation.util.StringUtil;

import org.parceler.Parcel;

import lombok.Data;
import lombok.EqualsAndHashCode;

/*
 * @author lsh
 * @since 2017. 5. 17.
*/
@Parcel
@Data
@EqualsAndHashCode(callSuper = false)
public final class UserCollection extends Base implements Checkable {

    //======================================================================
    // Variables
    //======================================================================

    @SerializedName("collectionInfoSeq")
    int mCollectionInfoSeq;

    @SerializedName("userSeq")
    int mUserSeq;

    @SerializedName("childSeq")
    int mChildSeq;

    @SerializedName("title")
    String mTitle;

    @SerializedName("image")
    String mImage;

    @SerializedName("thumbnailImage")
    String mThumbnailImage;

    @SerializedName("regDate")
    String mRegDate;

    @SerializedName("bookCount")
    int mBookCount;

    @SerializedName("registerYn")
    String mRegisterYn;

    boolean mChecked;

    boolean mFirstSetup;

    State mState = State.USER_COLLECTION;

    //======================================================================
    // Constructor
    //======================================================================

    public UserCollection() {
        // Nothing
    }

    //======================================================================
    // Override Methods
    //======================================================================

    @Override
    public void setChecked(boolean checked) {
        mChecked = checked;
    }

    @Override
    public boolean isChecked() {
        if (mFirstSetup == false) {
            mChecked = isRegisterYn();
            mFirstSetup = true;
        }
        return mChecked;
    }

    @Override
    public void toggle() {
        setChecked(!mChecked);
    }

    //======================================================================
    // Public Methods
    //======================================================================

    @Deprecated
    public String getImage() {
        return getUrl(mImage);
    }

    @Deprecated
    public String getThumbnailImage() {
        return getUrl(mThumbnailImage);
    }

    public String getChoiceImage() {
        String url = getUrl(mThumbnailImage);
        if (StringUtil.isEmpty(url) == true) {
            return getUrl(mImage);
        }
        return url;
    }

    public boolean isRegisterYn() {
        return getBoolean(mRegisterYn);
    }

    public void setRegisterYn(boolean registerYn) {
        mRegisterYn = toStringOf(registerYn, "Y", "N");
    }

    public boolean isDefaultLibrary() {
        return mState == State.DEFAULT_LIBRARY;
    }

    //======================================================================
    // State
    //======================================================================

    public enum State {
        /**
         * 기본책장
         */
        DEFAULT_LIBRARY,
        /**
         * 전집
         */
        COLLECTION,
        /**
         * 책폴더
         */
        USER_COLLECTION
    }
}
