package com.ibook.barcode.network.parameter;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.ibook.barcode.network.model.Base;
import com.ibook.barcode.network.model.user.child.Child;
import com.starnation.util.StringUtil;
import com.starnation.util.validator.CollectionValidator;

import org.parceler.Parcel;

import java.util.ArrayList;

import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.functions.Func1;

/*
 * @author lsh
 * @since 2017. 5. 17.
*/
@Parcel
public final class BodyParameter extends BaseParameter<BodyParameter> {

    //======================================================================
    // Constructor
    //======================================================================

    public BodyParameter() {
        super(ValueState.EMPTY_OR_NULL_MAP_NULL);
    }

    public BodyParameter(ValueState valueState) {
        super(valueState);
    }

    //======================================================================
    // Public Methods
    //======================================================================

    public static <Item extends Base> ArrayList<BodyParameter> asBodyParameterList(ArrayList<Item> ddd, Func1<Item, BodyParameter> merge) {
        final ArrayList<BodyParameter> list = new ArrayList<>();
        Subscription subscribe = Observable.from(ddd)
                .map(merge)
                .subscribe(new Subscriber<BodyParameter>() {
                    @Override
                    public void onCompleted() {
                        // Nothing
                    }

                    @Override
                    public void onError(Throwable e) {
                        // Nothing
                    }

                    @Override
                    public void onNext(BodyParameter builder) {
                        if (builder != null) {
                            list.add(builder);
                        }
                    }
                });
        if (subscribe != null) {
            subscribe.unsubscribe();
        }
        return list;
    }

    public String getBookCode() {
        return get(getMethodName(new Object() {
        }));
    }

    public BodyParameter bookCode(String bookCode) {
        return putInternal(getMethodName(new Object() {
        }), bookCode);
    }

    public BodyParameter setCode(String setCode) {
        return putInternal(getMethodName(new Object() {
        }), setCode);
    }

    @SuppressWarnings({"PointlessBooleanExpression", "unused"})
    public JsonArray asChildSeqList() {
        String value = get(getMethodName(new Object() {
        }));

        if (StringUtil.isEmpty(value) == false) {
            return new JsonParser().parse(value).getAsJsonObject().getAsJsonArray(getMethodName(new Object() {
            }));
        }
        return null;
    }

    @SuppressWarnings("unused")
    public BodyParameter childSeqList(ArrayList<Child> list) {
        JsonArray array = new JsonArray();
        for (Child child : list) {
            JsonObject object = new JsonObject();
            object.addProperty("childSeq", child.getChildSeq());
            array.add(object);
        }
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty(getMethodName(new Object() {
        }), array.toString());
        return putInternal(getMethodName(new Object() {
        }), jsonObject.toString());
    }

    @SuppressWarnings("unused")
    public BodyParameter selType(String type) {
        return putInternal(getMethodName(new Object() {
        }), type);
    }

    @SuppressWarnings("unused")
    public BodyParameter fullPrice(long fullPrice) {
        return putInternal(getMethodName(new Object() {
        }), Long.toString(fullPrice));
    }

    public BodyParameter content(String content) {
        return putInternal(getMethodName(new Object() {
        }), content);
    }

    public BodyParameter title(String title) {
        return putInternal(getMethodName(new Object() {
        }), title);
    }

    @SuppressWarnings("unused")
    public BodyParameter sellBookSeq(int seq) {
        return putInternal(getMethodName(new Object() {
        }), Integer.toString(seq));
    }

    public BodyParameter name(String name) {
        return putInternal(getMethodName(new Object() {
        }), name);
    }

    public BodyParameter author(String author) {
        return putInternal(getMethodName(new Object() {
        }), author);
    }

    @SuppressWarnings("PointlessBooleanExpression")
    public BodyParameter set(String setCode, ArrayList<String> bookList) {
        JsonObject setObject = new JsonObject();
        setObject.addProperty("setCode", setCode);
        if (CollectionValidator.isValid(bookList) == true) {
            JsonArray bookCodeJson = new JsonArray();
            for (String bookCode : bookList) {
                bookCodeJson.add(bookCode);
            }
            setObject.add("bookCode", bookCodeJson);
        }
        put(getMethodName(new Object() {
        }), setObject.toString());
        return this;
    }

    public String getSet() {
        return get(getMethodName(new Object() {
        }));
    }
}
