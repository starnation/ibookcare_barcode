package com.ibook.barcode.network.response;

import com.google.gson.annotations.SerializedName;
import com.starnation.generics.annotation.GenericsClass;
import com.starnation.util.StringUtil;

/*
 * @author lsh
 * @since 2017. 5. 17.
*/
@GenericsClass(extendsClass = BaseResponse.class, type = {String.class})
public class BaseResponse<Result> {

    //======================================================================
    // Variables
    //======================================================================

    @SerializedName("status")
    String mStatus;

    @SerializedName("msg")
    String mMessage;

    @SerializedName("result")
    Result mResult;

    //======================================================================
    // Constructor
    //======================================================================

    @Deprecated
    public BaseResponse() {
        // Nothing
    }

    //======================================================================
    // Public Methods
    //======================================================================

    public Result getResult() {
        return mResult;
    }

    public String getStatus() {
        return mStatus;
    }

    public String getMessage() {
        return mMessage;
    }

    public boolean isSuccess() {
        if (StringUtil.isEmpty(mStatus) == false) {
            switch(mStatus) {
                case "ok":
                    return true;
            }
        }
        return false;
    }
}