package com.ibook.barcode.network.response;


import com.ibook.barcode.network.exception.ServerResponseErrorException;
import com.ibook.barcode.util.log.AppLogger;
import com.ibook.barcode.util.log.Tag;

import java.io.IOException;

/*
 * @author lsh
 * @since 2017. 5. 17.
*/
public enum ErrorStatus {
    /**
     * 401 에러
     */
    failAuth,
    /**
     * 정보가 없는경우 (자녀정보가 없는경우)
     */
    noInfo,
    /**
     * 중복 에러
     */
    overlap,
    /**
     * 기본 에러
     */
    error;

    public static ErrorStatus toErrorStatus(IOException e) {
        try {
            if (e instanceof ServerResponseErrorException) {
                return ErrorStatus.valueOf(((ServerResponseErrorException) e).getStatus());
            }
        } catch (Exception e1) {
            AppLogger.e(Tag.NETWORK_RESPONSE, e1.getMessage());
        }
        return null;
    }
}
