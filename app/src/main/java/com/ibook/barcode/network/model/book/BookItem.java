package com.ibook.barcode.network.model.book;

import android.widget.Checkable;

import com.google.gson.annotations.SerializedName;
import com.ibook.barcode.network.model.user.UserCollection;
import com.ibook.barcode.network.model.user.child.Child;
import com.ibook.barcode.network.response.BaseResponse;
import com.starnation.generics.annotation.GenericsClass;
import com.starnation.util.StringUtil;
import com.starnation.util.validator.CollectionValidator;

import org.parceler.Parcel;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/*
 * @author lsh
 * @since 2017. 5. 17.
*/
@Parcel
@GenericsClass(extendsClass = BaseResponse.class, type = {ArrayList.class})
public final class BookItem implements Checkable {

    //======================================================================
    // Variables
    //======================================================================

    @SerializedName("childCollection")
    UserCollection mUserCollection;

    @SerializedName("info")
    BookInfo mBookInfo;

    @SerializedName("evaluation")
    BookEvaluation mBookEvaluation;

    @SerializedName("balance")
    ArrayList<BookBalance> mBookBalanceList;

    @SerializedName("collection")
    BookCollection mBookCollection;

    @SerializedName("isbnInfo")
    IsbnInfo mIsbnInfo;

    @SerializedName("content")
    BookContent mContent;

    @SerializedName("childList")
    ArrayList<Child> mChildList;

    ArrayList<BookBalance> mShowFlagBookBalanceList = new ArrayList<>();

    boolean mChecked;

    //======================================================================
    // Constructor
    //======================================================================

    @Deprecated
    public BookItem() {
        // Nothing
    }

    //======================================================================
    // Public Methods
    //======================================================================

    public int getBookCount() {
        if (mUserCollection != null) {
            return mUserCollection.getBookCount();
        }
        return 0;
    }

    public BookInfo getBookInfo() {
        return mBookInfo;
    }

    public int getSellBookSeq() {
        if (mBookInfo != null) {
            return mBookInfo.getSellSeq();
        }
        return -1;
    }

    public BookEvaluation getBookEvaluation() {
        return mBookEvaluation;
    }

    public ArrayList<BookBalance> getBookBalanceList() {
        return mBookBalanceList;
    }

    public BookCollection getBookCollection() {
        return mBookCollection;
    }

    public IsbnInfo getIsbnInfo() {
        return mIsbnInfo;
    }

    public BookContent getContent() {
        return mContent;
    }

    @Override
    public void setChecked(boolean checked) {
        mChecked = checked;
    }

    @Override
    public boolean isChecked() {
        return mChecked;
    }

    @Override
    public void toggle() {
        setChecked(!mChecked);
    }

    public String getBookCode() {
        if (mBookInfo == null) {
            return null;
        }
        return mBookInfo.getBookCode();
    }

    public String getChildCollectionSeqText() {
        if (mUserCollection != null) {
            return Integer.toString(mUserCollection.getCollectionInfoSeq());
        }
        return "";
    }

    public String getAuthor() {
        if (mBookInfo == null) {
            return null;
        }
        return mBookInfo.getAuthor();
    }

    public String getPublisher() {
        if (mBookInfo == null) {
            return null;
        }
        return mBookInfo.getPublisher();
    }

    public String getCoverAladin() {
        if (mBookInfo == null) {
            return null;
        }
        return mBookInfo.getCoverAladin();
    }

    public float getRatio() {
        if (mBookInfo == null) {
            return 0f;
        }
        return mBookInfo.getRatio();
    }

    public void setReadYn(boolean readYn) {
        if (mBookInfo == null) {
            return;
        }
        mBookInfo.setReadYn(readYn);
    }

    public boolean isReadingBook() {
        if (mBookInfo == null) {
            return false;
        }
        return mBookInfo.isReadingBook();
    }

    public boolean isLike() {
        if (mBookInfo == null) {
            return false;
        }
        return mBookInfo.isLike();
    }

    public boolean isInterest() {
        if (mBookInfo == null) {
            return false;
        }
        return mBookInfo.isInterest();
    }

    public boolean isCollection() {
        if (mBookInfo == null) {
            return false;
        }
        return mBookInfo.isCollection();
    }

    private String getCollectionBookCode() {
        if (mBookInfo == null) {
            return null;
        }
        return mBookInfo.getCollectionBookCode();
    }

    public boolean isRegister() {
        if (mBookInfo == null) {
            return false;
        }
        return mBookInfo.isChildLibraryRegister();
    }

    /**
     * 콜렉션 정보 사용 가능 여부
     *
     * @return 사용가능 true
     */
    public boolean isCollectionInfoUse() {
        return isCollection() == true && StringUtil.isEmpty(getCollectionBookCode()) == true && mBookCollection != null;
    }

    public ArrayList<BookBalance> getShowFlagBookBalanceList() {
        if (CollectionValidator.isValid(mBookBalanceList) == true
                && CollectionValidator.isValid(mShowFlagBookBalanceList) == false) {

            for (BookBalance balance : mBookBalanceList) {
                if (balance.isCategoryFlag() == true) {
                    mShowFlagBookBalanceList.add(balance);
                }
            }
        }
        return mShowFlagBookBalanceList;
    }

    public ArrayList<Child> getChildList() {
        return mChildList;
    }

    //======================================================================
    // Private Methods
    //======================================================================

    private static String replaceSpecialSymbols(String raw) {
        try {
            Pattern pattern = Pattern.compile("[\\(\\)\\[\\]\\|]");
            Matcher matcher = pattern.matcher(raw);

            while (matcher.find()) {
                try {
                    String match = matcher.group();
                    raw = raw.replace(match, "\\" + match);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            //Nothing
        }
        return raw;
    }

    public String getTitle() {
        if (mBookInfo == null) {
            return null;
        }
        return mBookInfo.getTitle();
    }

    public String getPublisherOrDate() {
        if (mBookInfo == null) {
            return null;
        }
        return mBookInfo.getPublisherOrDate();
    }
}
