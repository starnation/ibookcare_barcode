package com.ibook.barcode.network.model.book;

/*
 * @author lsh
 * @since 2017. 5. 17.
*/
public enum BookEvaluationState {
    GOOD,
    BAD,
    WORSE;
}
