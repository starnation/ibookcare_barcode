package com.ibook.barcode.network;

import com.ibook.barcode.BuildConfig;

/*
 * @author lsh
 * @since 2017. 5. 16.
*/
public enum IBookUrl {
    RESPONSE_LOG(""),
    USER("/api/user"),
    CHILD("/api/child"),
    SEARCH("/api/search"),
    BOOK("/api/book");

    //======================================================================
    // Constants
    //======================================================================

    public final static String TEST_SERVER_URL = "http://52.78.34.111:8090";

    public final static String REAL_SERVER_URL = "http://52.78.34.111:80";

    //======================================================================
    // Variables
    //======================================================================

    private final static CompatImpl IMPL;

    String mPage;

    //======================================================================
    // Constructor
    //======================================================================

    static {
        if (BuildConfig.DEBUG) {
            IMPL = new CompatDebug();
        } else {
            IMPL = new CompatRelease();
        }
    }

    IBookUrl(String page) {
        mPage = page;
    }

    //======================================================================
    // Public Methods
    //======================================================================

    public static String getDomain() {
        return IMPL.getDomain();
    }

    public static String getUrl(IBookUrl iBookUrl) {
        return IMPL.getUrl(iBookUrl.mPage);
    }

    public String getUrl() {
        return IMPL.getUrl(mPage);
    }

    //======================================================================
    // CompatImpl
    //======================================================================

    interface CompatImpl {
        String getDomain();

        String getUrl(String page);
    }

    //======================================================================
    // CompatDebug
    //======================================================================

    final static class CompatDebug implements CompatImpl {

        @Override
        public String getDomain() {
            return TEST_SERVER_URL;
        }

        @Override
        public String getUrl(String url) {
            return getDomain() + url;
        }
    }

    //======================================================================
    // CompatRelease
    //======================================================================

    final static class CompatRelease implements CompatImpl {

        @Override
        public String getDomain() {
            return REAL_SERVER_URL;
        }

        @Override
        public String getUrl(String url) {
            return getDomain() + url;
        }
    }
}
