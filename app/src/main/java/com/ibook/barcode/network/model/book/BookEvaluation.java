package com.ibook.barcode.network.model.book;

import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

import lombok.Data;

/*
 * @author lsh
 * @since 2017. 5. 17.
*/
@Parcel
@Data
public final class BookEvaluation {

    //======================================================================
    // Variables
    //======================================================================

    @SerializedName("grade")
    float mGrade;

    @SerializedName("regCount")
    int mRegCount;
}
