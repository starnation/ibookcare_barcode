package com.ibook.barcode.network.request;

import com.ibook.barcode.network.model.book.BookItem;
import com.ibook.barcode.network.model.user.LoginResult;
import com.ibook.barcode.network.response.BaseResponse;
import com.starnation.okhttp.RequestBuilder;

import java.util.ArrayList;

/*
 * @author lsh
 * @since 2017. 5. 17.
*/
public class Requests {

    public static RequestBuilder<BaseResponse<LoginResult>> postUserLogin() {
        return RequestFactory.<BaseResponse<LoginResult>>postUser(true)
                .parse(ParseFactory.createModelParse(LoginResult.class))
                .suffix(SuffixFactory.getLogin());
    }

    public static RequestBuilder<BaseResponse<BookItem>> getIsbn(String isbn) {
        return RequestFactory.<BaseResponse<BookItem>>getSearch()
                .parse(ParseFactory.createModelParse(BookItem.class))
                .suffix(SuffixFactory.getISBN(isbn));
    }

    /**
     * 책 등록
     * @param childSeq
     * @return
     */
    public static RequestBuilder<BaseResponse> postChildMyBook(int childSeq) {
        return RequestFactory.postChild().suffix(SuffixFactory.getMyBook(childSeq));
    }

    public static RequestBuilder<BaseResponse<ArrayList<BookItem>>> getSearchCollection(String setCode) {
        return RequestFactory.<BaseResponse<ArrayList<BookItem>>>getSearch()
                .parse(ParseFactory.createArrayListParse(BookItem.class))
                .suffix(SuffixFactory.getCollectionCode(setCode));
    }
}
