package com.ibook.barcode.widget.recyclerview.viewholder;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.ViewGroup;

import com.ibook.barcode.app.model.bundle.ViewHolderBundle;
import com.starnation.widget.recyclerview.RecyclerViewHolder;
import com.starnation.widget.recyclerview.holder.SelectorViewHolder;

/*
 * @author lsh
 * @since 2017. 5. 17.
*/
public abstract class BaseRecyclerViewHolder<Item> extends SelectorViewHolder<Item> implements RecyclerViewHolderImpl {

    //======================================================================
    // Variables
    //======================================================================

    final RecyclerViewHolderDelegate mDelegate;

    ViewHolderBundle mViewHolderBundle = new ViewHolderBundle();

    ViewDataBinding mViewDataBinding;

    //======================================================================
    // Constructor
    //======================================================================

    public BaseRecyclerViewHolder(Fragment fragment, ViewGroup viewGroup, @LayoutRes int itemVieId) {
        super(viewGroup, itemVieId);
        mDelegate = new RecyclerViewHolderDelegate(fragment) {

            @Override
            public RecyclerViewHolder holder() {
                return BaseRecyclerViewHolder.this;
            }
        };
        mViewDataBinding = DataBindingUtil.bind(getItemView());
    }

    //======================================================================
    // Override Methods
    //======================================================================

    @Override
    protected final void onBindArguments(@NonNull Bundle arguments) {
        super.onBindArguments(arguments);
    }

    @Override
    public final Fragment getFragment() {
        return mDelegate.getFragment();
    }

    @Override
    public final void sendEvent(int id) {
        mDelegate.sendEvent(id);
    }

    @Override
    public void sendEvent(ViewHolderBundle arguments) {
        mDelegate.sendEvent(arguments);
    }

    @Override
    public final void updateFullSpan() {
        mDelegate.updateFullSpan();
    }

    @Override
    public final void updateFullSpan(boolean fullSpan) {
        mDelegate.updateFullSpan(fullSpan);
    }

    //======================================================================
    // Public Methods
    //======================================================================

    public ViewDataBinding viewDataBinding() {
        return mViewDataBinding;
    }

    public void onBindArguments(@NonNull ViewHolderBundle arguments) {
        //override
    }

    public ViewHolderBundle getViewHolderBundle() {
        return mViewHolderBundle;
    }
}
