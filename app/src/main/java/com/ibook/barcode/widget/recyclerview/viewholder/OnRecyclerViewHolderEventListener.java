package com.ibook.barcode.widget.recyclerview.viewholder;

import android.support.annotation.NonNull;

import com.ibook.barcode.app.model.bundle.ViewHolderBundle;
import com.starnation.widget.recyclerview.RecyclerViewHolder;

/*
 * @author lsh
 * @since 2017. 5. 17.
*/
public interface OnRecyclerViewHolderEventListener {

    void onRecyclerViewHolderEvent(int id, @NonNull RecyclerViewHolder holder);

    void onRecyclerViewHolderEvent(ViewHolderBundle arguments, @NonNull RecyclerViewHolder holder);
}
