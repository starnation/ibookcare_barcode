package com.ibook.barcode.widget;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PixelFormat;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.support.annotation.ColorInt;
import android.support.annotation.NonNull;
import android.support.v7.view.ContextThemeWrapper;
import android.util.AttributeSet;

import com.ibook.barcode.R;
import com.ibook.barcode.util.Util;
import com.starnation.android.view.ViewUtil;
import com.starnation.util.StringUtil;
import com.starnation.widget.BaseImageView;

/*
 * @author lsh
 * @since 2017. 5. 17.
*/
public final class BookImageView extends BaseImageView {

    //======================================================================
    // Constants
    //======================================================================

    private final static float MIN_RATIO = 0.5f;

    private final static float MAX_RATIO = 0.9f;

    private final static float DEFAULT_RATIO = 0.7f;

    //======================================================================
    // Variables
    //======================================================================

    private float mImageRatio;

    private TextDrawable mTextDrawable;

    private boolean mTextDrawableUse;

    private OnErrorImageCallback mErrorImageCallback;

    //======================================================================
    // Constructor
    //======================================================================

    public BookImageView(Context context) {
        super(createBookStyle(context));
    }

    public BookImageView(Context context, AttributeSet attrs) {
        super(createBookStyle(context), attrs);
    }

    //======================================================================
    // Override Methods
    //======================================================================

    @Override
    public void setImageDrawable(Drawable drawable) {
        if (isErrorImageDrawable() == true) {
            if (mErrorImageCallback != null) {
                mErrorImageCallback.onErrorDrawable(drawable);
            }
            if (mTextDrawable != null) {
                setImageRatio(mTextDrawable.mRatio);
            }
        } else {
            if (mErrorImageCallback != null) {
                mErrorImageCallback.onErrorDrawable(null);
            }
        }
        super.setImageDrawable(drawable);
    }

    @Override
    public void setImageUrl(String url) {
        processImageUrl(url, null);
    }

    @Override
    public void setImageUrl(String url, Callback callback) {
        processImageUrl(url, callback);
    }

    @Override
    public void releaseImage() {
        super.releaseImage();
        ViewUtil.setBackgroundDrawable(this, null);
        releaseTextDrawable();
    }

    //======================================================================
    // Public Methods
    //======================================================================

    public void setDefaultImage(@ColorInt int color, String text) {
        setDefaultImageInternal(color, ViewUtil.dpToPx(17f), DEFAULT_RATIO, text);
    }

    public void setDefaultImage(@ColorInt int color, String text, float ratio) {
        setDefaultImageInternal(color, ViewUtil.dpToPx(17f), ratio, text);
    }

    public void setLargeDefaultImage(@ColorInt int color) {
        setDefaultImageInternal(color, ViewUtil.dpToPx(30f), DEFAULT_RATIO, "");
    }

    public void setLargeTextDefaultImage(@ColorInt int color, String text) {
        setDefaultImageInternal(color, ViewUtil.dpToPx(30f), DEFAULT_RATIO, text);
    }

    public void setImageRatio(float imageRatio) {
        if (imageRatio == 0) {
            return;
        }

        if (imageRatio != mImageRatio) {
            if (isRangeRatio(MIN_RATIO, MAX_RATIO, imageRatio) == true) {
                setPadding(0, 0, 0, 0);
                setScaleType(ScaleType.FIT_XY);
            } else {
                int padding = getResources().getDimensionPixelSize(R.dimen.app_margin_10dip);
                setPadding(padding, padding, padding, padding);
                setScaleType(ScaleType.FIT_CENTER);
            }
        }
        mImageRatio = imageRatio;
    }

    public void setErrorImageCallback(OnErrorImageCallback errorImageCallback) {
        mErrorImageCallback = errorImageCallback;
    }

    //======================================================================
    // Private Methods
    //======================================================================

    private void setDefaultImageInternal(@ColorInt int color, int fontSize, float ratio, String text) {
        if (mTextDrawable == null) {
            mTextDrawable = new TextDrawable();
        }

        mTextDrawable.setFontSize(fontSize);
        mTextDrawable.setCornerRadius(getOption().getCornerRadius());
        mTextDrawable.setColor(color);
        mTextDrawable.setText(replaceRegularText(text));
        mTextDrawable.setRatio(ratio);
        mTextDrawableUse = true;
    }

    private String replaceRegularText(String text) {
        if (StringUtil.isEmpty(text) == false) {
            text = text.replaceAll("^[^A-Za-z0-9ㄱ-ㅎㅏ-ㅣ가-힣]+", "");
            if (text.length() > 1) {
                return text.substring(0, 1);
            }
        }
        return "";
    }

    private void processImageUrl(String url, Callback callback) {
        if (isDefaultImageUrl(url) == true) {
            setErrorImage(mTextDrawable);
            super.setImageUrl(null);
        } else {
            setErrorImage(mTextDrawable);
            super.setImageUrl(url, callback);
        }
    }

    private void releaseTextDrawable() {
        if (mTextDrawable != null) {
            mTextDrawable.setCallback(null);
            mTextDrawable = null;
            mTextDrawableUse = false;
        }
    }

    private boolean isDefaultImageUrl(String url) {
        return mTextDrawableUse == true && Util.isDefaultImage(url);
    }

    @SuppressWarnings("RestrictedApi")
    private static ContextThemeWrapper createBookStyle(@NonNull Context context) {
        return new ContextThemeWrapper(context, R.style.RoundedImageViewBook);
    }

    private static boolean isRangeRatio(float minRatio, float maxRatio, float imageRatio) {
        return imageRatio >= minRatio && imageRatio <= maxRatio;
    }

    //======================================================================
    // OnErrorImageCallback
    //======================================================================

    public interface OnErrorImageCallback {
        void onErrorDrawable(Drawable drawable);
    }

    //======================================================================
    // TextDrawable
    //======================================================================

    final static class TextDrawable extends GradientDrawable {

        //======================================================================
        // Variables
        //======================================================================

        final Paint mTextPaint = new Paint();

        String mText;

        int mWidth;

        int mHeight;

        int mTempColor;

        float mCornerRadius;

        float mRatio = DEFAULT_RATIO;

        //======================================================================
        // Constructor
        //======================================================================

        public TextDrawable() {
            mTextPaint.setColor(Color.WHITE);
            mTextPaint.setTypeface(Typeface.DEFAULT);
            mTextPaint.setStyle(Paint.Style.FILL);
            mTextPaint.setAntiAlias(true);
            mTextPaint.setTextAlign(Paint.Align.CENTER);
            mTextPaint.setFakeBoldText(true);
        }

        //======================================================================
        // Override Methods
        //======================================================================

        @Override
        public void draw(Canvas canvas) {
            super.draw(canvas);
            Rect r = getBounds();
            if (StringUtil.isEmpty(mText) == true) {
                return;
            }

            int count = canvas.save();
            canvas.translate(r.left, r.top);

            mWidth = r.width();
            mHeight = r.height();

            canvas.drawText(mText, r.centerX(), mHeight / 2 - ((mTextPaint.descent() + mTextPaint.ascent()) / 2), mTextPaint);

            canvas.restoreToCount(count);
        }

        @Override
        public int getOpacity() {
            return PixelFormat.TRANSLUCENT;
        }

        @Override
        public void setColor(int argb) {
            if (argb != mTempColor) {
                super.setColor(getDarkerShade(argb));
                mTempColor = argb;
            }
        }

        @Override
        public void setCornerRadius(float cornerRadius) {
            if (cornerRadius != mCornerRadius) {
                super.setCornerRadius(cornerRadius);
                mCornerRadius = cornerRadius;
            }
        }

        @Override
        public int getIntrinsicWidth() {
            return 200;
        }

        @Override
        public int getIntrinsicHeight() {
            return Math.round(getIntrinsicWidth() * mRatio);
        }

        //======================================================================
        // Public Methods
        //======================================================================

        public void setText(String text) {
            mText = text;
        }

        public void setFontSize(int fontSize) {
            mTextPaint.setTextSize(fontSize);
        }

        public void setRatio(float ratio) {
            mRatio = ratio;
        }

        //======================================================================
        // Private Methods
        //======================================================================

        private int getDarkerShade(int color) {
            return Color.argb(200, Color.red(color), Color.green(color), Color.blue(color));
        }
    }
}
