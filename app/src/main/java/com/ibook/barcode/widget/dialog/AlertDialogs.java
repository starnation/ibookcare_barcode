package com.ibook.barcode.widget.dialog;

import android.content.Context;
import android.content.DialogInterface;
import android.support.annotation.NonNull;
import android.support.annotation.StringRes;
import android.support.v7.app.AlertDialog;

import com.ibook.barcode.R;

/*
 * @author lsh
 * @since 2017. 5. 17.
*/
public final class AlertDialogs {

    public static AlertDialog create(@NonNull Context context, CharSequence message, @StringRes int positive, DialogInterface.OnClickListener onPositiveListener) {
        /*Not Formatter*/
        return new AlertDialog.Builder(context)
                .setMessage(message)
                .setPositiveButton(positive, onPositiveListener)
                .create();
    }

    public static AlertDialog create(Context context, @NonNull CharSequence message, @StringRes int negative, @StringRes int positive, DialogInterface.OnClickListener onNegativeListener, DialogInterface.OnClickListener onPositiveListener) {
        /*Not Formatter*/
        return new AlertDialog.Builder(context)
                .setMessage(message)
                .setPositiveButton(positive, onPositiveListener)
                .setNegativeButton(negative, onNegativeListener)
                .create();
    }

    public static AlertDialog createConfirm(@NonNull Context context, String message, DialogInterface.OnClickListener positiveListener) {
        return new AlertDialog.Builder(context)
                .setMessage(message)
                .setCancelable(false)
                .setPositiveButton(R.string.app_confirm, positiveListener)
                .create();
    }
}
