package com.ibook.barcode.widget.recyclerview.viewholder;

import android.support.v4.app.Fragment;
import android.support.v7.widget.StaggeredGridLayoutManager;

import com.ibook.barcode.app.fragment.FragmentDelegate;
import com.ibook.barcode.app.model.bundle.ViewHolderBundle;
import com.starnation.widget.recyclerview.RecyclerViewHolder;

/*
 * @author lsh
 * @since 2016. 7. 22.
*/
abstract class RecyclerViewHolderDelegate implements RecyclerViewHolderImpl {

    //======================================================================
    // Variables
    //======================================================================

    int mFragmentHashCode;

    //======================================================================
    // Constructor
    //======================================================================

    public RecyclerViewHolderDelegate(Fragment fragment) {
        if (fragment != null) {
            mFragmentHashCode = fragment.hashCode();
        }
    }

    //======================================================================
    // Abstract Methods
    //======================================================================

    public abstract RecyclerViewHolder holder();

    //======================================================================
    // Override Methods
    //======================================================================

    @Override
    public Fragment getFragment() {
        return FragmentDelegate.findFragment(mFragmentHashCode);
    }

    @Override
    public void sendEvent(int id) {
        Fragment f = getFragment();
        if (f == null) {
            return;
        }

        if (f instanceof OnRecyclerViewHolderEventListener) {
            ((OnRecyclerViewHolderEventListener) f).onRecyclerViewHolderEvent(id, holder());
        }
    }

    @Override
    public void sendEvent(ViewHolderBundle arguments) {
        Fragment f = getFragment();
        if (f == null) {
            return;
        }

        if (f instanceof OnRecyclerViewHolderEventListener) {
            ((OnRecyclerViewHolderEventListener) f).onRecyclerViewHolderEvent(arguments, holder());
        }
    }

    @Override
    public void updateFullSpan() {
        if (holder().getItemView().getLayoutParams() instanceof StaggeredGridLayoutManager.LayoutParams) {
            ((StaggeredGridLayoutManager.LayoutParams) holder().getItemView().getLayoutParams()).setFullSpan(true);
        }
    }

    @Override
    public void updateFullSpan(boolean fullSpan) {
        if (holder().getItemView().getLayoutParams() instanceof StaggeredGridLayoutManager.LayoutParams) {
            ((StaggeredGridLayoutManager.LayoutParams) holder().getItemView().getLayoutParams()).setFullSpan(fullSpan);
        }
    }
}
