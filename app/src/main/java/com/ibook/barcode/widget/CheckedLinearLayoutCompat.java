package com.ibook.barcode.widget;

import android.content.Context;
import android.support.v7.widget.LinearLayoutCompat;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Checkable;

/*
 * @author lsh
 * @since 2017. 5. 17.
*/
public class CheckedLinearLayoutCompat extends LinearLayoutCompat implements Checkable {

    //======================================================================
    // Constants
    //======================================================================

    public static final int[] CHECKED_STATE = {android.R.attr.state_checked};

    //======================================================================
    // Variables
    //======================================================================

    private boolean mChecked;

    private boolean mCheckedEnable;

    private OnClickListener mOnClickListener;

    //======================================================================
    // Constructor
    //======================================================================

    public CheckedLinearLayoutCompat(Context context) {
        super(context);
        init();
    }

    public CheckedLinearLayoutCompat(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public CheckedLinearLayoutCompat(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        super.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mCheckedEnable == true) {
                    toggle();
                }
                if (mOnClickListener != null) {
                    mOnClickListener.onClick(v);
                }
            }
        });
    }

    //======================================================================
    // Override Methods
    //======================================================================

    @Override
    public void setChecked(boolean checked) {
        mChecked = checked;
        refreshDrawableState();
    }

    @Override
    public boolean isChecked() {
        return mChecked;
    }

    @Override
    public void toggle() {
        setChecked(!mChecked);
    }

    @Override
    public int[] onCreateDrawableState(int extraSpace) {
        final int[] drawableState = super.onCreateDrawableState(extraSpace + 1);
        if (isChecked()) {
            mergeDrawableStates(drawableState, CHECKED_STATE);
        }
        return drawableState;
    }

    @Override
    public void setOnClickListener(OnClickListener onClickListener) {
        mOnClickListener = onClickListener;
    }

    //======================================================================
    // Public Methods
    //======================================================================

    public void setCheckedEnable(boolean checkedEnable) {
        mCheckedEnable = checkedEnable;
    }
}
