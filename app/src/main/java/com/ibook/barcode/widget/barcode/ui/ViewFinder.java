package com.ibook.barcode.widget.barcode.ui;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.View;

/*
 * @author lsh
 * @since 2017. 5. 17.
*/
public class ViewFinder extends View {

    //======================================================================
    // Constance
    //======================================================================

    int MaskColor = Color.argb(96, 0, 0, 0);

    int LineColor = Color.argb(255, 204, 0, 0);

    //======================================================================
    // Variables
    //======================================================================

    Paint paint;

    Rect mFrameRect;

    //======================================================================
    // Constructor
    //======================================================================

    public ViewFinder(Context context, AttributeSet attrs) {
        super(context, attrs);

        paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mFrameRect = new Rect();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        int width = canvas.getWidth();
        int height = canvas.getHeight();

        int widthHalf = width / 2;
        int heightHalf = height / 2;

        int offset = Math.min(widthHalf, heightHalf) / 2;

        int marginY = 250;

        mFrameRect.set(widthHalf - (int)(offset * 1.5f), heightHalf - offset - marginY, widthHalf + (int)(offset * 1.5f), heightHalf + offset - marginY);

        paint.setColor(MaskColor);
        canvas.drawRect(0, 0, width, mFrameRect.top, paint);
        canvas.drawRect(0, mFrameRect.top, mFrameRect.left, mFrameRect.bottom + 1, paint);
        canvas.drawRect(mFrameRect.right + 1, mFrameRect.top, width, mFrameRect.bottom + 1, paint);
        canvas.drawRect(0, mFrameRect.bottom + 1, width, height, paint);

        paint.setColor(LineColor);
        canvas.drawLine(mFrameRect.left, heightHalf - marginY, mFrameRect.right, heightHalf - marginY, paint);
    }
}
