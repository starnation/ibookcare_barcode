package com.ibook.barcode.widget.recyclerview;

import android.os.Bundle;
import android.view.ViewGroup;

import com.ibook.barcode.app.model.bundle.ViewHolderBundle;
import com.ibook.barcode.app.viewholder.LoadingViewHolder;
import com.ibook.barcode.widget.recyclerview.viewholder.BaseRecyclerViewHolder;
import com.starnation.widget.recyclerview.RecyclerViewAdapter;
import com.starnation.widget.recyclerview.RecyclerViewHolder;

/*
 * @author lsh
 * @since 2017. 5. 17.
*/
public abstract class BaseRecyclerViewAdapter extends RecyclerViewAdapter {

    //======================================================================
    // Variables
    //======================================================================

    Provider mProvider;

    EmptyViewHolderProcessor mEmptyViewHolderProcessor;

    //======================================================================
    // Constructor
    //======================================================================

    public BaseRecyclerViewAdapter(Provider provider) {
        mProvider = provider;
    }

    public BaseRecyclerViewAdapter() {
        // Nothing
    }

    //======================================================================
    // Override Methods
    //======================================================================

    @Override
    protected RecyclerViewHolder onCreateEmptyViewHolder(ViewGroup parent) {
        if (mEmptyViewHolderProcessor != null) {
            return mEmptyViewHolderProcessor.onCreateEmptyViewHolder(parent);
        }
        return null;
    }

    @Override
    protected RecyclerViewHolder onCreateFooterViewHolder(ViewGroup parent) {
        return new LoadingViewHolder(parent);
    }

    @Override
    public final int getItemHeaderCount() {
        if (mProvider != null) {
            return mProvider.getItemHeaderCount();
        }
        return 0;
    }

    @Override
    public final Object getSupportItem(int viewType, int position) {
        switch(viewType) {
            case TYPE_EMPTY:
                if (mEmptyViewHolderProcessor != null) {
                    return mEmptyViewHolderProcessor.getItem();
                }
        }
        if (mProvider != null) {
            return mProvider.getSupportItem(viewType, position);
        }
        return null;
    }

    @Override
    public final int getSupportItemCount() {
        if (mProvider != null) {
            return mProvider.getSupportItemCount();
        }
        return 0;
    }

    @Override
    public final int getSupportItemViewType(int position) {
        if (mProvider != null) {
            return mProvider.getSupportItemViewType(position);
        }
        return super.getSupportItemViewType(position);
    }

    @Override
    protected void onPreBindViewHolder(RecyclerViewHolder holder, int position) {
        super.onPreBindViewHolder(holder, position);
        if (holder instanceof BaseRecyclerViewHolder) {
            if (mProvider != null && mProvider instanceof ArgumentsProvider) {
                ((ArgumentsProvider) mProvider).onBindArguments(holder.getItemViewType(), ((BaseRecyclerViewHolder) holder).getViewHolderBundle());
            }
            ((BaseRecyclerViewHolder) holder).onBindArguments(((BaseRecyclerViewHolder) holder).getViewHolderBundle());
        }
    }

    //======================================================================
    // Public Methods
    //======================================================================

    public void setEmptyViewHolderProcessor(EmptyViewHolderProcessor emptyViewHolderProcessor) {
        mEmptyViewHolderProcessor = emptyViewHolderProcessor;
    }

    public void setProvider(Provider provider) {
        mProvider = provider;
    }

    //======================================================================
    // Provider
    //======================================================================

    public interface Provider {

        int getItemHeaderCount();

        int getSupportItemCount();

        int getSupportItemViewType(int position);

        Object getSupportItem(int viewType, int position);
    }

    //======================================================================
    // ArgumentsProvider
    //======================================================================

    public interface ArgumentsProvider extends Provider {

        /**
         * {@link RecyclerViewAdapter#onBindArguments(int, Bundle)}
         */
        void onBindArguments(int viewType, ViewHolderBundle arguments);
    }

    //========================================================`==============
    // EmptyViewHolderProcessor
    //======================================================================

    public interface EmptyViewHolderProcessor {

        RecyclerViewHolder onCreateEmptyViewHolder(ViewGroup parent);

        Object getItem();
    }
}
