package com.ibook.barcode.widget.barcode;

import android.annotation.SuppressLint;
import android.content.Context;
import android.hardware.Camera;
import android.os.Build;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.LinearLayout;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.vision.MultiProcessor;
import com.google.android.gms.vision.barcode.Barcode;
import com.google.android.gms.vision.barcode.BarcodeDetector;
import com.ibook.barcode.R;
import com.ibook.barcode.app.model.eventbus.Event;
import com.ibook.barcode.app.model.bundle.EventBundle;
import com.ibook.barcode.app.model.eventbus.EventKey;
import com.ibook.barcode.util.log.AppLogger;
import com.ibook.barcode.util.log.Tag;
import com.ibook.barcode.widget.barcode.ui.CameraSource;
import com.ibook.barcode.widget.barcode.ui.CameraSourcePreview;
import com.ibook.barcode.widget.barcode.ui.GraphicOverlay;
import com.starnation.eventbus.EventBus;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

/*
 * @author lsh
 * @since 2017. 5. 17.
*/
public class ScanView extends LinearLayout {

    //======================================================================
    // Constance
    //======================================================================

    // intent request code to handle updating play services if needed.
    private static final int RC_HANDLE_GMS = 9001;

    // constants used to pass extra data in the intent
    public static final String AUTO_FOCUS = "AutoFocus";

    public static final String USE_FLASH = "UseFlash";

    //======================================================================
    // Variables
    //======================================================================

    private CameraSource mCameraSource;

    private LinearLayout mTopLayout;

    private CameraSourcePreview mPreview;

    private GraphicOverlay<BarcodeGraphic> mGraphicOverlay;

    private Subscription mSubscription;

    private int mSupportFormat;

    //======================================================================
    // Constructor
    //======================================================================

    public ScanView(Context context) {
        super(context);
        init();
    }

    public ScanView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public ScanView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    @SuppressWarnings("unchecked")
    private void init() {
        LayoutInflater.from(getContext()).inflate(R.layout.merge_barcode_scanview, this, true);
        setOrientation(VERTICAL);

        mPreview = (CameraSourcePreview) findViewById(R.id.preview);
        mGraphicOverlay = (GraphicOverlay<BarcodeGraphic>) findViewById(R.id.graphicOverlay);
        mGraphicOverlay.setGraphicDraw(false);

        createCameraSource(true, false);
    }

    //======================================================================
    // Public Methods
    //======================================================================

    /**
     * @param supportFormat {@link Barcode}
     */
    public void setSupportFormat(int supportFormat) {
        mSupportFormat = supportFormat;
    }

    /**
     * Restarts the camera.
     */
    public void onResume() {
        startCameraSource();
    }

    /**
     * Stops the camera.
     */
    public void onPause() {
        if (mPreview != null) {
            mPreview.stop();
        }
        if (mSubscription != null) {
            mSubscription.unsubscribe();
            mSubscription = null;
        }
    }

    /**
     * Releases the resources associated with the camera source, the associated detectors, and the
     * rest of the processing pipeline.
     */
    public void onRelease() {
        if (mPreview != null) {
            mPreview.release();
        }
        if (mSubscription != null) {
            mSubscription.unsubscribe();
            mSubscription = null;
        }
    }

    //======================================================================
    // Private Methods
    //======================================================================

    /**
     * Creates and starts the camera.  Note that this uses a higher resolution in comparison
     * to other detection examples to enable the barcode detector to detect small barcodes
     * at long distances.
     * <p>
     * Suppressing InlinedApi since there is a check that the minimum version is met before using
     * the constant.
     */
    @SuppressLint("InlinedApi")
    private void createCameraSource(boolean autoFocus, boolean useFlash) {
        Context context = getContext().getApplicationContext();

        // A barcode detector is created to track barcodes.  An associated multi-processor instance
        // is set to receive the barcode detection results, track the barcodes, and maintain
        // graphics for each barcode on screen.  The factory is used by the multi-processor to
        // init a separate tracker instance for each barcode.
        BarcodeDetector barcodeDetector = new BarcodeDetector.Builder(context).build();
        BarcodeTrackerFactory barcodeFactory = new BarcodeTrackerFactory(mGraphicOverlay);
        barcodeDetector.setProcessor(
                new MultiProcessor.Builder<>(barcodeFactory).build());

        if (!barcodeDetector.isOperational()) {
            // Note: The first time that an app using the barcode or face API is installed on a
            // device, GMS will download a native libraries to the device in order to do detection.
            // Usually this completes before the app is run for the first time.  But if that
            // download has not yet completed, then the above call will not detect any barcodes
            // and/or faces.
            //
            // isOperational() can be used to check if the required native libraries are currently
            // available.  The detectors will automatically become operational once the library
            // downloads complete on device.
            AppLogger.w(Tag.DEFAULT, "Detector dependencies are not yet available.");

            // Check for low storage.  If there is low storage, the native library will not be
            // downloaded, so detection will not become operational.
//            IntentFilter lowstorageFilter = new IntentFilter(Intent.ACTION_DEVICE_STORAGE_LOW);
//            boolean hasLowStorage = registerReceiver(null, lowstorageFilter) != null;
//
//            if (hasLowStorage) {
//                Toast.makeText(this, R.string.low_storage_error, Toast.LENGTH_LONG).show();
//                AppLogger.w(Tag.DEFAULT, getContext().getString(R.string.low_storage_error));
//            }
        }

        // Creates and starts the camera.  Note that this uses a higher resolution in comparison
        // to other detection examples to enable the barcode detector to detect small barcodes
        // at long distances.
        CameraSource.Builder builder = new CameraSource.Builder(getContext().getApplicationContext(), barcodeDetector)
                .setFacing(CameraSource.CAMERA_FACING_BACK)
                .setRequestedPreviewSize(1600, 1024)
                .setRequestedFps(15.0f);

        // make sure that auto focus is an available option
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
            builder = builder.setFocusMode(
                    autoFocus ? Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE : null);
        }

        mCameraSource = builder
                .setFlashMode(useFlash ? Camera.Parameters.FLASH_MODE_TORCH : null)
                .build();
    }

    /**
     * Starts or restarts the camera source, if it exists.  If the camera source doesn't exist yet
     * (e.g., because onResume was called before the camera source was created), this will be called
     * again when the camera source is created.
     */
    private void startCameraSource() throws SecurityException {
        long current = System.currentTimeMillis();
        // check that the device has play services available.
        int code = GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(
                getContext().getApplicationContext());
        if (code != ConnectionResult.SUCCESS) {
//            GoogleApiAvailability.getInstance()
//                    .getErrorDialog(getContext(), code, RC_HANDLE_GMS)
//                    .show();
        }

        if (mCameraSource != null) {
            try {
                mPreview.start(mCameraSource, mGraphicOverlay);
            } catch (IOException e) {
                AppLogger.e(Tag.DEFAULT, "Unable to start camera source.");
                mCameraSource.release();
                mCameraSource = null;
            }
        }

        if (mSubscription != null) {
            mSubscription.unsubscribe();
            mSubscription = null;
        }

        mSubscription = Observable.interval(100, TimeUnit.MILLISECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.newThread())
                .subscribe(new Action1<Long>() {
                    @Override
                    public void call(Long aLong) {
                        for (BarcodeGraphic graphic : mGraphicOverlay.getGraphics()) {
                            Barcode barcode = graphic.getBarcode();

                            if (barcode != null) {
                                if (mSupportFormat == 0 || mSupportFormat == barcode.valueFormat) {
                                    EventBus.with().postAll(new Event(EventKey.RESULT_BARCODE,
                                            new EventBundle()
                                                    .scanResult(barcode.displayValue)
                                                    .scanResultFormat(getFormat(barcode.valueFormat))));
                                }
                            }
                        }
                    }
                });
        AppLogger.w(Tag.DEFAULT, "startCameraSource end : " + (System.currentTimeMillis() - current));
    }

    private String getFormat(int format) {
        switch(format) {
            case 16:
                return "DATA_MATRIX";
            case 32:
                return "EAN_13";
            case 64:
                return "EAN_8";
            case 128:
                return "ITF";
            case 256:
                return "QR_CODE";
            case 512:
                return "UPC_A";
            case 1024:
                return "UPC_E";
            case 2048:
                return "PDF417";
            case 4096:
                return "AZTEC";
            case 1:
                return "CONTACT_INFO";
            case 2:
                return "EMAIL";
            case 3:
                return "ISBN";
            case 4:
                return "PHONE";
            case 5:
                return "PRODUCT";
            case 6:
                return "SMS";
            case 7:
                return "TEXT";
            case 8:
                return "URL";
            case 9:
                return "WIFI";
            case 10:
                return "GEO";
            case 11:
                return "CALENDAR_EVENT";
            case 12:
                return "DRIVER_LICENSE";
        }
        return "ALL_FORMATS";
    }
}
