package com.ibook.barcode.widget;

import android.content.Context;
import android.os.Build;
import android.support.annotation.ColorInt;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Checkable;

import com.starnation.widget.BaseImageView;

/*
 * @author lsh
 * @since 2017. 5. 17.
*/
public class CheckedImageView extends BaseImageView implements Checkable {

    //======================================================================
    // Constants
    //======================================================================

    public static final int[] CHECKED_STATE = {android.R.attr.state_checked};

    //======================================================================
    // Variables
    //======================================================================

    private boolean mChecked;

    private @ColorInt int mCheckedColor;

    //======================================================================
    // Constructor
    //======================================================================

    public CheckedImageView(Context context) {
        super(context);
    }

    public CheckedImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CheckedImageView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    //======================================================================
    // Override Methods
    //======================================================================

    @Override
    public void setChecked(boolean checked) {
        mChecked = checked;

        refreshDrawableState();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            if (mCheckedColor != 0) {
                clearColorFilter();
                if (checked == true) {
                    setColorFilter(mCheckedColor);
                }
            }
        }
    }

    @Override
    public boolean isChecked() {
        return mChecked;
    }

    @Override
    public void toggle() {
        setChecked(!mChecked);
    }

    @Override
    public int[] onCreateDrawableState(int extraSpace) {
        int[] drawableState = super.onCreateDrawableState(extraSpace + 1);
        if (isChecked()) {
            mergeDrawableStates(drawableState, CHECKED_STATE);
        }
        return drawableState;
    }

    @Override
    protected void drawableStateChanged() {
        super.drawableStateChanged();
        invalidate();
    }

    @Override
    public void setOnClickListener(View.OnClickListener onClickListener) {
        super.setOnClickListener(new OnClickListenerWrapper(onClickListener) {
            @Override
            public void onPreClick(View v) {
                toggle();
            }
        });
    }

    //======================================================================
    // OnClickListenerWrapper
    //======================================================================

    abstract static class OnClickListenerWrapper implements View.OnClickListener {

        View.OnClickListener mOnClickListener;

        public OnClickListenerWrapper(View.OnClickListener onClickListener) {
            mOnClickListener = onClickListener;
        }

        public abstract void onPreClick(View v);

        @Override
        public void onClick(View v) {
            onPreClick(v);
            if (mOnClickListener != null) {
                mOnClickListener.onClick(v);
            }
        }
    }
}
