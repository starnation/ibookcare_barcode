package com.ibook.barcode.widget.recyclerview.viewholder;

import android.support.v4.app.Fragment;

import com.ibook.barcode.app.model.bundle.ViewHolderBundle;

/*
 * @author lsh
 * @since 2017. 5. 17.
*/
interface RecyclerViewHolderImpl {

    Fragment getFragment();

    void sendEvent(int id);

    void sendEvent(ViewHolderBundle arguments);

    void updateFullSpan();

    void updateFullSpan(boolean fullSpan);
}
