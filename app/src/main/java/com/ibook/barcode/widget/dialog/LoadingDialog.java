package com.ibook.barcode.widget.dialog;

import android.app.Dialog;
import android.content.Context;

import com.ibook.barcode.R;

/*
 * @author lsh
 * @since 17. 5. 16.
*/
public final class LoadingDialog {

    @SuppressWarnings("ConstantConditions")
    public static Dialog create(Context context) {
        Dialog dialog = new Dialog(context, R.style.DialogStyle_Transparent);
        dialog.setContentView(R.layout.dialog_loading);
        dialog.setCancelable(false);
        return dialog;
    }
}
