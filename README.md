# 바코드 #

바코드 샘플 프로젝트

- bitbucket에서 maven repostory 사용하는 내용 
 - https://jeroenmols.com/blog/2016/02/05/wagongit/
 - **git 주소 사용 하는 방식을 https가 아닌 ssh 방식으로 사용 해야됩나다.**


# Dependencies

root build.gradle 파일에는 

[barcode_support](https://bitbucket.org/starnation/barcode_support) repository 주소를 추가 해야됩니다.

```
buildscript {
    repositories {
        jcenter()
        mavenCentral()
    }
    dependencies {
        classpath 'com.android.tools.build:gradle:2.2.3'
        classpath 'com.neenbedankt.gradle.plugins:android-apt:1.8'
    }
}

allprojects {
    repositories {
        jcenter()
        maven {
            url "https://api.bitbucket.org/1.0/repositories/starnation/barcode_support/raw/releases"
        }
    }
}

task clean(type: Delete) {
    delete rootProject.buildDir
}
```




샘플 프로젝트에서 사용하는  library 종류입니다.

패키지명이 com.starnation 시작되는 library는 public_support 프로젝트 [barcode_support](https://bitbucket.org/starnation/barcode_support) repository 되어 있습니다.


project-dependencies.gradle
```
apply plugin: 'com.neenbedankt.android-apt'

dependencies {
    def SUPPORT_VERSION = "25.1.1"
    def GOOGLE_PLAY_SERVICES_VERSION = "10.0.1"

    //Modify 2017. 5. 16. lsh Google Support
    //noinspection GradleCompatible
    compile "com.android.support:appcompat-v7:${SUPPORT_VERSION}"
    compile "com.android.support:recyclerview-v7:${SUPPORT_VERSION}"
    compile "com.android.support:design:${SUPPORT_VERSION}"
    compile "com.android.support:support-annotations:${SUPPORT_VERSION}"

    //Modify 2017. 5. 17. lsh GooglePlay
    compile "com.google.android.gms:play-services-vision:${GOOGLE_PLAY_SERVICES_VERSION}"

    //Modify 2017. 5. 16. lsh RxAndroid
    compile 'io.reactivex:rxandroid:1.1.0'

    //Modify 2017. 5. 17. lsh https://github.com/JakeWharton/butterknife
    apt 'com.jakewharton:butterknife-compiler:8.1.0'

    //Modify 2017. 5. 17. lsh Paarceler https://github.com/johncarl81/parceler
    apt 'org.parceler:parceler:1.1.5'

    //Modify 2016, 5. 17. lsh Lombok https://projectlombok.org/mavenrepo/index.html
    compile "org.projectlombok:lombok:1.16.14"

    compile project(':generics')
    apt project(':generics')

    //Modify 2017. 5. 16. lsh starnation_okhttp
    compile 'com.starnation.okhttp:release:0.0.1'

    //Modify 2017. 5. 16. lsh starnation_util
    compile('com.starnation.util:release:0.0.1') {
        exclude group: 'com.android.support', module: 'appcompat-v7'
    }

    //Modify 2017. 5. 16. lsh starnation_eventbus
    compile('com.starnation.eventbus:release:0.0.1') {
        exclude group: 'com.android.support', module: 'appcompat-v7'
    }

    //Modify 2017. 5. 16. lsh starnation_glide
    compile('com.starnation.glide:release:0.0.1') {
        exclude group: 'com.android.support', module: 'appcompat-v7'
        exclude group: 'com.starnation.okhttp', module: 'release'
        exclude group: 'com.starnation.util', module: 'release'
    }

    //Modify 2017. 5. 16. lsh starnation_android
    compile('com.starnation.android:release:0.0.1') {
        exclude group: 'com.android.support', module: 'appcompat-v7'
        exclude group: 'com.android.support', module: 'design'
        exclude group: 'com.android.support', module: 'preference-v7'
        exclude group: 'com.starnation.eventbus', module: 'release'
        exclude group: 'com.starnation.util', module: 'release'
        exclude group: 'com.nineoldandroids', module: 'library'
    }

    //Modify 2017. 5. 16. lsh starnation_widget
    compile('com.starnation.widget:release:0.0.1') {
        exclude group: 'com.android.support', module: 'appcompat-v7'
        exclude group: 'com.android.support', module: 'recyclerview'
        exclude group: 'com.starnation.glide', module: 'release'
        exclude group: 'com.starnation.util', module: 'release'
        exclude group: 'com.starnation.android', module: 'release'
    }
}
```


# BookRegisterFragment 

멀티 바코드 화면

![Alt text](/screenshot/device-2017-05-18-161154.png)
![Alt text](/screenshot/device-2017-05-18-161246.png)

# InputFragment 

바코드 직접 입력 화면

![Alt text](/screenshot/device-2017-05-18-161322.png)

# CollectionBookSelectFragment

전집일 경우 선택 책 추가 화면

![Alt text](/screenshot/device-2017-05-18-161304.png)